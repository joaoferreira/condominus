package backend;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A existência da classe AtividadeManutencao permite a criação de atividades de
 * manutenção que serão registadas para cada condomínio
 *
 * @author JoaoFerreira
 */
public class AtividadeManutencao implements Serializable {

    // Variáveis de Instância 
    private Date data;
    private String atividadeManutencao, descricao;
    private Colaborador[] colaboradores;

    /**
     * Método construtor da classe
     *
     * @param data Data da realização da atividade de manutenção
     * @param atividadeManutencao Nome da atividade de manutenção
     * @param descricao Descrição da atividade de manutenção
     * @param colaboradores Lista de colaboradores responsáveis pela atividade
     * de manutenção
     */
    public AtividadeManutencao(Date data, String atividadeManutencao, String descricao, Colaborador[] colaboradores) {
        this.data = data;
        this.atividadeManutencao = atividadeManutencao;
        this.descricao = descricao;
        this.colaboradores = colaboradores;
    }

    /**
     * Método seletor da classe
     *
     * @return Date - Data da atividade de manutenção
     */
    public Date getData() {
        return data;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Nome da atividade de manutenção
     */
    public String getActividadeManutencao() {
        return atividadeManutencao;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Descrição da atividade de manutenção
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Método seletor da classe
     *
     * @return Array - Colaboradores responsáveis pela atividade de manutenção
     */
    public Colaborador[] getColaboradores() {
        return colaboradores;
    }

    /**
     * Método modificador da classe: Altera a data da atividade de manutenção
     *
     * @param novaData Nova data da atividade de manutenção
     */
    public void setData(Date novaData) {
        data = novaData;
    }

    /**
     * Método modificador da classe: Altera o nome da atividade de manutenção
     *
     * @param novaAtividadeManutencao Novo nome da atividade de manutenção
     */
    public void setAtividadeManutencao(String novaAtividadeManutencao) {
        atividadeManutencao = novaAtividadeManutencao;
    }

    /**
     * Método modificador da classe: Altera a descrição da atividade de
     * manutenção
     *
     * @param novaDescricao Nova descrição da atividade de manutenção
     */
    public void setDescricao(String novaDescricao) {
        descricao = novaDescricao;
    }

    /**
     * Método modificador da classe: Altera a lista de colaboradores
     * responsáveis pela atividade de manutenção
     *
     * @param novosColaboradores Nova lista de colaboradores responsáveis pela
     * atividade de manutenção
     */
    public void setColaboradores(Colaborador[] novosColaboradores) {
        colaboradores = novosColaboradores;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * da atividade de manutenção
     *
     * @return String - Informação da atividade de manutenção
     */
    @Override
    public String toString() {
        String stringData = new SimpleDateFormat("yyyy-MM-dd").format(data);
        StringBuilder s = new StringBuilder("");
        s.append("Atividade de Manutenção: " + atividadeManutencao + "\n");
        s.append("Data: " + stringData + "\n");
        s.append("Descrição: " + descricao + "\n");
        s.append("\n");
        return s.toString();
    }
}
