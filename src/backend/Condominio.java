package backend;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * A existência da classe Condominio permite a criação dos condomínios da
 * empresa Condominus
 *
 * @author JoaoFerreira
 */
public class Condominio implements Serializable, Comparator {

    // Variáveis de instância 
    private String nome, morada, codigoPostal2, localidade;
    private long codigoPostal1;
    private double estimativaDespesas, valorDespesasAdministrativas, valorLimpezas,
            valorReparacoes, valorElevadores, valorOutras, honorarios, valorCondominio;
    private double volumeFaturacao, volumeFaturacaoPeriodo;
    private int modalidadeCalculoQuota;
    private final ArrayList<Condomino> listaCondominos;
    private final ArrayList<Reuniao> listaReunioes;
    private final ArrayList<AtividadeManutencao> listaAtivManut;

    /**
     * Método construtor da classe
     *
     * @param nome Nome do condomínio
     * @param morada Morada do condomínio
     * @param codigoPostal1 Código postal do condomínio (4 dígitos)
     * @param codigoPostal2 Código postal do condomínio (3 dígitos)
     * @param localidade Localidade do condomínio
     * @param estimativaDespesas Valor das despesas estimadas do condomínio
     * @param valorDespesasAdministrativas Valor específico das despesas
     * administrativas
     * @param valorLimpezas Valor específico das despesas com limpezas
     * @param valorReparacoes Valor específico das despesas com reparações
     * @param valorElevadores Valor específico das despesas com elevadores
     * @param valorOutras Valor específico de outras despesas do condomínio
     * @param honorarios Valor dos honorários da empresa gestora
     * @param modalidadeCalculoQuota Modalidade para o cálculo do valor da quota
     */
    public Condominio(String nome, String morada, long codigoPostal1,
            String codigoPostal2, String localidade, double estimativaDespesas,
            double valorDespesasAdministrativas, double valorLimpezas, double valorReparacoes,
            double valorElevadores, double valorOutras, double honorarios,
            int modalidadeCalculoQuota) {
        this.nome = nome;
        this.morada = morada;
        this.codigoPostal1 = codigoPostal1;
        this.codigoPostal2 = codigoPostal2;
        this.localidade = localidade;
        this.estimativaDespesas = estimativaDespesas;
        this.valorDespesasAdministrativas = valorDespesasAdministrativas;
        this.valorLimpezas = valorLimpezas;
        this.valorReparacoes = valorReparacoes;
        this.valorElevadores = valorElevadores;
        this.valorOutras = valorOutras;
        this.honorarios = honorarios;
        this.modalidadeCalculoQuota = modalidadeCalculoQuota;
        valorCondominio = honorarios + estimativaDespesas;
        listaCondominos = new ArrayList<>();
        listaReunioes = new ArrayList<>();
        listaAtivManut = new ArrayList<>();
        calculoValorFracao();
    }

    /**
     * Método seletor da classe
     *
     * @return String - Nome do condomínio
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Morada do condomínio
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Método seletor da classe
     *
     * @return Long - Código postal do condomínio (4 dígitos)
     */
    public long getCodigoPostal1() {
        return codigoPostal1;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Código postal do condomínio (3 dígitos)
     */
    public String getCodigoPostal2() {
        return codigoPostal2;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Localidade do condomínio
     */
    public String getLocalidade() {
        return localidade;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor da estimativa de despesas anuais do condomínio
     */
    public double getEstimativaDespesas() {
        return estimativaDespesas;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor estimado para as despesas administrativas do
     * condomínio
     */
    public double getValorDespesasAdministrativas() {
        return valorDespesasAdministrativas;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor estimado para as despesas com as limpezas do
     * condomínio
     */
    public double getValorLimpezas() {
        return valorLimpezas;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor estimado para as despesas com as reparações do
     * condomínio
     */
    public double getValorReparacoes() {
        return valorReparacoes;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor estimado para as despesas com a manutenção dos
     * elevadores do condomínio
     */
    public double getValorElevadores() {
        return valorElevadores;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor estimado para outras despesas do condomínio
     */
    public double getValorOutras() {
        return valorOutras;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor dos honorários da empresa Condominus
     */
    public double getHonorarios() {
        return honorarios;
    }

    /**
     * Método selector da classe
     *
     * @return Double - Valor do condomínio
     */
    public double getValorCondominio() {
        return valorCondominio;
    }

    /**
     * Método selector da classe
     *
     * @return Int - Modalidade de cálculo do valor da quota
     */
    public int getModalidadeCalculoQuota() {
        // 0 - Permilagem, 1 - Equitativa, 2 - Exacta, 3 - Manual
        return modalidadeCalculoQuota;
    }

    /**
     * Método selector da classe
     *
     * @return ArrayList - Lista de Condóminos
     */
    public ArrayList<Condomino> getListaCondominos() {
        return listaCondominos;
    }

    /**
     * Método selector da classe
     *
     * @return ArrayList - Lista de atividades de manutenção
     */
    public ArrayList<AtividadeManutencao> getListaAtivManut() {
        return listaAtivManut;
    }

    /**
     * Método selector da classe
     *
     * @return ArrayList - Lista de reuniões
     */
    public ArrayList<Reuniao> getListaReunioes() {
        return listaReunioes;
    }

    /**
     * Método selector da classe
     *
     * @return double - Volume de faturação do condomínio
     */
    public double getVolumeFaturacao() {
        return volumeFaturacao;
    }

    /**
     * Método selector da classe
     *
     * @return double - Volume de faturação do condomínio por periodo
     */
    public double getVolumeFaturacaoPeriodo() {
        return volumeFaturacaoPeriodo;
    }

    /**
     * Método modificador da classe: Altera o nome do condomínio
     *
     * @param novoNome Novo nome do condomínio
     */
    public void setNome(String novoNome) {
        nome = novoNome;
    }

    /**
     * Método modificador da classe: Altera a morada do condomínio
     *
     * @param novaMorada Nova morada do condomínio
     */
    public void setMorada(String novaMorada) {
        morada = novaMorada;
    }

    /**
     * Método modificador da classe: Altera o código postal (4 dígitos) do
     * condomínio
     *
     * @param novoCodigoPostal1 Novo código postal (4 dígitos) do condomínio
     */
    public void setCodigoPostal1(long novoCodigoPostal1) {
        codigoPostal1 = novoCodigoPostal1;
    }

    /**
     * Método modificador da classe: Altera o código postal (3 dígitos) do
     * condomínio
     *
     * @param novoCodigoPostal2 Novo código postal (3 dígitos) do condomínio
     */
    public void setCodigoPostal2(String novoCodigoPostal2) {
        codigoPostal2 = novoCodigoPostal2;
    }

    /**
     * Método modificador da classe: Altera a localidade do condomínio
     *
     * @param novaLocalidade Nova localidade do condomínio
     */
    public void setLocalidade(String novaLocalidade) {
        localidade = novaLocalidade;
    }

    /**
     * Método modificador da classe: Altera o valor da estimativa das despesas
     * do condomínio
     *
     * @param novasDespesas Novo valor da estimativa das despesas do condomínio
     */
    public void setEstimativaDespesas(double novasDespesas) {
        estimativaDespesas = novasDespesas;
        updateValorCondominio();
    }

    /**
     * Método modificador da classe: Altera o valor estimado para as despesas
     * administrativas do condomínio
     *
     * @param novoValorDespesasAdministrativas Novo valor da estimativa das
     * despesas administrativas do condomínio
     */
    public void setValorDespesasAdministrativas(double novoValorDespesasAdministrativas) {
        valorDespesasAdministrativas = novoValorDespesasAdministrativas;
    }

    /**
     * Método modificador da classe: Altera o valor estimado para as despesas
     * com a limpeza do condomínio
     *
     * @param novoValorLimpeza Novo valor da estimativa das despesas com a
     * limpeza do condomínio
     */
    public void setValorLimpezas(double novoValorLimpeza) {
        valorLimpezas = novoValorLimpeza;
    }

    /**
     * Método modificador da classe: Altera o valor estimado para as despesas
     * com as reparações do condomínio
     *
     * @param novoValorReparacoes Novo valor da estimativa das despesas com as
     * reparações do condomínio
     */
    public void setValorReparacoes(double novoValorReparacoes) {
        valorReparacoes = novoValorReparacoes;
    }

    /**
     * Método modificador da classe: Altera o valor estimado para as despesas
     * com a manutenção dos elevadores do condomínio
     *
     * @param novoValorElevadores Novo valor da estimativa das despesas com a
     * manutenção dos elevadores do condomínio
     */
    public void setValorElevadores(double novoValorElevadores) {
        valorElevadores = novoValorElevadores;
    }

    /**
     * Método modificador da classe: Altera o valor estimado para outras
     * despesas do condomínio
     *
     * @param novoValorOutras Novo valor da estimativa de outras despesas do
     * condomínio
     */
    public void setValorOutras(double novoValorOutras) {
        valorOutras = novoValorOutras;
    }

    /**
     * Método modificador da classe Altera o valor dos honorários da empresa
     * Condominus
     *
     * @param novosHonorarios Novo valor dos honorários da empresa Condominus
     */
    public void setHonorarios(double novosHonorarios) {
        honorarios = novosHonorarios;
        updateValorCondominio();
    }

    /**
     * Método modificador da classe: Atualiza o valor do condomínio
     */
    public void updateValorCondominio() {
        valorCondominio = estimativaDespesas + honorarios;
        calculoValorFracao();
    }

    /**
     * Método modificador da classe - Altera a modalidade de cálculo do valor da
     * quota
     *
     * @param novaModalidadeCalculoQuota Nova modalidade de cálculo do valor da
     * quota
     */
    public void setModalidadeCalculoQuota(int novaModalidadeCalculoQuota) {
        modalidadeCalculoQuota = novaModalidadeCalculoQuota;
        calculoValorFracao();
    }

    /**
     * Método modificador da classe - Altera a o volume de faturação de um
     * determinado condomínio
     *
     * @param novoVolumeFaturacao Novo volume de faturação de um determinado
     * condomínio
     */
    public void setVolumeFAturacao(double novoVolumeFaturacao) {
        volumeFaturacao = novoVolumeFaturacao;
    }

    /**
     * Método modificador da classe - Altera a o volume de faturação de um
     * determinado condomínio por período de tempo
     *
     * @param novoVolumeFaturacaoPeriodo Novo volume de faturação de um
     * determinado condomínio por periodo de tempo
     */
    public void setVolumeFAturacaoPeriodo(double novoVolumeFaturacaoPeriodo) {
        volumeFaturacaoPeriodo = novoVolumeFaturacaoPeriodo;
    }

    /**
     * Método criado para determinar a forma de calcular o valor da quota de
     * cada fracção face à modalidade de pagamento escolhida
     */
    public void calculoValorFracao() {
        switch (modalidadeCalculoQuota) {
            case 0:
                calculoPermilagem();
                break; // Calculo da quota por permilagem
            case 1:
                calculoEquitativa();
                break; // Cálculo da quota de forma equitativa
            case 2:
                calculoExacta();
                break; // Cálculo da quota de forma exacta
            case 3:
                break; // Valor a ser definido manualmente
        }
    }

    /**
     * Método criado para determinar a quota de cada fração através do método:
     * permilagem
     */
    public void calculoPermilagem() {
        for (int i = 0; i < listaCondominos.size(); i++) {
            double valorQuota = (valorCondominio / 1000)
                    * listaCondominos.get(i).getFracao().getPermilagem();
            valorQuota = Math.round(valorQuota * 100) / 100;
            listaCondominos.get(i).getFracao().setQuota(valorQuota);
        }
    }

    /**
     * Método criado para determinar a quota de cada fração através do método:
     * equitativa
     */
    public void calculoEquitativa() {

        // Esta porção de código determina o andar máximo do condomínio
        int maxAndares = 0;
        for (int i = 0; i < listaCondominos.size(); i++) {
            if (listaCondominos.get(i).getFracao().getAndar() > maxAndares) {
                maxAndares = listaCondominos.get(i).getFracao().getAndar();
            }
        }

        /* Esta porção de código guarda num array o número de frações existentes
        em cada andar
         */
        int[] fracoesAndares = new int[maxAndares + 1];
        int indice = 0;
        int contador = 0;
        for (int i = 0; i < listaCondominos.size(); i++) {
            if (listaCondominos.get(i).getFracao().getAndar() == indice) {
                if ((i + 1 != listaCondominos.size()) && (listaCondominos.get(i + 1).getFracao().getAndar() == indice)) {
                    contador += 1;
                } else {
                    contador += 1;
                    fracoesAndares[listaCondominos.get(i).getFracao().getAndar()] = contador;
                    contador = 0;
                }
            } else {
                indice += 1;
                i -= 1;
            }
        }

        // Esta porção de código calcula o valor da quota para cada fração
        for (int i = 0; i < listaCondominos.size(); i++) {
            double valorQuota = (valorCondominio / fracoesAndares[listaCondominos.get(i).getFracao().getAndar()])
                    * listaCondominos.get(i).getFracao().getTaxaUtilizacao();
            valorQuota = Math.round(valorQuota * 100) / 100;
            listaCondominos.get(i).getFracao().setQuota(valorQuota);
        }
    }

    /**
     * Método criado para determinar a quota de cada fração através do método:
     * exacta
     */
    public void calculoExacta() {
        double valorQuota = valorCondominio
                / listaCondominos.size();
        valorQuota = Math.round(valorQuota * 100) / 100;
        for (int i = 0; i < listaCondominos.size(); i++) {
            listaCondominos.get(i).getFracao().setQuota(valorQuota);
        }
    }

    /**
     * Método para registar um novo condómino
     *
     * @param novoCondomino Novo condómino a ser registado
     */
    public void registarCondomino(Condomino novoCondomino) {
        listaCondominos.add(novoCondomino);
        calculoValorFracao();
    }

    /**
     * Método para eliminar um determinado condómino
     *
     * @param condominoEliminar Condómino a ser eliminado
     */
    public void eliminarCondomino(Condomino condominoEliminar) {
        listaCondominos.remove(condominoEliminar);
        calculoValorFracao();

    }

    /**
     * Método para agendar uma nova reunião
     *
     * @param novaReuniao Reunião a ser agendada
     */
    public void agendarReuniao(Reuniao novaReuniao) {
        listaReunioes.add(novaReuniao);
        ordenarListaReunioes();
    }

    /**
     * Método para ordenar a lista de reuniões (ascendente por data)
     *
     */
    public void ordenarListaReunioes() {
        Collections.sort(listaReunioes, (Reuniao o1,
                Reuniao o2) -> o1.getData().compareTo(o2.getData()));
    }

    /**
     * Método para desagendar uma determinada reunião
     *
     * @param reuniaoDesagendar Reunião a ser desagendada
     */
    public void desagendarReuniao(Reuniao reuniaoDesagendar) {
        listaReunioes.remove(reuniaoDesagendar);
    }

    /**
     * Método para agendar uma nova atividade de manutenção
     *
     * @param novaAtivManut Atividade de manutenção a ser agendada
     */
    public void agendarAtivManut(AtividadeManutencao novaAtivManut) {
        listaAtivManut.add(novaAtivManut);
        ordenarListaAtivManut();
    }

    /**
     * Método para ordenar a lista de atividades de manutenção (ascendente por
     * data)
     *
     */
    public void ordenarListaAtivManut() {
        Collections.sort(listaAtivManut, (AtividadeManutencao o1,
                AtividadeManutencao o2) -> o1.getData().compareTo(o2.getData()));
    }

    /**
     * Método para desagendar uma determinada atividade de manutenção
     *
     * @param ativManutDesagendar Atividade de manutenção a ser desagendada
     */
    public void desagendarAtivManut(AtividadeManutencao ativManutDesagendar) {
        listaAtivManut.remove(ativManutDesagendar);
    }

    /**
     * Método para calcular o volume de faturação de um condomínio
     *
     * @return double - Volume de faturação de um condomínio
     */
    public double calcularVolumeFacturacao() {
        volumeFaturacao = 0;
        for (int i = 0; i < listaCondominos.size(); i++) {
            for (int j = 0; j < listaCondominos.get(i).getListaFaturas().size(); j++) {
                if (listaCondominos.get(i).getListaFaturas().get(j).isPaga() == true) {
                    volumeFaturacao = volumeFaturacao + listaCondominos.get(i).
                            getListaFaturas().get(j).getValorMensalidade();
                }
            }
        }
        return volumeFaturacao;
    }

    /**
     * Método para calcular o volume de faturação de um condomínio num
     * determinado período de tempo
     *
     * @param inicio Data para inicio do cálculo
     * @param fim Data para fim do cálculo
     * @return double - Volume de faturação de um condomínio num determinado
     * período de tempo
     */
    public double calcularVolumeFacturacaoPeriodo(Date inicio, Date fim) {
        volumeFaturacaoPeriodo = 0;
        for (int i = 0; i < listaCondominos.size(); i++) {
            for (int j = 0; j < listaCondominos.get(i).getListaFaturas().size(); j++) {
                if (listaCondominos.get(i).getListaFaturas().get(j).isPaga() == true
                        && (listaCondominos.get(i).getListaFaturas().get(j).getdataPagamento().compareTo(inicio) >= 0
                        && fim.compareTo(listaCondominos.get(i).getListaFaturas().get(j).getdataPagamento()) >= 0)) {
                    volumeFaturacaoPeriodo = volumeFaturacaoPeriodo + listaCondominos.get(i).
                            getListaFaturas().get(j).getValorMensalidade();
                }
            }
        }
        return volumeFaturacaoPeriodo;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * do condomínio
     *
     * @return String - Informação do condomínio
     */
    @Override
    public String toString() {
        String modalidade = "";
        if (modalidadeCalculoQuota == 0) {
            modalidade = "Permilagem";
        } else if (modalidadeCalculoQuota == 1) {
            modalidade = "Equitativa";
        } else if (modalidadeCalculoQuota == 2) {
            modalidade = "Exacta";
        } else {
            modalidade = "Manual";
        }

        StringBuilder s = new StringBuilder();
        s.append("\n                                                       1.Informação do Condomínio\n\n");
        s.append("Nome do Condomínio: " + nome + "\n");
        s.append("Morada: " + morada + " " + codigoPostal1 + "-" + codigoPostal2
                + " " + localidade + "\n");
        s.append("Método de Cálculo do valor da Quota: " + modalidade + "\n");
        s.append("Despesas Estimadas:\n");
        s.append("        ** Despesas Administrativas................................ " + valorDespesasAdministrativas + "€\n");
        s.append("        ** Despesas em Limpezas................................... " + valorLimpezas + "€\n");
        s.append("        ** Despesas em Pequenas Reparações.............. " + valorReparacoes + "€\n");
        s.append("        ** Despesas Manutenção de Elevadores............. " + valorElevadores + "€\n");
        s.append("        ** Honorários........................................................ " + honorarios + "€\n");
        s.append("        ** Valor do Condomínio........................................ " + valorCondominio + "€\n");
        s.append("\n                                                       2.Informação dos Condóminos\n\n");
        for (int i = 0; i < listaCondominos.size(); i++) {
            s.append(listaCondominos.get(i).toString());
        }
        s.append("                                              3.Informação das Atividades de Manutenção\n\n");
        Date hoje = new Date();
        for (int i = 0; i < listaAtivManut.size(); i++) {
            s.append(listaAtivManut.get(i).toString());
        }
        s.append("                                              4.Informação do Agendamento de Reuniões\n\n");
        for (int i = 0; i < listaReunioes.size(); i++) {
            s.append(listaReunioes.get(i).toString());
        }
        return s.toString();
    }

    /**
     * Método para criar uma string com toda a informação de um condomínio
     * orçamentado
     *
     * @return String - Informação do condomínio orçamentado
     */
    public String toStringOrcamento() {
        String modalidade = "";
        switch (modalidadeCalculoQuota) {
            case 0:
                modalidade = "Permilagem";
                break;
            case 1:
                modalidade = "Equitativa";
                break;
            case 2:
                modalidade = "Exacta";
                break;
            default:
                modalidade = "Manual";
                break;
        }

        StringBuilder s = new StringBuilder();
        s.append("\n                                                       1.Informação do Condomínio\n\n");
        s.append("Nome do Condomínio: " + nome + "\n");
        s.append("Morada: " + morada + " " + codigoPostal1 + "-" + codigoPostal2
                + " " + localidade + "\n");
        s.append("Método de Cálculo do valor da Quota: " + modalidade + "\n");
        s.append("Despesas Estimadas:\n");
        s.append("        ** Despesas Administrativas................................ " + valorDespesasAdministrativas + "€\n");
        s.append("        ** Despesas em Limpezas................................... " + valorLimpezas + "€\n");
        s.append("        ** Despesas em Pequenas Reparações.............. " + valorReparacoes + "€\n");
        s.append("        ** Despesas Manutenção de Elevadores............. " + valorElevadores + "€\n");
        s.append("        ** Honorários........................................................ " + honorarios + "€\n");
        s.append("        ** Valor do Condomínio........................................ " + valorCondominio + "€\n");
        s.append("\n                                                       2.Informação das Frações\n\n");
        for (int i = 0; i < listaCondominos.size(); i++) {
            s.append(listaCondominos.get(i).toStringOrcamento());
            s.append("\n");
        }
        return s.toString();
    }

    /**
     * Override do método compare da Interface Compare
     *
     */
    @Override
    public int compare(Object o1, Object o2) {
        throw new UnsupportedOperationException("Not supported yet.");
        // Override para compilação sem erros
    }
}
