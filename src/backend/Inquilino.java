package backend;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A existência da classe Inquilino permite a criação dos inquilínos que não são
 * proprietários das frações que representam cada condómino de um condomínio
 *
 * @author JoaoFerreira
 */
public class Inquilino implements Serializable {

    // Variáveis de Instância 
    private String nome, email, contactoTelefonico, profissao;
    private Date dataNascimento;

    /**
     * Método construtor da classe
     *
     * @param nome Nome do inquilíno
     * @param email Endereço electrónico do inquilíno
     * @param contactoTelefonico Contacto telefónico do inquilíno
     * @param dataNascimento Data de nascimento do inquilíno
     * @param profissao Profissão do inquilíno
     */
    public Inquilino(String nome, String email,
            String contactoTelefonico, Date dataNascimento,
            String profissao) {
        this.nome = nome;
        this.email = email;
        this.contactoTelefonico = contactoTelefonico;
        this.dataNascimento = dataNascimento;
        this.profissao = profissao;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Nome do inquilíno
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Endereço electrónico do inquilíno
     */
    public String getEmail() {
        return email;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Contacto telefónico do inquilíno
     */
    public String getContactoTelefonico() {
        return contactoTelefonico;
    }

    /**
     * Método seletor da classe
     *
     * @return Date - Data de nascimento do inquilíno
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Profissão do inquilíno
     */
    public String getProfissao() {
        return profissao;
    }

    /**
     * Método modificador da classe: Altera o nome do inquilíno
     *
     * @param novoNome Novo nome do inquilíno
     */
    public void setNome(String novoNome) {
        nome = novoNome;
    }

    /**
     * Método modificador da classe: Altera o endereço electrónico do inquilíno
     *
     * @param novoEmail Novo endereço electrónico do inquilíno
     */
    public void setEmail(String novoEmail) {
        email = novoEmail;
    }

    /**
     * Método modificador da classe: Altera o contacto telefónico do inquilíno
     *
     * @param novoContactoTelefonico Novo contacto telefónico do inquilíno
     */
    public void setContactoTelefonico(String novoContactoTelefonico) {
        contactoTelefonico = novoContactoTelefonico;
    }

    /**
     * Método modificador da classe: Altera a profissão do inquilíno
     *
     * @param novaProfissao Nova profissão do inquilíno
     */
    public void setProfissao(String novaProfissao) {
        profissao = novaProfissao;
    }

    /**
     * Método modificador da classe: Altera a data de nascimento do inquilíno
     *
     * @param novaDataNascimento Nova data de nascimento do inquilíno
     */
    public void setDataNascimento(Date novaDataNascimento) {
        dataNascimento = novaDataNascimento;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * do inquilíno
     *
     * @return String - Informação do inquilíno
     */
    @Override
    public String toString() {
        String stringData = new SimpleDateFormat("yyyy-MM-dd").format(dataNascimento);
        StringBuilder s = new StringBuilder("");
        s.append("        ** Nome: " + nome + "\n");
        s.append("        ** email: " + email + "\n");
        s.append("        ** Contacto Telefónico: " + contactoTelefonico + "\n");
        s.append("        ** Data Nascimento: " + stringData + "\n");
        s.append("        ** Profissão: " + profissao + "\n");
        return s.toString();
    }
}
