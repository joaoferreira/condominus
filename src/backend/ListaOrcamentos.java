package backend;

import java.util.ArrayList;
import java.io.Serializable;

/**
 * A existência da classe ListaOrcamentos permite a criação de uma ArrayList de
 * orçamentos que devido ao facto de implementar o interface Serializable
 * permite a gravação dos orçamentos dos possíveis clientes da empresa
 * Condominus em ficheiro
 *
 * @author JoaoFerreira
 */
public class ListaOrcamentos implements Serializable {

    // Variáveis de instância 
    private final ArrayList<Orcamento> listaOrcamentos;
    int numeracao = 0;

    /**
     * Método construtor da classe
     */
    public ListaOrcamentos() {
        listaOrcamentos = new ArrayList<>();
    }

    /**
     * Método para registar um novo orçamento
     *
     * @param novoOrcamento Novo condomínio a ser registado
     */
    public void registarOrcamento(Orcamento novoOrcamento) {
        listaOrcamentos.add(novoOrcamento);
        numeracao += 1;
        atribuirNumeracao(novoOrcamento);
    }

    /**
     * Método para eliminar um determinado orçamento
     *
     * @param orcamentoEliminar Orçamento a ser eliminado
     */
    public void eliminarOrcamento(Orcamento orcamentoEliminar) {
        listaOrcamentos.remove(orcamentoEliminar);

    }

    /**
     * Método uulizador para fazer a numeração automática e incremental dos
     * orçamentos
     *
     * @param novoOrcamento - Orçamento a ser numerado
     */
    public void atribuirNumeracao(Orcamento novoOrcamento) {
        novoOrcamento.setNumeroOrcamento(numeracao);
    }

    /**
     * Override do método get da classe ArrayList
     *
     * @param i Índice
     * @return Orcamento - Orçamento selecionado
     */
    public Orcamento get(int i) {
        return listaOrcamentos.get(i);
    }

    /**
     * Override do método size da classe ArrayList
     *
     * @return int - Tamanho da lista de orçamentos
     */
    public int size() {
        return listaOrcamentos.size();
    }
}
