package backend;

import java.io.Serializable;

/**
 * A existência da classe Orcamento permite a criação dos orçamentos da empresa
 * Condominus
 *
 * @author JoaoFerreira
 */
public class Orcamento implements Serializable {

    // Variáveis de instância 
    private int numeroOrcamento;
    private Condominio condominioOrcamentado;

    /**
     * Método construtor da classe
     *
     * @param condominioOrcamentado - Condomínio a ser orçamentado
     */
    public Orcamento(Condominio condominioOrcamentado) {
        this.condominioOrcamentado = condominioOrcamentado;
        this.numeroOrcamento = 0;
    }

    /**
     * Método seletor da classe
     *
     * @return Condominio - Condomínio orçamentado
     */
    public Condominio getCondominioOrcamentado() {
        return condominioOrcamentado;
    }

    /**
     * Método seletor da classe
     *
     * @return int - Número do orçamento
     */
    public int getNumeroOrcamento() {
        return numeroOrcamento;
    }

    /**
     * Método modificador da classe: Altera o condomínio orçamentado
     *
     * @param novoCodominioOrcamentado Novo condomínio orçamentado
     */
    public void setCondominioOrcamentado(Condominio novoCodominioOrcamentado) {
        condominioOrcamentado = novoCodominioOrcamentado;
    }

    /**
     * Método modificador da classe: Altera o número do orçamento
     *
     * @param novoNumeroOrcamento Novo número do orçamento
     */
    public void setNumeroOrcamento(int novoNumeroOrcamento) {
        numeroOrcamento = novoNumeroOrcamento;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * do orçamento
     *
     * @return String - Informação do orçamento
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("\n                                                                Orçamento n.º" + numeroOrcamento + "\n\n");
        s.append(condominioOrcamentado.toStringOrcamento());
        return s.toString();
    }
}
