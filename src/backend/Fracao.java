package backend;

import java.io.Serializable;

/**
 * A existência da classe Fracao permite a criação das frações que representam
 * cada condómino de um condomínio
 *
 * @author JoaoFerreira
 */
public class Fracao implements Serializable {

    // Variáveis de instância
    private String designacao;
    private double permilagem, taxaUtilizacao, quota;
    private int andar;

    /**
     * Método construtor da classe
     *
     * @param designacao Designação da fração
     * @param permilagem Valor da permilagem da fração
     * @param andar Andar da fração
     * @param taxaUtilizacao Valor da taxa de utilização da fração
     */
    public Fracao(String designacao, double permilagem, int andar,
            double taxaUtilizacao) {
        this.designacao = designacao;
        this.permilagem = permilagem;
        this.andar = andar;
        this.taxaUtilizacao = taxaUtilizacao;
        quota = 0;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Designação da fração
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * Método seletor da classe
     *
     * @return Double - Permilagem da fração
     */
    public double getPermilagem() {
        return permilagem;
    }

    /**
     * Método seletor da classe
     *
     * @return Int - Andar da fração
     */
    public int getAndar() {
        return andar;
    }

    /**
     * Método seletor da classe
     *
     * @return Double - Taxa de utilização da fração
     */
    public double getTaxaUtilizacao() {
        return taxaUtilizacao;
    }

    /**
     * Método seletor da classe
     *
     * @return double - Valor da quota da fração
     */
    public double getQuota() {
        return quota;
    }

    /**
     * Método modificador da classe: Altera a designação da fração
     *
     * @param novaDesignacao Nova designação da fração
     */
    public void setDesignacao(String novaDesignacao) {
        designacao = novaDesignacao;
    }

    /**
     * Método modificador da classe: Altera a permilagem da fração
     *
     * @param novaPermilagem Novo valor da permilagem da fração
     */
    public void setPermilagem(double novaPermilagem) {
        permilagem = novaPermilagem;
    }

    /**
     * Método modificador da classe: Altera o andar da fração
     *
     * @param novoAndar Novo andar da fração
     */
    public void setAndar(int novoAndar) {
        andar = novoAndar;
    }

    /**
     * Método modificador da classe: Altera a taxa de utilização da fração
     *
     * @param novaTaxaUtilizacao Nova taxa de utilização da fração
     */
    public void setTaxaUtilizacao(double novaTaxaUtilizacao) {
        taxaUtilizacao = novaTaxaUtilizacao;
    }

    /**
     * Método modificador da classe: Altera o valor da quota da fração
     *
     * @param novoValorQuota Novo valor da quota da fração
     */
    public void setQuota(double novoValorQuota) {
        quota = novoValorQuota;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * da fração
     *
     * @return String - Informação da fração
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("");
        s.append("        ** Fração: " + designacao + "\n");
        s.append("        ** Permilagem: " + permilagem + "\n");
        s.append("        ** Taxa de Utilização: " + taxaUtilizacao + "\n");
        s.append("        ** Andar: " + andar + "\n");
        s.append("        ** Quota: " + quota + "\n");
        return s.toString();
    }
}
