package backend;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A existência da classe Proprietario permite a criação dos proprietários das
 * frações que representam cada condómino de um determinado condomínio
 *
 * @author JoaoFerreira
 */
public class Proprietario implements Serializable {

    // Variáveis de instância
    private String nome, morada, email, contactoTelefonico, nif, profissao;
    private Date dataNascimento;

    /**
     * Método construtor da classe
     *
     * @param nome Nome do proprietário
     * @param morada Morada do proprietário
     * @param email Endereço electrónico do proprietário
     * @param contactoTelefonico Contacto telefónico do proprietário
     * @param nif Número de identificaçao fiscal do proprietário
     * @param dataNascimento Data de nascimento do proprietário
     * @param profissao Profissão do proprietário
     */
    public Proprietario(String nome, String morada, String email,
            String contactoTelefonico, String nif, Date dataNascimento,
            String profissao) {
        this.nome = nome;
        this.morada = morada;
        this.email = email;
        this.contactoTelefonico = contactoTelefonico;
        this.nif = nif;
        this.dataNascimento = dataNascimento;
        this.profissao = profissao;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Nome do proprietário
     */
    public String getNome() {
        return nome;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Morada do proprietário
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Endereço electrónico do proprietário
     */
    public String getEmail() {
        return email;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Contacto telefónico do proprietário
     */
    public String getContactoTelefonico() {
        return contactoTelefonico;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Número de identificação fiscal do proprietário
     */
    public String getNif() {
        return nif;
    }

    /**
     * Método seletor da classe
     *
     * @return Date - Data de nascimento do proprietário
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Profissão do proprietário
     */
    public String getProfissao() {
        return profissao;
    }

    /**
     * Método modificador da classe: Altera o nome do proprietário
     *
     * @param novoNome Novo nome do proprietário
     */
    public void setNome(String novoNome) {
        nome = novoNome;
    }

    /**
     * Método modificador da classe: Altera a morada do proprietário
     *
     * @param novaMorada Nova morada do proprietário
     */
    public void setMorada(String novaMorada) {
        morada = novaMorada;
    }

    /**
     * Método modificador da classe: Altera o endereço electrónico do
     * proprietário
     *
     * @param novoEmail Novo endereço electrónico do proprietário
     */
    public void setEmail(String novoEmail) {
        email = novoEmail;
    }

    /**
     * Método modificador da classe: Altera o contacto telefónico do
     * proprietário
     *
     * @param novoContactoTelefonico Novo contacto telefónico do proprietário
     */
    public void setContactoTelefonico(String novoContactoTelefonico) {
        contactoTelefonico = novoContactoTelefonico;
    }

    /**
     * Método modificador da classe: Altera o número de identificação fiscal do
     * proprietário
     *
     * @param novoNif Novo número de identificação fiscal do proprietário
     */
    public void setNif(String novoNif) {
        nif = novoNif;
    }

    /**
     * Método modificador da classe: Altera a data de nascimento do proprietário
     *
     * @param novaDataNascimento Nova data de nascimento do proprietário
     */
    public void setDataNascimento(Date novaDataNascimento) {
        dataNascimento = novaDataNascimento;
    }

    /**
     * Método modificador da classe: Altera a profissão do proprietário
     *
     * @param novaProfissao Nova profissão do proprietário
     */
    public void setProfissao(String novaProfissao) {
        profissao = novaProfissao;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * do proprietário
     *
     * @return String - Informação do proprietário
     */
    @Override
    public String toString() {
        String stringData = new SimpleDateFormat("yyyy-MM-dd").format(dataNascimento);
        StringBuilder s = new StringBuilder("");
        s.append("        ** Nome: " + nome + "\n");
        s.append("        ** Morada: " + morada + "\n");
        s.append("        ** email: " + email + "\n");
        s.append("        ** Contacto Telefónico: " + contactoTelefonico + "\n");
        s.append("        ** NIF: " + nif + "\n");
        s.append("        ** Data Nascimento: " + stringData + "\n");
        s.append("        ** Profissão: " + profissao + "\n");
        return s.toString();
    }
}
