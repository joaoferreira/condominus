package backend;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * A existência da classe Fatura permite a criação e manipulação das diversas
 * faturas dos respectivos condóminos e seus proprietários
 *
 * @author JoaoFerreira
 */
public class Fatura implements Serializable {

    // Variáveis de Instância
    private final Proprietario proprietario;
    private final Fracao fracao;
    private int mensalidade, diasAtraso;
    private double taxaAgravamento;
    private String estado;
    private double valorMensalidade;
    private boolean emAtraso, paga, pagavel;
    private final GregorianCalendar dataAtraso, dataLimite;
    private Date dataPagamento;

    /**
     * Método construtor da classe
     *
     * @param propietario Propriatário da fração
     * @param fracao Fração associada à fatura
     * @param limitePagamento Limite de pagamento (dia do mês)
     * @param diasAtraso Dias de atraso plausível para aceitação do pagamento
     * das mensalidades em atraso
     * @param mensalidade Mês da fatura
     * @param taxaAgravamento Taxa de agravamento a ser aplicada em caso de
     * mensalidade em atraso
     * @param valorMensalidade Valor da mensalidade (sem aplicação de taxas de
     * agravamento)
     */
    public Fatura(Proprietario propietario, Fracao fracao, int limitePagamento,
            int diasAtraso, int mensalidade, double taxaAgravamento, double valorMensalidade) {

        this.proprietario = propietario;
        this.fracao = fracao;
        this.mensalidade = mensalidade;
        this.dataAtraso = new GregorianCalendar();
        this.dataLimite = new GregorianCalendar();
        this.valorMensalidade = valorMensalidade;
        this.diasAtraso = diasAtraso;
        this.taxaAgravamento = taxaAgravamento;
        this.dataPagamento = null;

        pagavel = true;
        emAtraso = false;
        paga = false;

        GregorianCalendar agora = new GregorianCalendar();
        dataAtraso.set(agora.get(agora.YEAR), mensalidade, limitePagamento);
        dataLimite.set(agora.get(agora.YEAR), mensalidade, limitePagamento + diasAtraso);

        if (agora.compareTo(dataAtraso) > 0 && agora.compareTo(dataLimite) < 0) {
            emAtraso = true;
            pagavel = true;
        } else if (agora.compareTo(dataLimite) > 0) {
            pagavel = false;
            emAtraso = true;
        } else {
            emAtraso = false;
            pagavel = true;
        }

        calcularValorMensalidade(taxaAgravamento);
    }

    /**
     * Método seletor da classe
     *
     * @return int - Mensalidade da fatura
     */
    public int getMensalidade() {
        return mensalidade;
    }

    /**
     * Método seletor da classe
     *
     * @return Double - Valor da mensalidade
     */
    public double getValorMensalidade() {
        return valorMensalidade;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Estado da mensalidade
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Método seletor da classe
     *
     * @return int - Dias de atraso aceites para pagamento de mensalidades em
     * atraso
     */
    public int getDiasAtraso() {
        return diasAtraso;
    }

    /**
     * Método seletor da classe
     *
     * @return Double - Taxa de agravamento a aplicar às mensalidade em atraso
     */
    public Double getTaxaAgravamaento() {
        return taxaAgravamento;
    }

    /**
     * Método seletor da classe
     *
     * @return Date - Data de pagamento
     */
    public Date getdataPagamento() {
        return dataPagamento;
    }

    /**
     * Método seletor da classe
     *
     * @return Boolean - true(se fatura em atraso), false(se fatura em dia)
     */
    public boolean isEmAtraso() {
        return emAtraso;
    }

    /**
     * Método seletor da classe
     *
     * @return Boolean - true(se fatura paga), false(se fatura por pagar)
     */
    public boolean isPaga() {
        return paga;
    }

    /**
     * Método seletor da classe
     *
     * @return Boolean - true(se fatura ainda pagável), false(se fatura não
     * pagável)
     */
    public boolean isPagavel() {
        return pagavel;
    }

    /**
     * Método seletor da classe
     *
     * @return GregorianCalendar - Data a partir da qual a fatura se encontra em
     * atraso de pagamento
     */
    public GregorianCalendar getDataAtraso() {
        return dataAtraso;
    }

    /**
     * Método seletor da classe
     *
     * @return GregorianCalendar - Data até quando é possível efetuar um
     * pagamento (em atraso)
     */
    public GregorianCalendar getDataLimite() {
        return dataLimite;
    }

    /**
     * Método modificador da classe: Altera o mês da mensalidade da fatura
     *
     * @param novaMensalidade Novo mensalidade (mês) associado à fatura
     */
    public void setMensalidade(int novaMensalidade) {
        mensalidade = novaMensalidade;
    }

    /**
     * Método modificador da classe: Altera o estado da mensalidade
     *
     * @param novoEstado - Novo estado da mensalidade
     */
    public void setEstado(String novoEstado) {
        estado = novoEstado;
    }

    /**
     * Método modificador da classe: Altera os dias de aceitação de pagamento de
     * mensalidades em atraso
     *
     * @param novosDiasAtraso - Novo valor de dias para aceitação de pagamentos
     * de mensalidade em atraso
     */
    public void setDiasAtraso(int novosDiasAtraso) {
        diasAtraso = novosDiasAtraso;
    }

    /**
     * Método modificador da classe: Altera a data de pagamento de uma
     * determinada mensalidade
     *
     * @param novaDataPagamento - Novo valor de dias para aceitação de
     * pagamentos de mensalidade em atraso
     */
    public void setDataPagamento(Date novaDataPagamento) {
        dataPagamento = novaDataPagamento;
    }

    /**
     * Método modificador da classe: Altera os a taxa de agravamento a ser
     * aplicada às mensalidades em atraso
     *
     * @param novaTaxaAgravamento - Nova taxa de agravamento a ser aplicada às
     * mensalidades em atraso
     */
    public void setTaxaAgravamento(Double novaTaxaAgravamento) {
        taxaAgravamento = novaTaxaAgravamento;
    }

    /**
     * Método modificador da classe: Altera o estado "Em Atraso" da fatura
     *
     * @param emAtraso - Modifica o estado de "Em Atraso" da fatura
     */
    public void setEmAtraso(boolean emAtraso) {
        this.emAtraso = emAtraso;
    }

    /**
     * Método modificador da classe: Altera o estado "Paga" da fatura
     *
     * @param paga - Modifica o estado de "Paga" da fatura
     */
    public void setPaga(boolean paga) {
        this.paga = paga;
    }

    /**
     * Método modificador da classe: Altera o estado "Pagável" da fatura
     *
     * @param pagavel - Modifica o estado de "Pagável" da fatura
     */
    public void setPagavel(boolean pagavel) {
        this.pagavel = pagavel;
    }

    /**
     * Método modificador da classe: Altera a data a partir da qual uma fatura
     * se encontra em atraso
     *
     * @param novoLimitePagamento Novo dia para limite de pagamento das
     * mensalidades
     */
    public void setDataAtraso(int novoLimitePagamento) {
        GregorianCalendar agora = new GregorianCalendar();
        dataAtraso.set(agora.get(agora.YEAR), mensalidade, novoLimitePagamento);
    }

    /**
     * Método modificador da classe: Altera a data até à qual se aceita o
     * pagamento de uma mensalidade em atraso
     *
     * @param novoLimitePagamento Novo dia para limite de pagamento das
     * mensalidades
     * @param novosDiasAtraso Valor de dias aceitáveis para pagamento de
     * mensalidades em atraso
     */
    public void setDataLimite(int novoLimitePagamento, int novosDiasAtraso) {
        GregorianCalendar agora = new GregorianCalendar();
        dataLimite.set(agora.get(agora.YEAR), mensalidade, novoLimitePagamento + novosDiasAtraso);
    }

    /**
     * Método que calcula o valor e o estado da mensalidade da fatura
     *
     * @param taxaAgravamento Valor da taxa de agravamento a ser aplicada ao
     * valor da mensalidade em caso de atraso de pagamento
     */
    public void calcularValorMensalidade(double taxaAgravamento) {
        if (pagavel && !emAtraso) {
            valorMensalidade = valorMensalidade;
            estado = "Pagamento dentro do prazo";
        } else if (pagavel && emAtraso) {
            valorMensalidade = valorMensalidade * (1 + taxaAgravamento);
            valorMensalidade = Math.round(valorMensalidade * 100) / 100;
            estado = "Mensalidade em atraso";
        } else if (emAtraso && !pagavel) {
            valorMensalidade = valorMensalidade * (1 + taxaAgravamento);
            valorMensalidade = Math.round(valorMensalidade * 100) / 100;
            estado = "Impossivel realizar pagamento, fora do prazo";
        }
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * da fatura
     *
     * @return String - Informação da fatura
     */
    @Override
    public String toString() {
        GregorianCalendar agora = new GregorianCalendar();
        StringBuilder s = new StringBuilder("");
        s.append("\n\n\nFatura Nº" + (mensalidade + 1) + "/" + agora.get(agora.YEAR) + "\n");
        s.append("Documento Original\n\n");
        s.append("--------------------------------------------------------------"
                + "--------------------------------------------------------------------\n\n");
        s.append(proprietario.getNome() + "\n");
        s.append(proprietario.getMorada() + "\n");
        s.append(proprietario.getNif() + "\n");
        s.append("Quota da Fração:" + fracao.getDesignacao() + "\n");
        s.append("Mensalidade: " + (mensalidade + 1) + "\n");
        s.append("Valor da Mensalidade: " + valorMensalidade + " euros\n\n");
        s.append("--------------------------------------------------------------"
                + "--------------------------------------------------------------------\n\n");
        s.append(agora.get(agora.DAY_OF_MONTH) + "-"
                + (agora.get(agora.MONTH) + 1) + "-" + agora.get(agora.YEAR) + "\n\n");
        s.append("                                   A Administração\n");
        s.append("                            ____________________");
        return s.toString();
    }

    /**
     * Método para criar uma string com toda a informação da 2ª via da fatura
     *
     * @return String - Informação da fatura
     */
    public String toString2Via() {
        GregorianCalendar agora = new GregorianCalendar();
        StringBuilder s = new StringBuilder("");
        s.append("\n\n\nFatura Nº" + (mensalidade + 1) + "/" + agora.get(agora.YEAR) + "\n");
        s.append("2ª Via\n\n");
        s.append("--------------------------------------------------------------"
                + "--------------------------------------------------------------------\n\n");
        s.append(proprietario.getNome() + "\n");
        s.append(proprietario.getMorada() + "\n");
        s.append(proprietario.getNif() + "\n");
        s.append("Quota da Fração:" + fracao.getDesignacao() + "\n");
        s.append("Mensalidade: " + (mensalidade + 1) + "\n");
        s.append("Valor da Mensalidade: " + valorMensalidade + " euros\n\n");
        s.append("--------------------------------------------------------------"
                + "--------------------------------------------------------------------\n\n");
        s.append(agora.get(agora.DAY_OF_MONTH) + "-"
                + (agora.get(agora.MONTH) + 1) + "-" + agora.get(agora.YEAR) + "\n\n");
        s.append("                                   A Administração\n");
        s.append("                            ____________________");
        return s.toString();
    }

    /**
     * Método para criar uma string com toda a informação do aviso de pagamento
     * em atraso
     *
     * @return String - Aviso de pagamento em atraso
     */
    public String toStringAviso() {
        GregorianCalendar agora = new GregorianCalendar();
        StringBuilder s = new StringBuilder("");
        s.append("\n\n\nEstimado Condómino da fração: " + fracao.getDesignacao() + "\n\n");
        s.append("Até a presente data não constam nos nossos registos o pagamento da quota relativa ao mês " + (mensalidade + 1)
                + " de " + agora.get(agora.YEAR) + " no valor de " + valorMensalidade + " euros\n\n");
        s.append("Desta forma, pedimos a gentileza de comparecer no nosso escritório afim de solucionar o débito em atraso.\n\n");
        s.append("Caso já tenha efetuado o pagamento do valor apontado, pedimos o favor "
                + "de nos encaminhar o recibo para que possamos regularizar a situação.\n\n");
        s.append(agora.get(agora.DAY_OF_MONTH) + "-"
                + (agora.get(agora.MONTH) + 1) + "-" + agora.get(agora.YEAR) + "\n\n");
        s.append("                                                                 A Administração\n");
        s.append("                                                ______________________________");
        return s.toString();
    }

    /**
     * Método para criar uma string com toda a informação da notificação da
     * existência de fatura em pagamento
     *
     * @return String - Aviso de pagamento em atraso
     */
    public String toStringNotificacao() {
        GregorianCalendar agora = new GregorianCalendar();
        StringBuilder s = new StringBuilder("");
        s.append("\n\n\nEstimado Condómino da fração: " + fracao.getDesignacao() + "\n\n");
        s.append("Vimos por este meio informá-lo que se encontra disponível para pagamento a quota relativa ao mês " + (mensalidade + 1)
                + " de " + agora.get(agora.YEAR) + " no valor de " + valorMensalidade + " euros\n\n");
        s.append("A data limite para o pagamento da mesma é: " + dataLimite.get(dataLimite.DAY_OF_MONTH) + "-"
                + dataLimite.get(dataLimite.MONTH) + "-" + dataLimite.get(dataLimite.YEAR) + ". Relembramos"
                + " que após essa data, terá " + diasAtraso + " dias para regularizar o pagamento, sendo que "
                + "o valor da mensalidade será agravado em " + (taxaAgravamento * 100) + "% \n\n");
        s.append("Caso já tenha efetuado o pagamento desta mensalidade, pedimos o favor de ignorar este aviso\n\n");
        s.append(agora.get(agora.DAY_OF_MONTH) + "-"
                + (agora.get(agora.MONTH) + 1) + "-" + agora.get(agora.YEAR) + "\n\n");
        s.append("                                                                 A Administração\n");
        s.append("                                                ______________________________");
        return s.toString();
    }
}
