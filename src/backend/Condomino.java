package backend;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.*;

/**
 * A existência da classe Condomino permite a criação dos condóminos existentes
 * num determinado Condomínio
 *
 * @author JoaoFerreira
 */
public class Condomino implements Serializable, Comparator {

    // Variáveis de instância
    private final Proprietario proprietario;
    private final Fracao fracao;
    private final ArrayList<Inquilino> listaInquilinos;
    private final ArrayList<Fatura> listaFaturas;
    private int limitePagamento, diasAtraso;
    private double taxaAgravamento, valorMensalidade;

    /**
     * Método construtor da classe
     *
     * @param proprietario Proprietário da fração associada a um condómino
     * @param fracao Fração de um determinado condómino
     */
    public Condomino(Proprietario proprietario, Fracao fracao) {
        this.proprietario = proprietario;
        this.fracao = fracao;
        listaInquilinos = new ArrayList<>();
        listaFaturas = new ArrayList<>();
    }

    /**
     * Método seletor da classe
     *
     * @return Proprietario - Proprietário do respectivo condómino
     */
    public Proprietario getProprietario() {
        return proprietario;
    }

    /**
     * Método seletor da classe
     *
     * @return Fracao - Fração associada ao condómino
     */
    public Fracao getFracao() {
        return fracao;
    }

    /**
     * Método seletor da classe
     *
     * @return limitePagamento - Dia limite para pagamento das mensalidades
     */
    public int getLimitePagamento() {
        return limitePagamento;
    }

    /**
     * Método seletor da classe
     *
     * @return diasAtraso - Número de dias para aceitação do pagamento de
     * mensalidades em atraso
     */
    public int getDiasAtraso() {
        return diasAtraso;
    }

    /**
     * Método seletor da classe
     *
     * @return taxaAgravamento - Taxa de agravamento a ser aplicado ao valor da
     * mensalidade em caso de atraso
     */
    public double getTaxaAgravamento() {
        return taxaAgravamento;
    }

    /**
     * Método seletor da classe
     *
     * @return valorMensalidade - Valor da mensalidade associada à fração
     */
    public double getValorMensalidade() {
        return valorMensalidade;
    }

    /**
     * Método seletor da classe
     *
     * @return ArrayList - Lista de inquilínos do condómino
     */
    public ArrayList<Inquilino> getListaInquilino() {
        return listaInquilinos;
    }

    /**
     * Método seletor da classe
     *
     * @return ArrayList - Lista de inquilínos do condómino
     */
    public ArrayList<Fatura> getListaFaturas() {
        return listaFaturas;
    }

    /**
     * Método modificador da classe: Altera o dia limite de pagamento
     *
     * @param novoLimitePagamento Novo dia do mês para o limite de pagamento da
     * mensalidade
     */
    public void setLimitePagamento(int novoLimitePagamento) {
        limitePagamento = novoLimitePagamento;
    }

    /**
     * Método modificador da classe: Altera o número de dias para aceitação de
     * um pagamento em atraso
     *
     * @param novosDiasAtraso Novo número de dias plausível para aceitação do
     * pagamento das mensalidades em atraso
     */
    public void setDiasAtraso(int novosDiasAtraso) {
        diasAtraso = novosDiasAtraso;
    }

    /**
     * Método modificador da classe: Altera a taxa de agravamento a aplicar em
     * caso de atraso de pagamento
     *
     * @param taxaAgravamento Novo valor da taxa de agravamento a ser aplicado
     * ao valor da mensalidade em caso de atraso no pagamento
     */
    public void setTaxaAgravamento(double taxaAgravamento) {
        this.taxaAgravamento = taxaAgravamento;
    }

    /**
     * Método para registar um novo inquilíno
     *
     * @param novoInquilino Novo inquilíno a ser registado
     */
    public void registarInquilino(Inquilino novoInquilino) {
        listaInquilinos.add(novoInquilino);
    }

    /**
     * Método para eliminar um determinado inquilíno
     *
     * @param inquilinoEliminar Inquilíno a ser eliminado
     */
    public void eliminarInquilino(Inquilino inquilinoEliminar) {
        listaInquilinos.remove(inquilinoEliminar);
    }

    /**
     * Método para registar a informação necessária para emitir faturas
     *
     * @param limitePagamento Dia do mês limite para o pagamento da mensalidade
     * @param diasAtraso Dias de atraso plausíveis para aceitação do pagamento
     * de fatura em atraso
     * @param taxaAgravamento Taxa a aplicar ao valor da mensalidade em caso de
     * atraso no pagamento da mensalidade
     */
    public void infoFaturas(int limitePagamento, int diasAtraso, double taxaAgravamento) {
        this.limitePagamento = limitePagamento;
        this.diasAtraso = diasAtraso;
        this.taxaAgravamento = taxaAgravamento;
        this.valorMensalidade = fracao.getQuota() / 12;
        this.valorMensalidade = Math.round(valorMensalidade * 100) / 100;
    }

    /**
     * Método para registar uma nova fatura
     *
     * @param novaFatura Fatura a ser registada
     */
    public void registarFatura(Fatura novaFatura) {
        listaFaturas.add(novaFatura);
        ordenarListaFaturas();
    }

    /**
     * Método para eliminar uma determinada fatura
     *
     * @param faturaEliminar Fatura a ser eliminada
     */
    public void eliminarFatura(Fatura faturaEliminar) {
        listaFaturas.remove(faturaEliminar);
    }

    /**
     * Método para ordenar a lista de faturas (ascendente por data)
     *
     */
    public void ordenarListaFaturas() {
        Collections.sort(listaFaturas, (Fatura o1,
                Fatura o2) -> o1.getDataLimite().compareTo(o2.getDataLimite()));
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * do condómino
     *
     * @return String - Informação do condómino
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("");
        s.append("Informação do Proprietário:\n");
        s.append(proprietario.toString());
        s.append(fracao.toString());
        s.append("\n");
        s.append("Informação dos Inquilinos:\n");
        for (int i = 0; i < listaInquilinos.size(); i++) {
            s.append(listaInquilinos.get(i).toString() + "\n");
        }
        return s.toString();
    }

    /**
     * Método para criar uma string com toda a informação das frações
     * orçamentadas
     *
     * @return String - Informação das frações orçamentadas
     */
    public String toStringOrcamento() {
        StringBuilder s = new StringBuilder("");
        s.append(fracao.toString());
        return s.toString();
    }

    /**
     * Override do método compare da Interface Compare
     *
     */
    @Override
    public int compare(Object o1, Object o2) {
        throw new UnsupportedOperationException("Not supported yet.");
        // Override para compilação sem erros
    }
}
