package backend;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A existência da classe Reuniao permite a criação das reuniões de cada
 * condomínio
 *
 * @author JoaoFerreira
 */
public class Reuniao implements Serializable {

    // Variáveis de Instância 
    private Date data;
    private int tipo;
    private String notas;

    /**
     * Método construtor da classe
     *
     * @param data Data da reunião
     * @param tipo Tipo de reunião (Obrigatória, Extraordinária)
     * @param notas Notas acerca da reunião
     */
    public Reuniao(Date data, int tipo, String notas) {
        this.data = data;
        this.tipo = tipo;
        this.notas = notas;
    }

    /**
     * Método seletor da classe
     *
     * @return Date - Data da reunião
     */
    public Date getData() {
        return data;
    }

    /**
     * Método seletor da classe
     *
     * @return int - Tipo de reunião (Obrigatória, Extraordinária)
     */
    public int getTipo() {
        // 0 - Obrigatória, 1 - Extraordinária
        return tipo;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Notas acerca da reunião
     */
    public String getNotas() {
        return notas;
    }

    /**
     * Método modificador da classe: Altera a data da reunião
     *
     * @param novaData Nova data da reunião
     */
    public void setData(Date novaData) {
        data = novaData;
    }

    /**
     * Método modificador da classe: Altera o tipo de reunião (Obrigatória,
     * Extraordinária)
     *
     * @param novoTipo Novo tipo de reunião (Obrigatória, Extraordinária)
     */
    public void setTipo(int novoTipo) {
        tipo = novoTipo;
    }

    /**
     * Método modificador da classe: Altera as notas da reunião
     *
     * @param novasNotas Novas notas da reunião
     */
    public void setNotas(String novasNotas) {
        notas = novasNotas;
    }

    /**
     * Override do método toString, resultando uma string com toda a informação
     * da reunião
     *
     * @return String - Informação da reunião
     */
    @Override
    public String toString() {
        String tipo = "";
        if (this.tipo == 0) {
            tipo = "Obrigatória";
        } else {
            tipo = "Extraordinária";
        }
        String stringData = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(data);
        StringBuilder s = new StringBuilder("");
        s.append("Reunião: " + tipo + "\n");
        s.append("Data: " + stringData + "\n");
        s.append("Notas: " + notas + "\n");
        s.append("\n");
        return s.toString();
    }
}
