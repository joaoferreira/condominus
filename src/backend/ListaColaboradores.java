package backend;

import java.util.ArrayList;
import java.io.Serializable;

/**
 * A existência da classe ListaColaboradores permite a criação de uma ArrayList
 * de colaboradores que devido ao facto de implementar o interface Serializable
 * permite a gravação dos dados dos colaboradores da empresa Condominus em
 * ficheiro
 *
 * @author JoaoFerreira
 */
public class ListaColaboradores implements Serializable {

    // Variáveis de instância 
    private final ArrayList<Colaborador> listaColaboradores;

    /**
     * Método construtor da classe
     */
    public ListaColaboradores() {
        listaColaboradores = new ArrayList<>();
    }

    /**
     * Método para registar um novo colaborador
     *
     * @param novoColaborador Novo colaborador a ser registado
     */
    public void registarColaborador(Colaborador novoColaborador) {
        listaColaboradores.add(novoColaborador);
    }

    /**
     * Método para eliminar um determinado colaborador
     *
     * @param colaboradorEliminar Colaborador a ser eliminado
     */
    public void eliminarColaborador(Colaborador colaboradorEliminar) {
        listaColaboradores.remove(colaboradorEliminar);
    }

    /**
     * Override do método get da classe ArrayList
     *
     * @param i Índice
     * @return Colaborador - Colaborador selecionado
     */
    public Colaborador get(int i) {
        return listaColaboradores.get(i);
    }

    /**
     * Override do método size da classe ArrayList
     *
     * @return int - Tamanho da lista de colaboradores
     */
    public int size() {
        return listaColaboradores.size();
    }
}
