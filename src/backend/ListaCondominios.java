package backend;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.*;

/**
 * A existência da classe ListaCondominios permite a criação de uma ArrayList de
 * condomínios que devido ao facto de implementar o interface Serializable
 * permite a gravação dos dados dos clientes da empresa Condominus em ficheiro
 *
 * @author JoaoFerreira
 */
public class ListaCondominios implements Serializable {

    // Variáveis de instância 
    private final ArrayList<Condominio> listaCondominios;
    private long[] listaCodigoPostal;
    private ArrayList<Condominio> listaOrdenada;

    /**
     * Método construtor da classe
     */
    public ListaCondominios() {
        listaCondominios = new ArrayList<>();
    }

    /**
     * Método para registar um novo condomínio
     *
     * @param novoCondominio Novo condomínio a ser registado
     */
    public void registarCondominio(Condominio novoCondominio) {
        listaCondominios.add(novoCondominio);
    }

    /**
     * Método para eliminar um determinado condomínio
     *
     * @param condominioEliminar Condomínio a ser eliminado
     */
    public void eliminarCondominio(Condominio condominioEliminar) {
        listaCondominios.remove(condominioEliminar);
    }

    /**
     * Método para listar os 5 melhores condomínios da empresa (por volume de
     * faturação)
     *
     * @return String - Lista dos 5 melhores condomínio da empresa (por volume
     * de faturação)
     */
    public String top5Condominios() {
        if (!listaCondominios.isEmpty()) {
            listaOrdenada = listaCondominios;
            ordenarListaCondominios();
            StringBuilder s = new StringBuilder("TOP 5 Condomínios: \n");
            int limite;
            if (listaOrdenada.size() > 5) {
                limite = 5;
            } else {
                limite = listaOrdenada.size();
            }
            for (int i = 0; i < limite; i++) {
                s.append(listaOrdenada.get(i).getNome() + " - Volume de faturação: "
                        + listaOrdenada.get(i).getVolumeFaturacao() + "\n");
            }
            return s.toString();
        } else {
            return "Não existem condomínios registados no momento";
        }
    }

    /**
     * Método para calcular o volume de faturação ganho num determinado período
     * de temopo
     *
     * @param dataInicio Data início para realização do apuramento da faturação
     * @param dataFim Data início para realização do apuramento da faturação
     * @return String - Lista dos 5 melhores condomínio da empresa (por volume
     * de faturação)
     */
    public String topFaturacaoPeriodo(Date dataInicio, Date dataFim) {
        if (!listaCondominios.isEmpty()) {
            double valorFaturacaoPeriodo = 0;
            for (int i = 0; i < listaCondominios.size(); i++) {
                valorFaturacaoPeriodo
                        += listaCondominios.get(i).calcularVolumeFacturacaoPeriodo(dataInicio, dataFim);
            }
            return "O volume faturado no período selecionado foi de "
                    + valorFaturacaoPeriodo + "€";
        } else {
            return "Não existem condomínios registados no momento";
        }
    }

    /**
     * Método para ordenar a lista de de condomínios por volume de faturação
     *
     */
    public void ordenarListaCondominios() {
        Collections.sort(listaOrdenada, (Condominio o1,
                Condominio o2) -> Double.compare(o2.calcularVolumeFacturacao(),
                o1.calcularVolumeFacturacao()));
    }

    /**
     * Método para listar o código postal onde a empresa encontra-se a gerir, no
     * momento, o maior número de condomínios
     *
     * @return String - Código postal onde a empresa encontra-se a gerir, no
     * momento, o maior número de condomínios
     */
    public String topCodigoPostal() {
        if (!listaCondominios.isEmpty()) {
            listaCodigoPostal = new long[listaCondominios.size()];
            for (int i = 0; i < listaCondominios.size(); i++) {
                listaCodigoPostal[i] = listaCondominios.get(i).getCodigoPostal1();
            }
            Arrays.sort(listaCodigoPostal);
            HashMap<Long, Integer> codigosPostais = new HashMap();
            for (int i = 0; i < listaCodigoPostal.length; i++) {
                if (codigosPostais.containsKey(listaCodigoPostal[i])) {
                    codigosPostais.put(listaCodigoPostal[i], codigosPostais.get(listaCodigoPostal[i]) + 1);
                } else {
                    codigosPostais.put(listaCodigoPostal[i], 1);
                }
            }

            Map.Entry<Long, Integer> maxEntrada = null;

            for (Map.Entry<Long, Integer> entrada : codigosPostais.entrySet()) {
                if (maxEntrada == null || entrada.getValue().compareTo(maxEntrada.getValue()) > 0) {
                    maxEntrada = entrada;
                }
            }
            return "A empresa encontra-se, actualmente, a gerir mais condomínios no código postal: \n"
                    + maxEntrada.getKey() + ", com um total de " + maxEntrada.getValue() + " condomínios.";
        } else {
            return "Não existem condomínios registados no momento";
        }
    }

    /**
     * Override do método get da classe ArrayList
     *
     * @param i Índice
     * @return Condomínio - Condomínio selecionado
     */
    public Condominio get(int i) {
        return listaCondominios.get(i);
    }

    /**
     * Override do método size da classe ArrayList
     *
     * @return int - Tamanho da lista de condomínios
     */
    public int size() {
        return listaCondominios.size();
    }

    /**
     * Método para verificar a existência de reuniões ou atividades de
     * manutenção agendadas para uma determinada data
     *
     * @param data Data sobre a qual será verificada a existência de reuniões
     * e/ou atividades de manutenção agendadas para essa mesma data
     * @return String - Informação das reuniões e/ou atividades agendadas para
     * uma determinada data
     */
    public String notificacoes(Date data) {
        StringBuilder s = new StringBuilder("");
        Calendar calData = Calendar.getInstance();
        calData.setTime(data);
        int ano = calData.get(Calendar.YEAR);
        int mes = calData.get(Calendar.MONTH);
        int dia = calData.get(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < listaCondominios.size(); i++) {
            for (int j = 0; j < listaCondominios.get(i).getListaReunioes().size(); j++) {
                Calendar calDataComparar = Calendar.getInstance();
                calDataComparar.setTime(listaCondominios.get(i).getListaReunioes().get(j).getData());
                int anoComparar = calDataComparar.get(Calendar.YEAR);
                int mesComparar = calDataComparar.get(Calendar.MONTH);
                int diaComparar = calDataComparar.get(Calendar.DAY_OF_MONTH);
                int hora = calDataComparar.get(Calendar.HOUR_OF_DAY);
                int min = calDataComparar.get(Calendar.MINUTE);
                if (ano == anoComparar && mes == mesComparar && dia == diaComparar) {
                    String tipo;
                    if (listaCondominios.get(i).getListaReunioes().get(j).getTipo() == 1) {
                        tipo = "obrigatória";
                    } else {
                        tipo = "extraordinária";
                    }
                    s.append("* Reunião " + tipo + " no condomínio "
                            + listaCondominios.get(i).getNome() + " às " + hora + "h:"
                            + min + "min\n");
                }
            }
            for (int k = 0; k < listaCondominios.get(i).getListaAtivManut().size(); k++) {
                Calendar calDataComparar = Calendar.getInstance();
                calDataComparar.setTime(listaCondominios.get(i).getListaAtivManut().get(k).getData());
                int anoComparar = calDataComparar.get(Calendar.YEAR);
                int mesComparar = calDataComparar.get(Calendar.MONTH);
                int diaComparar = calDataComparar.get(Calendar.DAY_OF_MONTH);
                if (ano == anoComparar && mes == mesComparar && dia == diaComparar) {
                    s.append("* Atividade de manutenção no condomínio " + listaCondominios.get(i).getNome()
                            + " com a descrição: " + listaCondominios.get(i).getListaAtivManut().get(k).getActividadeManutencao() + "\n");
                }
            }
        }
        return s.toString();
    }
}
