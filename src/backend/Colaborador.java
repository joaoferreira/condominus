package backend;

import java.io.Serializable;
import java.util.Date;

/**
 * A existência da classe Colaborador permite a criação dos colaboradores da
 * empresa Condominus
 *
 * @author JoaoFerreira
 */
public class Colaborador implements Serializable {

    // Variáveis de Instância 
    private String primeiroNome, ultimoNome, morada, contactoTelefonico, email,
            habilitacoes, funcao;
    private Date dataNascimento;

    /**
     * Método construtor da classe
     *
     * @param primeiroNome Nome do colaborador
     * @param ultimoNome Apelido do colaborador
     * @param morada Morada do colaborador
     * @param dataNascimento Data de nascimento do colaborador
     * @param contactoTelefonico Contacto telefónico do colaborador
     * @param email Endereço electrónico do colaborador
     * @param habilitacoes Habilitações literárias do colaborador
     * @param funcao Função do colaborador na empresa
     */
    public Colaborador(String primeiroNome, String ultimoNome, String morada, Date dataNascimento,
            String contactoTelefonico, String email, String habilitacoes, String funcao) {
        this.primeiroNome = primeiroNome;
        this.ultimoNome = ultimoNome;
        this.morada = morada;
        this.dataNascimento = dataNascimento;
        this.contactoTelefonico = contactoTelefonico;
        this.email = email;
        this.habilitacoes = habilitacoes;
        this.funcao = funcao;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Nome do colaborador
     */
    public String getPrimeiroNome() {
        return primeiroNome;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Apelido do colaborador
     */
    public String getUltimoNome() {
        return ultimoNome;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Morada do colaborador
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Método seletor da classe
     *
     * @return Date - Data de nascimento do colaborador
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Contacto telefónico do colaborador
     */
    public String getContactoTelefonico() {
        return contactoTelefonico;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Endereço electrónico do colaborador
     */
    public String getEmail() {
        return email;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Habilitações literárias do colaborador
     */
    public String getHabilitacoes() {
        return habilitacoes;
    }

    /**
     * Método seletor da classe
     *
     * @return String - Função do colaborador na empresa
     */
    public String getFuncao() {
        return funcao;
    }

    /**
     * Método modificador da classe: Altera o nome do colaborador
     *
     * @param novoPrimeiroNome Novo nome do colaborador
     */
    public void setPrimeiroNome(String novoPrimeiroNome) {
        primeiroNome = novoPrimeiroNome;
    }

    /**
     * Método modificador da classe: Altera o apelido do colaborador
     *
     * @param novoUltimoNome Novo apelido do colaborador
     */
    public void setUltimoNome(String novoUltimoNome) {
        ultimoNome = novoUltimoNome;
    }

    /**
     * Método modificador da classe: Altera a morada do colaborador
     *
     * @param novaMorada Nova morada do colaborador
     */
    public void setMorada(String novaMorada) {
        morada = novaMorada;
    }

    /**
     * Método modificador da classe: Altera a data de nascimento do colaborador
     *
     * @param novaDataNascimento Nova data de nascimento do colaborador
     */
    public void setDataNascimento(Date novaDataNascimento) {
        dataNascimento = novaDataNascimento;
    }

    /**
     * Método modificador da classe: Altera o contacto telefónico do colaborador
     *
     * @param novoContactoTelefonico Novo contacto telefónico do colaborador
     */
    public void setContactoTelefonico(String novoContactoTelefonico) {
        contactoTelefonico = novoContactoTelefonico;
    }

    /**
     * Método modificador da classe: Altera o endereço electrónico do
     * colaborador
     *
     * @param novoEmail Novo endereço electrónico do colaborador
     */
    public void setEmail(String novoEmail) {
        email = novoEmail;
    }

    /**
     * Método modificador da classe: Altera as habilitações literárias do
     * colaborador
     *
     * @param novasHabilitacoes Novas habilitações literárias do colaborador
     */
    public void setHabilitacoes(String novasHabilitacoes) {
        habilitacoes = novasHabilitacoes;
    }

    /**
     * Método modificador da classe: Altera a função do colaborador
     *
     * @param novaFuncao Nova função do colaborador
     */
    public void setFuncao(String novaFuncao) {
        funcao = novaFuncao;
    }
}
