package frontend;

import backend.*;
import java.awt.Desktop;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.*;
import javax.swing.*;

/**
 * A existência da classe ImprimirDocumentos permite a criação e abertura de
 * ficheiros pdf contendo as informações sobre condomínios, colaboradores,
 * faturas, entre outros
 *
 * @author JoaoFerreira
 */
public class ImprimirDocumentos {

    // Variáveis de Instância 
    private ListaCondominios listaCondominios;
    private ListaOrcamentos listaOrcamentos;
    private Condominio imprimirCondominio;
    private Orcamento imprimirOrcamento;
    private JDialog selecao;
    private JPanel selecaoPanel;
    public static final String FICHEIRO_COND = "saved_docs/relatorio_condominio.pdf";
    public static final String FICHEIRO_ORC = "saved_docs/relatorio_orcamento.pdf";
    public static final String FICHEIRO_FAT = "saved_docs/fatura.pdf";
    public static final String FICHEIRO_2VIA = "saved_docs/segunda_via.pdf";
    public static final String FICHEIRO_NOT = "saved_docs/notificacao.pdf";
    public static final String ATRASO = "saved_docs/atraso.pdf";

    /**
     * Método construtor da classe
     *
     * @throws java.io.IOException Exceção lançada
     */
    public ImprimirDocumentos() throws IOException {
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregarCondominios() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(selecaoPanel,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(selecaoPanel,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que carrega a lista de orçamentos de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregarOrcamentos() throws IOException {
        ListaOrcamentos infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_orcamentos.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaOrcamentos) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaOrcamentos = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(selecaoPanel,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaOrcamentos = new ListaOrcamentos();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(selecaoPanel,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaOrcamentos = new ListaOrcamentos();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método para criar e abrir um ficheiro pdf com a informação do condomínio
     * selecionado
     *
     * @param ficheiro_con Nome e directoria do ficheiro a criar
     * @param condominioImprimir Condomínio a imprimir
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void createPdfCondominios(String ficheiro_con, Condominio condominioImprimir)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(ficheiro_con));
        document.open();
        Image logotipo = Image.getInstance("logotipo2.png");
        logotipo.setAbsolutePosition(380f, 750f);
        logotipo.scaleAbsolute(200f, 72.9f);
        document.add(logotipo);
        document.add(new Paragraph(condominioImprimir.toString()));
        document.close();

        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(ficheiro_con);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(selecaoPanel,
                        "Não possui software para visualizar ficheiros pdf", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Método para criar e abrir um ficheiro pdf com a informação do orçamento
     * selecionado
     *
     * @param ficheiro_orc Nome e directoria do ficheiro a criar
     * @param orcamentoImprimir Orçamento a imprimir
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void createPdfOrcamentos(String ficheiro_orc, Orcamento orcamentoImprimir)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(ficheiro_orc));
        document.open();
        Image logotipo = Image.getInstance("logotipo2.png");
        logotipo.setAbsolutePosition(380f, 750f);
        logotipo.scaleAbsolute(200f, 72.9f);
        document.add(logotipo);
        document.add(new Paragraph(orcamentoImprimir.toString()));
        document.close();

        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(ficheiro_orc);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(selecaoPanel,
                        "Não possui software para visualizar ficheiros pdf", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Método para criar e abrir um ficheiro pdf com a fatura desejada
     *
     * @param fatura Fatura a imprimir
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirFatura(Fatura fatura)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(FICHEIRO_FAT));
        document.open();
        Image logotipo = Image.getInstance("logotipo2.png");
        logotipo.setAbsolutePosition(380f, 750f);
        logotipo.scaleAbsolute(200f, 72.9f);
        document.add(logotipo);
        document.add(new Paragraph(fatura.toString()));
        document.close();

        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(FICHEIRO_FAT);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(selecaoPanel,
                        "Não possui software para visualizar ficheiros pdf", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Método para criar e abrir um ficheiro pdf com a 2ª via da fatura desejada
     *
     * @param fatura Fatura a imprimir (2ª via)
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirSegundaVia(Fatura fatura)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(FICHEIRO_2VIA));
        document.open();
        Image logotipo = Image.getInstance("logotipo2.png");
        logotipo.setAbsolutePosition(380f, 750f);
        logotipo.scaleAbsolute(200f, 72.9f);
        document.add(logotipo);
        document.add(new Paragraph(fatura.toString2Via()));
        document.close();

        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(FICHEIRO_2VIA);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(selecaoPanel,
                        "Não possui software para visualizar ficheiros pdf", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Método para criar e abrir um ficheiro pdf com o aviso de atraso de
     * pagamento
     *
     * @param fatura Fatura em atraso
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirAviso(Fatura fatura)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(ATRASO));
        document.open();
        Image logotipo = Image.getInstance("logotipo2.png");
        logotipo.setAbsolutePosition(380f, 750f);
        logotipo.scaleAbsolute(200f, 72.9f);
        document.add(logotipo);
        document.add(new Paragraph(fatura.toStringAviso()));
        document.close();

        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(ATRASO);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(selecaoPanel,
                        "Não possui software para visualizar ficheiros pdf", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Método para criar e abrir um ficheiro pdf com notificação de mensalidade
     * em pagamento
     *
     * @param fatura Fatura em pagamento
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirNotificacao(Fatura fatura)
            throws DocumentException, IOException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(FICHEIRO_NOT));
        document.open();
        Image logotipo = Image.getInstance("logotipo2.png");
        logotipo.setAbsolutePosition(380f, 750f);
        logotipo.scaleAbsolute(200f, 72.9f);
        document.add(logotipo);
        document.add(new Paragraph(fatura.toStringNotificacao()));
        document.close();

        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(FICHEIRO_NOT);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(selecaoPanel,
                        "Não possui software para visualizar ficheiros pdf", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Método para selecionar o condominio sobre o qual se quer obter o
     * relatório
     *
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void selecionarCondominio() throws DocumentException, IOException {

        carregarCondominios();

        selecao = new JDialog();
        selecaoPanel = new JPanel(null);

        JComboBox listaCond = new JComboBox();
        for (int i = 0; i < this.listaCondominios.size(); i++) {
            this.imprimirCondominio = listaCondominios.get(i);
            listaCond.addItem(this.imprimirCondominio.getNome());
        }

        if (listaCondominios.size() != 0) {

            int selecao = JOptionPane.showConfirmDialog(null, listaCond,
                    "Selecionar Condomínio", JOptionPane.OK_OPTION);

            if (selecao == JOptionPane.OK_OPTION) {
                this.imprimirCondominio = this.listaCondominios.get(listaCond.getSelectedIndex());
                createPdfCondominios(FICHEIRO_COND, imprimirCondominio);
            }
        }
    }

    /**
     * Método para selecionar o orçamento sobre o qual se quer obter o relatório
     *
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void selecionarOrcamento() throws DocumentException, IOException {

        carregarOrcamentos();

        selecao = new JDialog();
        selecaoPanel = new JPanel(null);

        JComboBox listaCond = new JComboBox();
        for (int i = 0; i < this.listaOrcamentos.size(); i++) {
            this.imprimirOrcamento = listaOrcamentos.get(i);
            listaCond.addItem(this.imprimirOrcamento.getNumeroOrcamento());
        }

        if (listaOrcamentos.size() != 0) {

            int selecao = JOptionPane.showConfirmDialog(null, listaCond,
                    "Selecionar Orçamento", JOptionPane.OK_OPTION);

            if (selecao == JOptionPane.OK_OPTION) {
                this.imprimirOrcamento = this.listaOrcamentos.get(listaCond.getSelectedIndex());
                createPdfOrcamentos(FICHEIRO_ORC, imprimirOrcamento);
            }
        }
    }
}
