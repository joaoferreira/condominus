package frontend;

import backend.*;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelCondomino permite a criação do content panel
 * específico da janela principal para o menu Reuniões
 *
 * @author JoaoFerreira
 */
public class PainelReuniao implements ActionListener {

    // Variáveis de Instância
    private JPanel painelReuniao;
    private JTextArea notas;
    private JSpinner data;
    private JList listagemCondominios, listagemReunioes;
    private JScrollPane scrollListagemCondominios, scrollListagemReunioes,
            scrollNotas;
    private JComboBox tipo;
    private final DefaultListModel modeloListagemCondominios = new DefaultListModel();
    private final DefaultListModel modeloListagemReunioes = new DefaultListModel();
    private JButton novo, gravar, editar, apagar;
    private ListaCondominios listaCondominios;
    private Condominio editarCondominio;
    private Reuniao novaReuniao, editarReuniao, apagarReuniao;
    boolean edicao = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Reuniões
     *
     * @return JPanel - content panel do menu Reuniões
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelReuniao() throws IOException {

        carregar();
        carregarListagem();

        painelReuniao = new JPanel(null);
        painelReuniao.setBackground(Color.white);

        JLabel titulo = new JLabel("Reuniões");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(300, 20, 200, 50);
        painelReuniao.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelReuniao.add(separador);

        JLabel labelListagem = new JLabel("Lista Condomínios:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelReuniao.add(labelListagem);

        JLabel labelListagemReunioes = new JLabel("Lista Reuniões:");
        labelListagemReunioes.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagemReunioes.setBounds(180, 115, 150, 20);
        painelReuniao.add(labelListagemReunioes);

        listagemCondominios = new JList(modeloListagemCondominios);
        listagemCondominios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominios.setLayoutOrientation(JList.VERTICAL);
        listagemCondominios.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominios.setFixedCellHeight(20);
        listagemCondominios.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {

                } else if (listagemCondominios.getSelectedIndex() != -1) {
                    listagemReunioes.setEnabled(true);
                    novo.setEnabled(true);
                    carregarListagemReunioes();
                }
            }
        });

        scrollListagemCondominios = new JScrollPane(listagemCondominios);
        scrollListagemCondominios.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominios.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominios.setBounds(20, 150, 150, 300);

        painelReuniao.add(scrollListagemCondominios);

        listagemReunioes = new JList(modeloListagemReunioes);
        listagemReunioes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemReunioes.setLayoutOrientation(JList.VERTICAL);
        listagemReunioes.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemReunioes.setFixedCellHeight(20);
        listagemReunioes.setEnabled(false);
        listagemReunioes.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagemReunioes.getSelectedIndex() != -1) {
                        carregarDadosReunioes();
                    }
                } else if (listagemReunioes.getSelectedIndex() != -1) {
                    carregarDadosReunioes();
                } else {
                    data.setEnabled(false);
                    tipo.setEnabled(false);
                    notas.setEnabled(false);
                    notas.setText("");
                    novo.setEnabled(true);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                    apagar.setEnabled(false);
                }
            }
        });

        scrollListagemReunioes = new JScrollPane(listagemReunioes);
        scrollListagemReunioes.setVerticalScrollBar(new JScrollBar());
        scrollListagemReunioes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemReunioes.setBounds(180, 150, 150, 300);

        painelReuniao.add(scrollListagemReunioes);

        JLabel labelTipo = new JLabel("Tipo de Reunião");
        labelTipo.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelTipo.setBounds(345, 150, 155, 20);
        painelReuniao.add(labelTipo);

        String[] tipos = {"Obrigatória", "Extraordinária"};
        tipo = new JComboBox(tipos);
        tipo.setFont(new Font("Verdana", Font.PLAIN, 14));
        tipo.setBounds(480, 150, 265, 20);
        tipo.setEnabled(false);
        painelReuniao.add(tipo);

        JLabel labelData = new JLabel("Data");
        labelData.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelData.setBounds(345, 185, 60, 20);
        painelReuniao.add(labelData);

        data = new JSpinner(new SpinnerDateModel());
        data.setEditor(new JSpinner.DateEditor(data, "dd/MM/yyyy HH:mm"));
        data.setFont(new Font("Verdana", Font.PLAIN, 14));
        data.setBounds(400, 185, 200, 20);
        data.setEnabled(false);
        painelReuniao.add(data);

        JLabel labelDescricao = new JLabel("Notas:");
        labelDescricao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDescricao.setBounds(345, 220, 80, 20);
        painelReuniao.add(labelDescricao);

        notas = new JTextArea();
        notas.setFont(new Font("Verdana", Font.PLAIN, 14));
        notas.setLineWrap(true);
        notas.setWrapStyleWord(true);
        notas.setEnabled(false);

        scrollNotas = new JScrollPane(notas);
        scrollNotas.setVerticalScrollBar(new JScrollBar());
        scrollNotas.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollNotas.setBounds(345, 255, 400, 195);

        painelReuniao.add(scrollNotas);

        novo = new JButton("Novo");
        novo.setName("novo");
        novo.setFont(new Font("Verdana", Font.ITALIC, 14));
        novo.setBounds(20, 500, 85, 20);
        novo.setEnabled(false);
        painelReuniao.add(novo);
        novo.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(120, 500, 85, 20);
        editar.setEnabled(false);
        painelReuniao.add(editar);
        editar.addActionListener(this);

        apagar = new JButton("Apagar");
        apagar.setName("apagar");
        apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagar.setBounds(220, 500, 85, 20);
        apagar.setEnabled(false);
        painelReuniao.add(apagar);
        apagar.addActionListener(this);

        gravar = new JButton("Gravar");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelReuniao.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelReuniao.add(fundo);

        painelReuniao.revalidate();
        painelReuniao.repaint();
        return painelReuniao;
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelReuniao,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelReuniao,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(painelReuniao, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelReuniao,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condomínios para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaCondominios.size(); i++) {
            modeloListagemCondominios.addElement("*" + listaCondominios.get(i).getNome());
        }
    }

    /**
     * Método que carrega as reuniões de um determinado condomínio para uma
     * Jlist
     */
    public void carregarListagemReunioes() {
        modeloListagemReunioes.clear();
        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaReunioes().size(); i++) {
            if (listaCondominios.get(listagemCondominios.
                    getSelectedIndex()).getListaReunioes().get(i).
                    getTipo() == 0) {
                modeloListagemReunioes.addElement("*" + "Obrigatória");
            } else {
                modeloListagemReunioes.addElement("*" + "Extraordinária");
            }
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados da reunião
     * selecionada
     */
    public void carregarDadosReunioes() {
        data.setEnabled(false);
        data.setValue(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaReunioes().get(listagemReunioes.getSelectedIndex()).getData());
        tipo.setEnabled(false);
        tipo.setSelectedIndex(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaReunioes().get(listagemReunioes.getSelectedIndex()).getTipo());
        notas.setEnabled(false);
        notas.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaReunioes().get(listagemReunioes.getSelectedIndex()).getNotas());

        gravar.setEnabled(false);
        novo.setEnabled(true);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelReuniao.revalidate();
        painelReuniao.repaint();
    }

    /**
     * Método utilizado para gravar uma nova reunião no respectivo condomínio
     */
    public void gravar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.notas.getText().isEmpty())
                || (tipo.getSelectedIndex() == -1)) {

            JOptionPane.showMessageDialog(painelReuniao, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            Date data = (Date) this.data.getValue();
            int tipo = this.tipo.getSelectedIndex();
            String notas = this.notas.getText();

            novaReuniao = new Reuniao(data, tipo, notas);

            int confirmacao = JOptionPane.showConfirmDialog(painelReuniao,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                editarCondominio = listaCondominios.get(listagemCondominios.getSelectedIndex());
                editarCondominio.agendarReuniao(novaReuniao);

                guardar();

                this.data.setEnabled(false);
                this.tipo.setEnabled(false);
                this.notas.setEnabled(false);
                this.notas.setText("");

                carregarListagemReunioes();

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelReuniao.revalidate();
                painelReuniao.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar uma determinada reunião
     */
    public void editar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.notas.getText().isEmpty())
                || (tipo.getSelectedIndex() == -1)) {

            JOptionPane.showMessageDialog(painelReuniao, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(painelReuniao,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarReuniao = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaReunioes().get(listagemReunioes.getSelectedIndex());

                editarReuniao.setData((Date) this.data.getValue());
                editarReuniao.setTipo(tipo.getSelectedIndex());
                editarReuniao.setNotas(this.notas.getText());

                listaCondominios.get(listagemCondominios.getSelectedIndex()).ordenarListaReunioes();

                guardar();

                this.data.setEnabled(false);
                this.tipo.setEnabled(false);
                this.notas.setEnabled(false);
                this.notas.setText("");

                listagemReunioes.setEnabled(true);
                listagemCondominios.setEnabled(true);

                carregarListagemReunioes();

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelReuniao.revalidate();
                painelReuniao.repaint();
            }
        }
    }

    /**
     * Método utilizado para apagar uma determinada reunião
     */
    public void apagar() {
        int confirmacao = JOptionPane.showConfirmDialog(painelReuniao,
                "Pretende apagar a reuniao?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarReuniao = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaReunioes().get(listagemReunioes.getSelectedIndex());
            listaCondominios.get(listagemCondominios.getSelectedIndex()).desagendarReuniao(apagarReuniao);

            guardar();

            JOptionPane.showMessageDialog(painelReuniao,
                    "Reunião eliminada com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.data.setEnabled(false);
            this.tipo.setEnabled(false);
            this.notas.setEnabled(false);
            this.notas.setText("");

            modeloListagemReunioes.clear();
            listagemReunioes.setEnabled(false);
            listagemCondominios.setEnabled(true);
            listagemReunioes.clearSelection();
            listagemCondominios.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelReuniao.revalidate();
            painelReuniao.repaint();
        }
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoClicado = (JButton) e.getSource();
        String nomeBotao = botaoClicado.getName();

        if (nomeBotao.equals("novo")) {
            this.data.setEnabled(true);
            this.tipo.setEnabled(true);
            this.notas.setEnabled(true);
            this.notas.setText("");

            listagemReunioes.setEnabled(true);
            listagemCondominios.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelReuniao.revalidate();
            painelReuniao.repaint();

        } else if (nomeBotao.equals("gravar")) {
            if (edicao == true) {
                editar();
            } else {
                gravar();
            }
        } else if (nomeBotao.equals("editar")) {
            this.data.setEnabled(true);
            this.tipo.setEnabled(true);
            this.notas.setEnabled(true);

            listagemReunioes.setEnabled(true);
            listagemCondominios.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = true;

            painelReuniao.revalidate();
            painelReuniao.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagar();
        }
    }
}
