package frontend;

import backend.*;
import java.util.Date;
import com.itextpdf.text.DocumentException;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelFatura permite a criação do content panel
 * específico da janela principal para o menu Pagamentos
 *
 * @author JoaoFerreira
 */
public class PainelPagamento implements ActionListener {

    // Variáveis de Instância
    private JPanel painelPagamento;
    private JTextField valorMensalidade, estado;
    private JSpinner dataAtraso, dataLimite;
    private JComboBox listaFaturas;
    private JList listagemCondominios, listagemCondominos;
    private JScrollPane scrollListagemCondominios, scrollListagemCondominos;
    private final DefaultListModel modeloListagemCondominios = new DefaultListModel();
    private final DefaultListModel modeloListagemCondominos = new DefaultListModel();
    private JButton registar, segundaVia, emitirAviso, emitirNotificacao;
    private ListaCondominios listaCondominios;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Pagamentos
     *
     * @return JPanel - Content panel do menu Pagamentos
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelPagamento() throws IOException {

        carregar();
        carregarListagem();

        painelPagamento = new JPanel(null);
        painelPagamento.setBackground(Color.white);

        JLabel titulo = new JLabel("Pagamentos");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(320, 20, 200, 50);
        painelPagamento.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelPagamento.add(separador);

        JLabel labelListagem = new JLabel("Lista Condomínios:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelPagamento.add(labelListagem);

        JLabel labelListagemCondominos = new JLabel("Lista Condomínos:");
        labelListagemCondominos.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagemCondominos.setBounds(180, 115, 150, 20);
        painelPagamento.add(labelListagemCondominos);

        listagemCondominios = new JList(modeloListagemCondominios);
        listagemCondominios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominios.setLayoutOrientation(JList.VERTICAL);
        listagemCondominios.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominios.setFixedCellHeight(20);
        listagemCondominios.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {

                } else if (listagemCondominios.getSelectedIndex() != -1) {
                    listagemCondominos.setEnabled(true);
                    carregarListagemCondominos();
                    registar.setEnabled(false);
                    valorMensalidade.setText("");
                    estado.setText("");
                    listaFaturas.setEnabled(false);
                    listaFaturas.removeAllItems();
                }
            }
        });

        scrollListagemCondominios = new JScrollPane(listagemCondominios);
        scrollListagemCondominios.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominios.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominios.setBounds(20, 150, 150, 300);

        painelPagamento.add(scrollListagemCondominios);

        listagemCondominos = new JList(modeloListagemCondominos);
        listagemCondominos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominos.setLayoutOrientation(JList.VERTICAL);
        listagemCondominos.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominos.setFixedCellHeight(20);
        listagemCondominos.setEnabled(false);
        listagemCondominos.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagemCondominos.getSelectedIndex() != -1) {
                        carregarDadosFatura();
                    }
                } else if (listagemCondominos.getSelectedIndex() != -1) {
                    carregarDadosFatura();
                } else {
                    valorMensalidade.setText("");
                    estado.setText("");
                    listaFaturas.setEnabled(false);
                    listaFaturas.removeAllItems();
                    registar.setEnabled(false);
                }
            }
        });

        scrollListagemCondominos = new JScrollPane(listagemCondominos);
        scrollListagemCondominos.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominos.setBounds(180, 150, 150, 300);

        painelPagamento.add(scrollListagemCondominos);

        JLabel labelFatura = new JLabel("Selecione Fatura:");
        labelFatura.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelFatura.setBounds(345, 150, 200, 20);
        painelPagamento.add(labelFatura);

        listaFaturas = new JComboBox();
        listaFaturas.setBounds(345, 185, 100, 20);
        listaFaturas.setEnabled(false);
        listaFaturas.addActionListener(listaFaturasActionListener);
        painelPagamento.add(listaFaturas);

        JLabel labelValorMensalidade = new JLabel("Valor Mensalidade");
        labelValorMensalidade.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelValorMensalidade.setBounds(345, 220, 200, 20);
        painelPagamento.add(labelValorMensalidade);

        valorMensalidade = new JTextField();
        valorMensalidade.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorMensalidade.setBounds(490, 220, 100, 20);
        valorMensalidade.setEnabled(false);
        painelPagamento.add(valorMensalidade);

        JLabel labelDataAtraso = new JLabel("Data Limite Pagamento");
        labelDataAtraso.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDataAtraso.setBounds(345, 255, 200, 20);
        painelPagamento.add(labelDataAtraso);

        dataAtraso = new JSpinner(new SpinnerDateModel());
        dataAtraso.setEditor(new JSpinner.DateEditor(dataAtraso, "dd/MM/yyyy"));
        dataAtraso.setFont(new Font("Verdana", Font.PLAIN, 14));
        dataAtraso.setBounds(530, 255, 165, 20);
        dataAtraso.setEnabled(false);
        painelPagamento.add(dataAtraso);

        JLabel labelDataLimite = new JLabel("Data Limite Pagamento Atraso");
        labelDataLimite.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDataLimite.setBounds(345, 290, 220, 20);
        painelPagamento.add(labelDataLimite);

        dataLimite = new JSpinner(new SpinnerDateModel());
        dataLimite.setEditor(new JSpinner.DateEditor(dataLimite, "dd/MM/yyyy"));
        dataLimite.setFont(new Font("Verdana", Font.PLAIN, 14));
        dataLimite.setBounds(580, 290, 165, 20);
        dataLimite.setEnabled(false);
        painelPagamento.add(dataLimite);

        JLabel labelEstado = new JLabel("Estado");
        labelEstado.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelEstado.setBounds(345, 325, 200, 20);
        painelPagamento.add(labelEstado);

        estado = new JTextField();
        estado.setFont(new Font("Verdana", Font.PLAIN, 14));
        estado.setBounds(400, 325, 350, 20);
        estado.setEnabled(false);
        painelPagamento.add(estado);

        registar = new JButton("Registar");
        registar.setName("registar");
        registar.setFont(new Font("Verdana", Font.ITALIC, 14));
        registar.setBounds(20, 500, 85, 20);
        registar.setEnabled(false);
        painelPagamento.add(registar);
        registar.addActionListener(this);

        segundaVia = new JButton("2ª Via");
        segundaVia.setName("segundaVia");
        segundaVia.setFont(new Font("Verdana", Font.ITALIC, 14));
        segundaVia.setBounds(120, 500, 85, 20);
        segundaVia.setEnabled(false);
        segundaVia.setVisible(false);
        painelPagamento.add(segundaVia);
        segundaVia.addActionListener(this);

        emitirAviso = new JButton("Aviso");
        emitirAviso.setName("aviso");
        emitirAviso.setFont(new Font("Verdana", Font.ITALIC, 14));
        emitirAviso.setBounds(120, 500, 85, 20);
        emitirAviso.setEnabled(false);
        emitirAviso.setVisible(false);
        painelPagamento.add(emitirAviso);
        emitirAviso.addActionListener(this);

        emitirNotificacao = new JButton("Notificação");
        emitirNotificacao.setName("notificacao");
        emitirNotificacao.setFont(new Font("Verdana", Font.ITALIC, 14));
        emitirNotificacao.setBounds(120, 500, 85, 20);
        emitirNotificacao.setEnabled(false);
        emitirNotificacao.setVisible(false);
        painelPagamento.add(emitirNotificacao);
        emitirNotificacao.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelPagamento.add(fundo);

        painelPagamento.revalidate();
        painelPagamento.repaint();
        return painelPagamento;
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelPagamento,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelPagamento,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(painelPagamento, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);

        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelPagamento,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condomínios para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaCondominios.size(); i++) {
            modeloListagemCondominios.addElement("*" + listaCondominios.get(i).getNome());
        }
    }

    /**
     * Método que carrega o nome dos condóminos de um determinado condomínio
     * para uma Jlist
     */
    public void carregarListagemCondominos() {
        modeloListagemCondominos.clear();
        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().size(); i++) {
            modeloListagemCondominos.addElement("*" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(i).getProprietario().getNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos a informação da fatura
     * selecionada
     */
    public void carregarDadosFatura() {
        valorMensalidade.setText("");
        estado.setText("");
        listaFaturas.removeAllItems();
        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                get(listagemCondominos.getSelectedIndex()).getListaFaturas().size(); i++) {
            listaFaturas.addItem(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                    get(listagemCondominos.getSelectedIndex()).getListaFaturas().get(i).getMensalidade() + 1 + "");
        }
        listaFaturas.setEnabled(true);
        if (listaFaturas.getSelectedIndex() == -1) {
            registar.setEnabled(false);
        }
    }

    ActionListener listaFaturasActionListener = new ActionListener() {

        /**
         * Override do método actionPerformed utilizado para determinar que ação
         * será realizada a quando de uma alteração na seleção de um item da
         * combobox
         *
         * @param e Evento ocorrido
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                Fatura faturaCarregada = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                        get(listagemCondominos.getSelectedIndex()).getListaFaturas().get(listaFaturas.getSelectedIndex());

                valorMensalidade.setText("" + faturaCarregada.getValorMensalidade());
                estado.setText(faturaCarregada.getEstado());
                dataAtraso.setValue(faturaCarregada.getDataAtraso().getTime());
                dataLimite.setValue(faturaCarregada.getDataLimite().getTime());

                if (faturaCarregada.isPaga()) {
                    registar.setEnabled(false);
                    segundaVia.setVisible(true);
                    segundaVia.setEnabled(true);
                    emitirAviso.setVisible(false);
                    emitirAviso.setEnabled(false);
                    emitirNotificacao.setEnabled(false);
                    emitirNotificacao.setVisible(false);
                } else if (faturaCarregada.isPagavel() == false) {
                    registar.setEnabled(false);
                    segundaVia.setEnabled(false);
                    segundaVia.setVisible(false);
                    emitirAviso.setVisible(true);
                    emitirAviso.setEnabled(true);
                    emitirNotificacao.setEnabled(false);
                    emitirNotificacao.setVisible(false);
                } else {
                    registar.setEnabled(true);
                    segundaVia.setEnabled(false);
                    segundaVia.setVisible(false);
                    emitirAviso.setVisible(false);
                    emitirAviso.setEnabled(false);
                    emitirNotificacao.setEnabled(true);
                    emitirNotificacao.setVisible(true);
                }
            } catch (Exception z) {

            }
        }
    };

    /**
     * Método utilizado para imprimir uma fatura
     *
     * @param fatura Fatura a ser impressa
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirFatura(Fatura fatura) throws DocumentException, IOException {
        ImprimirDocumentos imprimirFatura = new ImprimirDocumentos();
        imprimirFatura.imprimirFatura(fatura);
    }

    /**
     * Método utilizado para imprimir a segunda via de uma fatua
     *
     * @param fatura Fatura a ser impressa (2ª via)
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirSegundaVia(Fatura fatura) throws DocumentException, IOException {
        ImprimirDocumentos imprimirSegundaVia = new ImprimirDocumentos();
        imprimirSegundaVia.imprimirSegundaVia(fatura);
    }

    /**
     * Método utilizado para imprimir um aviso de pagamento em atraso
     *
     * @param fatura Fatura em atraso
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirAviso(Fatura fatura) throws DocumentException, IOException {
        ImprimirDocumentos imprimirAviso = new ImprimirDocumentos();
        imprimirAviso.imprimirAviso(fatura);
    }

    /**
     * Método utilizado para imprimir uma notificação de mensalidade em
     * pagamento
     *
     * @param fatura Fatura em pagamento
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirNotificacao(Fatura fatura) throws DocumentException, IOException {
        ImprimirDocumentos imprimirNotificacao = new ImprimirDocumentos();
        imprimirNotificacao.imprimirNotificacao(fatura);
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JButton botaoClicado = (JButton) e.getSource();
        String nomebotao = botaoClicado.getName();

        if (nomebotao.equals("registar")) {
            Fatura faturaCarregada = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                    get(listagemCondominos.getSelectedIndex()).getListaFaturas().get(listaFaturas.getSelectedIndex());
            faturaCarregada.setPaga(true);
            faturaCarregada.setEstado("Paga");
            Date agora = new Date();
            faturaCarregada.setDataPagamento(agora);
            System.out.println(agora.toString());
            guardar();

            listaFaturas.setSelectedIndex(-1);
            int imprimir = JOptionPane.showConfirmDialog(painelPagamento, "Deseja imprimir fatura?",
                    "Imprimir", JOptionPane.YES_NO_OPTION);
            valorMensalidade.setText("");
            estado.setText("");
            emitirNotificacao.setVisible(false);
            registar.setEnabled(false);

            if (imprimir == JOptionPane.YES_NO_OPTION) {
                try {
                    imprimirFatura(faturaCarregada);
                } catch (DocumentException | IOException ex) {
                    Logger.getLogger(PainelPagamento.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (nomebotao.equals("segundaVia")) {
            Fatura faturaCarregada = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                    get(listagemCondominos.getSelectedIndex()).getListaFaturas().get(listaFaturas.getSelectedIndex());
            listaFaturas.setSelectedIndex(-1);
            int imprimir = JOptionPane.showConfirmDialog(painelPagamento, "Deseja imprimir 2ª via?",
                    "Imprimir", JOptionPane.YES_NO_OPTION);
            valorMensalidade.setText("");
            estado.setText("");
            listaFaturas.setSelectedIndex(-1);
            segundaVia.setVisible(false);
            if (imprimir == JOptionPane.YES_OPTION) {
                try {
                    imprimirSegundaVia(faturaCarregada);
                } catch (DocumentException | IOException ex) {
                    Logger.getLogger(PainelPagamento.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (nomebotao.equals("aviso")) {
            Fatura faturaCarregada = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                    get(listagemCondominos.getSelectedIndex()).getListaFaturas().get(listaFaturas.getSelectedIndex());
            listaFaturas.setSelectedIndex(-1);
            int imprimir = JOptionPane.showConfirmDialog(painelPagamento, "Deseja imprimir aviso de pagamento?",
                    "Imprimir", JOptionPane.YES_NO_OPTION);
            valorMensalidade.setText("");
            estado.setText("");
            listaFaturas.setSelectedIndex(-1);
            emitirAviso.setVisible(false);
            if (imprimir == JOptionPane.YES_OPTION) {
                try {
                    imprimirAviso(faturaCarregada);
                } catch (DocumentException | IOException ex) {
                    Logger.getLogger(PainelPagamento.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else if (nomebotao.equals("notificacao")) {
            Fatura faturaCarregada = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                    get(listagemCondominos.getSelectedIndex()).getListaFaturas().get(listaFaturas.getSelectedIndex());
            listaFaturas.setSelectedIndex(-1);
            int imprimir = JOptionPane.showConfirmDialog(painelPagamento, "Deseja imprimir notificação de pagamento?",
                    "Imprimir", JOptionPane.YES_NO_OPTION);
            valorMensalidade.setText("");
            estado.setText("");
            listaFaturas.setSelectedIndex(-1);
            emitirNotificacao.setVisible(false);
            registar.setEnabled(false);
            if (imprimir == JOptionPane.YES_OPTION) {
                try {
                    imprimirNotificacao(faturaCarregada);
                } catch (DocumentException | IOException ex) {
                    Logger.getLogger(PainelPagamento.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
