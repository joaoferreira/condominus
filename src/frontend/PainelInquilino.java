package frontend;

import backend.*;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelInquilino permite a criação do content panel
 * específico da janela principal para o menu Inquilínos
 *
 * @author JoaoFerreira
 */
public class PainelInquilino implements ActionListener {

// Variáveis de Instância
    private JPanel painelInquilino;
    private JTextField nome, email, contactoTelefonico, profissao, condominioSelecionado;
    private JSpinner dataNascimento;
    private JList listagemInquilinos, listagemCondominos;
    private JScrollPane scrollListagemInquilinos, scrollListagemCondominos;
    private final DefaultListModel modeloListagemCondominos = new DefaultListModel();
    private final DefaultListModel modeloListagemInquilinos = new DefaultListModel();
    private JButton novo, gravar, editar, apagar;
    private ListaCondominios listaCondominios;
    private Condominio editarCondominio;
    private Condomino editarCondomino;
    private Inquilino novoInquilino, editarInquilino, apagarInquilino;
    private boolean edicao = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Inquilínos
     *
     * @return JPanel - Content panel do menu Inquilínos
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelInquilino() throws IOException {

        carregar();

        painelInquilino = new JPanel(null);
        painelInquilino.setBackground(Color.white);

        JComboBox listaCond = new JComboBox();
        for (int i = 0; i < this.listaCondominios.size(); i++) {
            this.editarCondominio = listaCondominios.get(i);
            listaCond.addItem(this.editarCondominio.getNome());
        }

        if (listaCondominios.size() != 0) {

            int selecao = JOptionPane.showConfirmDialog(painelInquilino, listaCond,
                    "Selecionar Condomínio", JOptionPane.OK_CANCEL_OPTION);

            if (selecao == JOptionPane.OK_OPTION) {
                this.editarCondominio = this.listaCondominios.get(listaCond.getSelectedIndex());

                carregarListagem();

                JLabel titulo = new JLabel("Inquilínos");
                titulo.setFont(new Font("Verdana", Font.BOLD, 25));
                titulo.setBounds(275, 20, 250, 50);
                painelInquilino.add(titulo);

                JSeparator separador = new JSeparator();
                separador.setBounds(20, 85, 760, 10);
                painelInquilino.add(separador);

                JLabel labelListagemCondominos = new JLabel("Lista Condóminos:");
                labelListagemCondominos.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelListagemCondominos.setBounds(20, 115, 150, 20);
                painelInquilino.add(labelListagemCondominos);

                JLabel labelListagemInquilinos = new JLabel("Lista Inquilínos:");
                labelListagemInquilinos.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelListagemInquilinos.setBounds(180, 115, 150, 20);
                painelInquilino.add(labelListagemInquilinos);

                JLabel labelCondominio = new JLabel("Condomínio Selecionado: ");
                labelCondominio.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelCondominio.setBounds(345, 115, 200, 20);
                painelInquilino.add(labelCondominio);

                condominioSelecionado = new JTextField();
                condominioSelecionado.setFont(new Font("Verdana", Font.BOLD, 14));
                condominioSelecionado.setBounds(530, 115, 240, 20);
                condominioSelecionado.setEnabled(false);
                condominioSelecionado.setText(editarCondominio.getNome());
                painelInquilino.add(condominioSelecionado);

                listagemCondominos = new JList(modeloListagemCondominos);
                listagemCondominos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                listagemCondominos.setLayoutOrientation(JList.VERTICAL);
                listagemCondominos.setFont(new Font("Verdana", Font.PLAIN, 14));
                listagemCondominos.setFixedCellHeight(20);
                listagemCondominos.addListSelectionListener(new ListSelectionListener() {
                    // Override do método valueChanged
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        if (e.getValueIsAdjusting()) {

                        } else if (listagemCondominos.getSelectedIndex() != -1) {
                            listagemInquilinos.setEnabled(true);
                            novo.setEnabled(true);
                            carregarListagemInquilinos();
                        }
                    }
                });

                scrollListagemCondominos = new JScrollPane(listagemCondominos);
                scrollListagemCondominos.setVerticalScrollBar(new JScrollBar());
                scrollListagemCondominos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
                scrollListagemCondominos.setBounds(20, 150, 150, 300);

                painelInquilino.add(scrollListagemCondominos);

                listagemInquilinos = new JList(modeloListagemInquilinos);
                listagemInquilinos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                listagemInquilinos.setLayoutOrientation(JList.VERTICAL);
                listagemInquilinos.setFont(new Font("Verdana", Font.PLAIN, 14));
                listagemInquilinos.setFixedCellHeight(20);
                listagemInquilinos.setEnabled(false);
                listagemInquilinos.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        if (e.getValueIsAdjusting()) {
                            if (listagemInquilinos.getSelectedIndex() != -1) {
                                carregarDadosInquilinos();
                            }
                        } else if (listagemInquilinos.getSelectedIndex() != -1) {
                            carregarDadosInquilinos();
                        } else {
                            nome.setEnabled(false);
                            nome.setText("");
                            email.setEnabled(false);
                            email.setText("");
                            contactoTelefonico.setEnabled(false);
                            contactoTelefonico.setText("");
                            dataNascimento.setEnabled(false);
                            profissao.setEnabled(false);
                            profissao.setText("");
                            novo.setEnabled(true);
                            editar.setEnabled(false);
                            gravar.setEnabled(false);
                            apagar.setEnabled(false);
                        }
                    }
                });

                scrollListagemInquilinos = new JScrollPane(listagemInquilinos);
                scrollListagemInquilinos.setVerticalScrollBar(new JScrollBar());
                scrollListagemInquilinos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
                scrollListagemInquilinos.setBounds(180, 150, 150, 300);

                painelInquilino.add(scrollListagemInquilinos);

                JLabel labelNome = new JLabel("Nome do Inquilíno");
                labelNome.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelNome.setBounds(345, 150, 145, 20);
                painelInquilino.add(labelNome);

                nome = new JTextField();
                nome.setFont(new Font("Verdana", Font.PLAIN, 14));
                nome.setBounds(500, 150, 270, 20);
                nome.setEnabled(false);
                painelInquilino.add(nome);

                JLabel labelEmail = new JLabel("e-mail");
                labelEmail.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelEmail.setBounds(345, 185, 60, 20);
                painelInquilino.add(labelEmail);

                email = new JTextField();
                email.setFont(new Font("Verdana", Font.PLAIN, 14));
                email.setBounds(420, 185, 350, 20);
                email.setEnabled(false);
                painelInquilino.add(email);

                JLabel labelContactoTelefonico = new JLabel("Contacto Telefónico");
                labelContactoTelefonico.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelContactoTelefonico.setBounds(345, 220, 140, 20);
                painelInquilino.add(labelContactoTelefonico);

                contactoTelefonico = new JTextField();
                contactoTelefonico.setFont(new Font("Verdana", Font.PLAIN, 14));
                contactoTelefonico.setBounds(500, 220, 145, 20);
                contactoTelefonico.setEnabled(false);
                painelInquilino.add(contactoTelefonico);

                JLabel labelDataNascimento = new JLabel("Data de Nascimento");
                labelDataNascimento.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelDataNascimento.setBounds(345, 255, 150, 20);
                painelInquilino.add(labelDataNascimento);

                dataNascimento = new JSpinner(new SpinnerDateModel());
                dataNascimento.setEditor(new JSpinner.DateEditor(dataNascimento, "dd/MM/yyyy"));
                dataNascimento.setFont(new Font("Verdana", Font.PLAIN, 14));
                dataNascimento.setBounds(510, 255, 165, 20);
                dataNascimento.setEnabled(false);
                painelInquilino.add(dataNascimento);

                JLabel labelProfissao = new JLabel("Profissão");
                labelProfissao.setFont(new Font("Verdana", Font.ITALIC, 14));
                labelProfissao.setBounds(345, 290, 65, 20);
                painelInquilino.add(labelProfissao);

                profissao = new JTextField();
                profissao.setFont(new Font("Verdana", Font.PLAIN, 14));
                profissao.setBounds(425, 290, 250, 20);
                profissao.setEnabled(false);
                painelInquilino.add(profissao);

                novo = new JButton("Novo");
                novo.setName("novo");
                novo.setFont(new Font("Verdana", Font.ITALIC, 14));
                novo.setBounds(20, 500, 85, 20);
                novo.setEnabled(false);
                painelInquilino.add(novo);
                novo.addActionListener(this);

                editar = new JButton("Editar");
                editar.setName("editar");
                editar.setFont(new Font("Verdana", Font.ITALIC, 14));
                editar.setBounds(120, 500, 85, 20);
                editar.setEnabled(false);
                painelInquilino.add(editar);
                editar.addActionListener(this);

                apagar = new JButton("Apagar");
                apagar.setName("apagar");
                apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
                apagar.setBounds(220, 500, 85, 20);
                apagar.setEnabled(false);
                painelInquilino.add(apagar);
                apagar.addActionListener(this);

                gravar = new JButton("Gravar");
                gravar.setName("gravar");
                gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
                gravar.setBounds(320, 500, 85, 20);
                gravar.setEnabled(false);
                painelInquilino.add(gravar);
                gravar.addActionListener(this);

                JLabel fundo = new JLabel();
                fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                        Image.SCALE_SMOOTH)));
                fundo.setBounds(450, 450, 320, 58);
                painelInquilino.add(fundo);

                painelInquilino.revalidate();
                painelInquilino.repaint();
                return painelInquilino;

            } else {

                JLabel fundo = new JLabel();
                fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(800, 144,
                        Image.SCALE_SMOOTH)));
                fundo.setBounds(0, 206, 800, 144);
                painelInquilino.add(fundo);

                painelInquilino.revalidate();
                painelInquilino.repaint();
                return painelInquilino;
            }
        } else {

            JLabel fundo = new JLabel();
            fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(800, 144,
                    Image.SCALE_SMOOTH)));
            fundo.setBounds(0, 206, 800, 144);
            painelInquilino.add(fundo);

            JOptionPane.showMessageDialog(painelInquilino, "Não existem condomínios registados",
                    "Aviso", JOptionPane.OK_OPTION);

            painelInquilino.revalidate();
            painelInquilino.repaint();
            return painelInquilino;
        }
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelInquilino,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelInquilino,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(painelInquilino, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelInquilino,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condóminos para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < editarCondominio.getListaCondominos().size(); i++) {
            modeloListagemCondominos.addElement("*" + editarCondominio.getListaCondominos().get(i).getProprietario().getNome());
        }
    }

    /**
     * Método que carrega o nome dos inquilínios de um determinada fração para
     * uma Jlist
     */
    public void carregarListagemInquilinos() {
        modeloListagemInquilinos.clear();
        for (int i = 0; i < editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().size(); i++) {
            modeloListagemInquilinos.addElement("*" + editarCondominio.getListaCondominos().
                    get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(i).getNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados do inquilíno
     * selecionado
     */
    public void carregarDadosInquilinos() {
        nome.setEnabled(false);
        nome.setText(editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex()).getNome());
        email.setEnabled(false);
        email.setText(editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex()).getEmail());
        contactoTelefonico.setEnabled(false);
        contactoTelefonico.setText(editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex()).getContactoTelefonico());
        dataNascimento.setEnabled(false);
        dataNascimento.setValue(editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex()).getDataNascimento());
        profissao.setEnabled(false);
        profissao.setText(editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex()).getProfissao());

        gravar.setEnabled(false);
        novo.setEnabled(true);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelInquilino.revalidate();
        painelInquilino.repaint();
    }

    /**
     * Método utilizado para gravar um novo inquilíno na respectiva fração
     */
    public void gravar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty())
                || (this.nome.getText().isEmpty())
                || (this.email.getText().isEmpty())
                || (this.profissao.getText().isEmpty())
                || (this.contactoTelefonico.getText().isEmpty())) {

            JOptionPane.showMessageDialog(painelInquilino, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String nome = this.nome.getText();
            String email = this.email.getText();
            String profissao = this.profissao.getText();

            int contactoTelefonico;

            try {
                contactoTelefonico = Integer.parseInt(this.contactoTelefonico.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelInquilino, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.contactoTelefonico.setText("");
                this.contactoTelefonico.requestFocusInWindow();
                return;
            }

            Date dataNascimento;

            try {
                dataNascimento = (Date) this.dataNascimento.getValue();
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelInquilino, "Este campo apenas aceita datas",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.dataNascimento.setValue(new SpinnerDateModel());
                this.dataNascimento.requestFocusInWindow();
                return;
            }

            novoInquilino = new Inquilino(nome, email, "" + contactoTelefonico, dataNascimento, profissao);
            editarCondomino = editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex());

            int confirmacao = JOptionPane.showConfirmDialog(painelInquilino,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                editarCondomino.registarInquilino(novoInquilino);

                guardar();

                this.nome.setEnabled(false);
                this.nome.setText("");
                this.email.setEnabled(false);
                this.email.setText("");
                this.contactoTelefonico.setEnabled(false);
                this.contactoTelefonico.setText("");
                this.dataNascimento.setEnabled(false);
                this.profissao.setEnabled(false);
                this.profissao.setText("");

                modeloListagemInquilinos.addElement("*" + novoInquilino.getNome());

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelInquilino.revalidate();
                painelInquilino.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar um determinado inquilíno
     */
    public void editar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty())
                || (this.nome.getText().isEmpty())
                || (this.email.getText().isEmpty())
                || (this.profissao.getText().isEmpty())
                || (this.contactoTelefonico.getText().isEmpty())) {

            JOptionPane.showMessageDialog(painelInquilino, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(painelInquilino,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarInquilino = editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex());

                editarInquilino.setNome(this.nome.getText());
                editarInquilino.setEmail(this.email.getText());
                editarInquilino.setProfissao(this.profissao.getText());
                editarInquilino.setDataNascimento((Date) this.dataNascimento.getValue());

                int contactoTelefonico;

                try {
                    contactoTelefonico = Integer.parseInt(this.contactoTelefonico.getText());
                    editarInquilino.setContactoTelefonico(this.contactoTelefonico.getText());
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelInquilino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.contactoTelefonico.setText("");
                    this.contactoTelefonico.requestFocusInWindow();
                    return;
                }
            }

            guardar();

            this.nome.setEnabled(false);
            this.nome.setText("");
            this.email.setEnabled(false);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(false);
            this.contactoTelefonico.setText("");
            this.dataNascimento.setEnabled(false);
            this.profissao.setEnabled(false);
            this.profissao.setText("");

            listagemCondominos.setEnabled(true);
            listagemInquilinos.setEnabled(true);
            modeloListagemInquilinos.setElementAt("*" + editarInquilino.getNome(), listagemInquilinos.getSelectedIndex());

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelInquilino.revalidate();
            painelInquilino.repaint();
        }
    }

    /**
     * Método utilizado para apagar um determinado inquilíno
     */
    public void apagar() {
        int confirmacao = JOptionPane.showConfirmDialog(painelInquilino,
                "Pretende apagar o inquilíno?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarInquilino = editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).getListaInquilino().get(listagemInquilinos.getSelectedIndex());
            editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex()).eliminarInquilino(apagarInquilino);

            guardar();

            JOptionPane.showMessageDialog(painelInquilino,
                    "Inquilíno eliminado com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.nome.setEnabled(false);
            this.nome.setText("");
            this.email.setEnabled(false);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(false);
            this.contactoTelefonico.setText("");
            this.dataNascimento.setEnabled(false);
            this.profissao.setEnabled(false);
            this.profissao.setText("");

            modeloListagemInquilinos.clear();
            listagemInquilinos.setEnabled(false);
            listagemCondominos.setEnabled(true);
            listagemCondominos.clearSelection();
            listagemInquilinos.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelInquilino.revalidate();
            painelInquilino.repaint();
        }
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoClicado = (JButton) e.getSource();
        String nomeBotao = botaoClicado.getName();

        if (nomeBotao.equals("novo")) {
            this.nome.setEnabled(true);
            this.nome.setText("");
            this.email.setEnabled(true);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(true);
            this.contactoTelefonico.setText("");
            this.dataNascimento.setEnabled(true);
            this.profissao.setEnabled(true);
            this.profissao.setText("");

            listagemInquilinos.setEnabled(true);
            listagemCondominos.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelInquilino.revalidate();
            painelInquilino.repaint();

        } else if (nomeBotao.equals("gravar")) {
            if (edicao == true) {
                editar();
            } else {
                gravar();
            }
        } else if (nomeBotao.equals("editar")) {
            this.nome.setEnabled(true);
            this.email.setEnabled(true);
            this.contactoTelefonico.setEnabled(true);
            this.dataNascimento.setEnabled(true);
            this.profissao.setEnabled(true);

            listagemInquilinos.setEnabled(true);
            listagemCondominos.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = true;

            painelInquilino.revalidate();
            painelInquilino.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagar();
        }
    }
}
