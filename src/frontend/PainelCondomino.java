package frontend;

import backend.*;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelCondomino permite a criação do content panel
 * específico da janela principal para o menu Condóminos
 *
 * @author JoaoFerreira
 */
public class PainelCondomino implements ActionListener {

    // Variáveis de Instância
    private JPanel painelCondomino;
    private JTextField nome, morada, email, contactoTelefonico, nif, profissao;
    private JSpinner dataNascimento;
    private JTextField designacao, permilagem, andar, taxaUtilizacao, quota;
    private JList listagemCondominios, listagemCondominos;
    private JScrollPane scrollListagemCondominios, scrollListagemCondominos;
    private final DefaultListModel modeloListagemCondominios = new DefaultListModel();
    private final DefaultListModel modeloListagemCondominos = new DefaultListModel();
    private JButton novo, gravar, editar, apagar;
    private ListaCondominios listaCondominios;
    private Condominio editarCondominio;
    private Condomino novoCondomino, editarCondomino, apagarCondomino;
    private Proprietario novoProprietario;
    private Fracao novoFracao;
    boolean edicao = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Condóminos
     *
     * @return JPanel - Content panel do menu Condóminos
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelCondomino() throws IOException {

        carregar();
        carregarListagem();

        painelCondomino = new JPanel(null);
        painelCondomino.setBackground(Color.white);

        JLabel titulo = new JLabel("Condóminos");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(275, 20, 250, 50);
        painelCondomino.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelCondomino.add(separador);

        JLabel labelListagem = new JLabel("Lista Condomínios:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelCondomino.add(labelListagem);

        JLabel labelListagemCondominos = new JLabel("Lista Condomínos:");
        labelListagemCondominos.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagemCondominos.setBounds(180, 115, 150, 20);
        painelCondomino.add(labelListagemCondominos);

        listagemCondominios = new JList(modeloListagemCondominios);
        listagemCondominios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominios.setLayoutOrientation(JList.VERTICAL);
        listagemCondominios.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominios.setFixedCellHeight(20);
        listagemCondominios.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {

                } else if (listagemCondominios.getSelectedIndex() != -1) {
                    listagemCondominos.setEnabled(true);
                    novo.setEnabled(true);
                    carregarListagemCondominos();
                }
            }
        });

        scrollListagemCondominios = new JScrollPane(listagemCondominios);
        scrollListagemCondominios.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominios.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominios.setBounds(20, 150, 150, 300);

        painelCondomino.add(scrollListagemCondominios);

        listagemCondominos = new JList(modeloListagemCondominos);
        listagemCondominos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominos.setLayoutOrientation(JList.VERTICAL);
        listagemCondominos.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominos.setFixedCellHeight(20);
        listagemCondominos.setEnabled(false);
        listagemCondominos.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagemCondominos.getSelectedIndex() != -1) {
                        carregarDadosCondomino();
                    }
                } else if (listagemCondominos.getSelectedIndex() != -1) {
                    carregarDadosCondomino();
                } else {
                    nome.setEnabled(false);
                    nome.setText("");
                    morada.setEnabled(false);
                    morada.setText("");
                    email.setEnabled(false);
                    email.setText("");
                    contactoTelefonico.setEnabled(false);
                    contactoTelefonico.setText("");
                    nif.setEnabled(false);
                    nif.setText("");
                    dataNascimento.setEnabled(false);
                    profissao.setEnabled(false);
                    profissao.setText("");
                    designacao.setEnabled(false);
                    designacao.setText("");
                    permilagem.setEnabled(false);
                    permilagem.setText("");
                    andar.setEnabled(false);
                    andar.setText("");
                    taxaUtilizacao.setEnabled(false);
                    taxaUtilizacao.setText("");
                    quota.setEnabled(false);
                    quota.setText("");
                    novo.setEnabled(true);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                    apagar.setEnabled(false);
                }
            }
        });

        scrollListagemCondominos = new JScrollPane(listagemCondominos);
        scrollListagemCondominos.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominos.setBounds(180, 150, 150, 300);

        painelCondomino.add(scrollListagemCondominos);

        JLabel labelNome = new JLabel("Nome do Condómino");
        labelNome.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelNome.setBounds(345, 150, 155, 20);
        painelCondomino.add(labelNome);

        nome = new JTextField();
        nome.setFont(new Font("Verdana", Font.PLAIN, 14));
        nome.setBounds(515, 150, 265, 20);
        nome.setEnabled(false);
        painelCondomino.add(nome);

        JLabel labelMorada = new JLabel("Morada");
        labelMorada.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelMorada.setBounds(345, 185, 60, 20);
        painelCondomino.add(labelMorada);

        morada = new JTextField();
        morada.setFont(new Font("Verdana", Font.PLAIN, 14));
        morada.setBounds(420, 185, 360, 20);
        morada.setEnabled(false);
        painelCondomino.add(morada);

        JLabel labelEmail = new JLabel("e-mail");
        labelEmail.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelEmail.setBounds(345, 220, 50, 20);
        painelCondomino.add(labelEmail);

        email = new JTextField();
        email.setFont(new Font("Verdana", Font.PLAIN, 14));
        email.setBounds(410, 220, 370, 20);
        email.setEnabled(false);
        painelCondomino.add(email);

        JLabel labelContactoTelefonico = new JLabel("Contacto Telefónico");
        labelContactoTelefonico.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelContactoTelefonico.setBounds(345, 255, 140, 20);
        painelCondomino.add(labelContactoTelefonico);

        contactoTelefonico = new JTextField();
        contactoTelefonico.setFont(new Font("Verdana", Font.PLAIN, 14));
        contactoTelefonico.setBounds(500, 255, 115, 20);
        contactoTelefonico.setEnabled(false);
        painelCondomino.add(contactoTelefonico);

        JLabel labelNif = new JLabel("NIF");
        labelNif.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelNif.setBounds(630, 255, 30, 20);
        painelCondomino.add(labelNif);

        nif = new JTextField();
        nif.setFont(new Font("Verdana", Font.PLAIN, 14));
        nif.setBounds(680, 255, 100, 20);
        nif.setEnabled(false);
        painelCondomino.add(nif);

        JLabel labelDataNascimento = new JLabel("Data de Nascimento");
        labelDataNascimento.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDataNascimento.setBounds(345, 290, 150, 20);
        painelCondomino.add(labelDataNascimento);

        dataNascimento = new JSpinner(new SpinnerDateModel());
        dataNascimento.setEditor(new JSpinner.DateEditor(dataNascimento, "dd/MM/yyyy"));
        dataNascimento.setFont(new Font("Verdana", Font.PLAIN, 14));
        dataNascimento.setBounds(510, 290, 165, 20);
        dataNascimento.setEnabled(false);
        painelCondomino.add(dataNascimento);

        JLabel labelProfissao = new JLabel("Profissão");
        labelProfissao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelProfissao.setBounds(345, 325, 65, 20);
        painelCondomino.add(labelProfissao);

        profissao = new JTextField();
        profissao.setFont(new Font("Verdana", Font.PLAIN, 14));
        profissao.setBounds(425, 325, 250, 20);
        profissao.setEnabled(false);
        painelCondomino.add(profissao);

        JLabel labelDesignacao = new JLabel("Fração");
        labelDesignacao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDesignacao.setBounds(345, 360, 60, 20);
        painelCondomino.add(labelDesignacao);

        designacao = new JTextField();
        designacao.setFont(new Font("Verdana", Font.PLAIN, 14));
        designacao.setBounds(420, 360, 155, 20);
        designacao.setEnabled(false);
        painelCondomino.add(designacao);

        JLabel labelAndar = new JLabel("Andar");
        labelAndar.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelAndar.setBounds(590, 360, 50, 20);
        painelCondomino.add(labelAndar);

        andar = new JTextField();
        andar.setFont(new Font("Verdana", Font.PLAIN, 14));
        andar.setBounds(655, 360, 50, 20);
        andar.setEnabled(false);
        painelCondomino.add(andar);

        JLabel labelPermilagem = new JLabel("Permilagem");
        labelPermilagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelPermilagem.setBounds(580, 395, 85, 20);
        painelCondomino.add(labelPermilagem);

        permilagem = new JTextField();
        permilagem.setFont(new Font("Verdana", Font.PLAIN, 14));
        permilagem.setBounds(680, 395, 65, 20);
        permilagem.setEnabled(false);
        painelCondomino.add(permilagem);

        JLabel labelTaxaUtilizacao = new JLabel("Taxa de Utilização");
        labelTaxaUtilizacao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelTaxaUtilizacao.setBounds(345, 395, 140, 20);
        painelCondomino.add(labelTaxaUtilizacao);

        taxaUtilizacao = new JTextField();
        taxaUtilizacao.setFont(new Font("Verdana", Font.PLAIN, 14));
        taxaUtilizacao.setBounds(500, 395, 65, 20);
        taxaUtilizacao.setEnabled(false);
        painelCondomino.add(taxaUtilizacao);

        JLabel labelQuota = new JLabel("Quota");
        labelQuota.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelQuota.setBounds(345, 430, 50, 20);
        painelCondomino.add(labelQuota);

        quota = new JTextField();
        quota.setFont(new Font("Verdana", Font.PLAIN, 14));
        quota.setBounds(410, 430, 65, 20);
        quota.setEnabled(false);
        painelCondomino.add(quota);

        novo = new JButton("Novo");
        novo.setName("novo");
        novo.setFont(new Font("Verdana", Font.ITALIC, 14));
        novo.setBounds(20, 500, 85, 20);
        novo.setEnabled(false);
        painelCondomino.add(novo);
        novo.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(120, 500, 85, 20);
        editar.setEnabled(false);
        painelCondomino.add(editar);
        editar.addActionListener(this);

        apagar = new JButton("Apagar");
        apagar.setName("apagar");
        apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagar.setBounds(220, 500, 85, 20);
        apagar.setEnabled(false);
        painelCondomino.add(apagar);
        apagar.addActionListener(this);

        gravar = new JButton("Gravar");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelCondomino.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelCondomino.add(fundo);

        painelCondomino.revalidate();
        painelCondomino.repaint();
        return painelCondomino;
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelCondomino,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelCondomino,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(painelCondomino, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);

            JOptionPane.showMessageDialog(painelCondomino, "O valor das quotas foram actualizados/recalculados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelCondomino,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condomínios para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaCondominios.size(); i++) {
            modeloListagemCondominios.addElement("*" + listaCondominios.get(i).getNome());
        }
    }

    /**
     * Método que carrega o nome dos condóminos de um determinado condomínio
     * para uma Jlist
     */
    public void carregarListagemCondominos() {
        modeloListagemCondominos.clear();
        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().size(); i++) {
            modeloListagemCondominos.addElement("*" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(i).getProprietario().getNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados do condómino
     * selecionado
     */
    public void carregarDadosCondomino() {
        nome.setEnabled(false);
        nome.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getNome());
        morada.setEnabled(false);
        morada.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getMorada());
        email.setEnabled(false);
        email.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getEmail());
        contactoTelefonico.setEnabled(false);
        contactoTelefonico.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getContactoTelefonico());
        nif.setEnabled(false);
        nif.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getNif());
        dataNascimento.setEnabled(false);
        dataNascimento.setValue(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getDataNascimento());
        profissao.setEnabled(false);
        profissao.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getProprietario().getProfissao());
        designacao.setEnabled(false);
        designacao.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getFracao().getDesignacao());
        permilagem.setEnabled(false);
        permilagem.setText("" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getFracao().getPermilagem());
        andar.setEnabled(false);
        andar.setText("" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getFracao().getAndar());
        taxaUtilizacao.setEnabled(false);
        taxaUtilizacao.setText("" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getFracao().getTaxaUtilizacao());
        quota.setEnabled(false);
        quota.setText("" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex()).
                getFracao().getQuota());

        gravar.setEnabled(false);
        novo.setEnabled(true);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelCondomino.revalidate();
        painelCondomino.repaint();
    }

    /**
     * Método utilizado para gravar um novo condómino no respectivo condomínio
     */
    public void gravar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty())
                || (this.morada.getText().isEmpty())
                || (this.email.getText().isEmpty())
                || (this.contactoTelefonico.getText().isEmpty())
                || (this.nif.getText().isEmpty())
                || (this.profissao.getText().isEmpty())
                || (this.designacao.getText().isEmpty())
                || (this.andar.getText().isEmpty())
                || (this.permilagem.getText().isEmpty())
                || (this.taxaUtilizacao.getText().isEmpty())
                || (this.quota.getText().isEmpty()
                && listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3)) {

            JOptionPane.showMessageDialog(painelCondomino, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String nome = this.nome.getText();
            String morada = this.morada.getText();
            String email = this.email.getText();

            int contactoTelefonico, nif;

            try {
                contactoTelefonico = Integer.parseInt(this.contactoTelefonico.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.contactoTelefonico.setText("");
                this.contactoTelefonico.requestFocusInWindow();
                return;
            }

            try {
                nif = Integer.parseInt(this.nif.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.nif.setText("");
                this.nif.requestFocusInWindow();
                return;
            }

            String profissao = this.profissao.getText();

            Date dataNascimento;

            try {
                dataNascimento = (Date) this.dataNascimento.getValue();
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita datas",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.dataNascimento.setValue(new SpinnerDateModel());
                this.dataNascimento.requestFocusInWindow();
                return;
            }

            novoProprietario = new Proprietario(nome, morada, email,
                    contactoTelefonico + "", nif + "", dataNascimento, profissao);

            String designacao = this.designacao.getText();

            int andar;
            Double permilagem, taxaUtilizacao;
            Double quota = 0.0;

            try {
                permilagem = Double.parseDouble(this.permilagem.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.permilagem.setText("");
                this.permilagem.requestFocusInWindow();
                return;
            }

            try {
                andar = Integer.parseInt(this.andar.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.andar.setText("");
                this.andar.requestFocusInWindow();
                return;
            }

            try {
                taxaUtilizacao = Double.parseDouble(this.taxaUtilizacao.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.taxaUtilizacao.setText("");
                this.taxaUtilizacao.requestFocusInWindow();
                return;
            }

            if (listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3) {
                try {
                    quota = Double.parseDouble(this.quota.getText());
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.quota.setText("");
                    this.quota.requestFocusInWindow();
                    return;
                }
            }

            novoFracao = new Fracao(designacao, permilagem, andar, taxaUtilizacao);
            novoCondomino = new Condomino(novoProprietario, novoFracao);

            int confirmacao = JOptionPane.showConfirmDialog(painelCondomino,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                editarCondominio = listaCondominios.get(listagemCondominios.getSelectedIndex());
                editarCondominio.registarCondomino(novoCondomino);
                if (listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3) {
                    novoCondomino.getFracao().setQuota(quota);
                }

                guardar();

                this.nome.setEnabled(false);
                this.nome.setText("");
                this.morada.setEnabled(false);
                this.morada.setText("");
                this.email.setEnabled(false);
                this.email.setText("");
                this.contactoTelefonico.setEnabled(false);
                this.contactoTelefonico.setText("");
                this.nif.setEnabled(false);
                this.nif.setText("");
                this.dataNascimento.setEnabled(false);
                this.profissao.setEnabled(false);
                this.profissao.setText("");
                this.designacao.setEnabled(false);
                this.designacao.setText("");
                this.permilagem.setEnabled(false);
                this.permilagem.setText("");
                this.andar.setEnabled(false);
                this.andar.setText("");
                this.taxaUtilizacao.setEnabled(false);
                this.taxaUtilizacao.setText("");
                this.quota.setEnabled(false);
                this.quota.setText("");

                modeloListagemCondominos.addElement("*" + novoCondomino.getProprietario().
                        getNome());

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelCondomino.revalidate();
                painelCondomino.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar um determinado condómino
     */
    public void editar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty())
                || (this.morada.getText().isEmpty())
                || (this.email.getText().isEmpty())
                || (this.contactoTelefonico.getText().isEmpty())
                || (this.nif.getText().isEmpty())
                || (this.profissao.getText().isEmpty())
                || (this.designacao.getText().isEmpty())
                || (this.andar.getText().isEmpty())
                || (this.permilagem.getText().isEmpty())
                || (this.taxaUtilizacao.getText().isEmpty())
                || (this.quota.getText().isEmpty()
                && listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3)) {

            JOptionPane.showMessageDialog(painelCondomino, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(painelCondomino,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarCondomino = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex());

                editarCondomino.getProprietario().setNome(this.nome.getText());
                editarCondomino.getProprietario().setMorada(this.morada.getText());
                editarCondomino.getProprietario().setEmail(this.email.getText());

                int contactoTelefonico, nif;

                try {
                    contactoTelefonico = Integer.parseInt(this.contactoTelefonico.getText());
                    editarCondomino.getProprietario().setContactoTelefonico(this.contactoTelefonico.getText());
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.contactoTelefonico.setText("");
                    this.contactoTelefonico.requestFocusInWindow();
                    return;
                }

                try {
                    nif = Integer.parseInt(this.nif.getText());
                    editarCondomino.getProprietario().setNif(this.nif.getText());

                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.nif.setText("");
                    this.nif.requestFocusInWindow();
                    return;
                }

                editarCondomino.getProprietario().setProfissao(this.profissao.getText());
                Date novaDataNascimento = (Date) this.dataNascimento.getValue();
                editarCondomino.getProprietario().setDataNascimento(novaDataNascimento);
                editarCondomino.getFracao().setDesignacao(this.designacao.getText());

                int andar;
                Double permilagem, taxaUtilizacao, quota;

                try {
                    permilagem = Double.parseDouble(this.permilagem.getText());
                    editarCondomino.getFracao().setPermilagem(permilagem);
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.permilagem.setText("");
                    this.permilagem.requestFocusInWindow();
                    return;
                }

                try {
                    andar = Integer.parseInt(this.andar.getText());
                    editarCondomino.getFracao().setAndar(andar);
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.andar.setText("");
                    this.andar.requestFocusInWindow();
                    return;
                }

                try {
                    taxaUtilizacao = Double.parseDouble(this.taxaUtilizacao.getText());
                    editarCondomino.getFracao().setTaxaUtilizacao(taxaUtilizacao);
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.taxaUtilizacao.setText("");
                    this.taxaUtilizacao.requestFocusInWindow();
                    return;
                }

                if (listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3) {
                    try {
                        quota = Double.parseDouble(this.quota.getText());
                        listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().
                                get(listagemCondominos.getSelectedIndex()).getFracao().setQuota(quota);
                    } catch (Exception z) {
                        JOptionPane.showMessageDialog(painelCondomino, "Este campo apenas aceita números",
                                "Erro", JOptionPane.ERROR_MESSAGE);
                        this.quota.setText("");
                        this.quota.requestFocusInWindow();
                        return;
                    }
                }
            }

            editarCondominio = listaCondominios.get(listagemCondominios.getSelectedIndex());
            if (editarCondominio.getModalidadeCalculoQuota() != 3) {
                listaCondominios.get(listagemCondominios.getSelectedIndex()).calculoValorFracao();
            }

            guardar();

            this.nome.setEnabled(false);
            this.nome.setText("");
            this.morada.setEnabled(false);
            this.morada.setText("");
            this.email.setEnabled(false);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(false);
            this.contactoTelefonico.setText("");
            this.nif.setEnabled(false);
            this.nif.setText("");
            this.dataNascimento.setEnabled(false);
            this.profissao.setEnabled(false);
            this.profissao.setText("");
            this.designacao.setEnabled(false);
            this.designacao.setText("");
            this.permilagem.setEnabled(false);
            this.permilagem.setText("");
            this.andar.setEnabled(false);
            this.andar.setText("");
            this.taxaUtilizacao.setEnabled(false);
            this.taxaUtilizacao.setText("");
            this.quota.setEnabled(false);
            this.quota.setText("");

            listagemCondominos.setEnabled(true);
            listagemCondominios.setEnabled(true);
            modeloListagemCondominos.setElementAt("*" + editarCondomino.getProprietario().getNome(), listagemCondominos.getSelectedIndex());

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelCondomino.revalidate();
            painelCondomino.repaint();
        }
    }

    /**
     * Método utilizado para apagar um determinado condómino
     */
    public void apagar() {
        int confirmacao = JOptionPane.showConfirmDialog(painelCondomino,
                "Pretende apagar o condómino?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarCondomino = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(listagemCondominos.getSelectedIndex());
            listaCondominios.get(listagemCondominios.getSelectedIndex()).eliminarCondomino(apagarCondomino);

            guardar();

            JOptionPane.showMessageDialog(painelCondomino,
                    "Condómino eliminado com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.nome.setEnabled(false);
            this.nome.setText("");
            this.morada.setEnabled(false);
            this.morada.setText("");
            this.email.setEnabled(false);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(false);
            this.contactoTelefonico.setText("");
            this.nif.setEnabled(false);
            this.nif.setText("");
            this.dataNascimento.setEnabled(false);
            this.profissao.setEnabled(false);
            this.profissao.setText("");
            this.designacao.setEnabled(false);
            this.designacao.setText("");
            this.permilagem.setEnabled(false);
            this.permilagem.setText("");
            this.andar.setEnabled(false);
            this.andar.setText("");
            this.taxaUtilizacao.setEnabled(false);
            this.taxaUtilizacao.setText("");
            this.quota.setEnabled(false);
            this.quota.setText("");

            modeloListagemCondominos.clear();
            listagemCondominos.setEnabled(false);
            listagemCondominios.setEnabled(true);
            listagemCondominos.clearSelection();
            listagemCondominios.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelCondomino.revalidate();
            painelCondomino.repaint();
        }
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoClicado = (JButton) e.getSource();
        String nomebotao = botaoClicado.getName();

        if (nomebotao.equals("novo")) {
            this.nome.setEnabled(true);
            this.nome.setText("");
            this.morada.setEnabled(true);
            this.morada.setText("");
            this.email.setEnabled(true);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(true);
            this.contactoTelefonico.setText("");
            this.nif.setEnabled(true);
            this.nif.setText("");
            this.dataNascimento.setEnabled(true);
            this.profissao.setEnabled(true);
            this.profissao.setText("");
            this.designacao.setEnabled(true);
            this.designacao.setText("");
            this.permilagem.setEnabled(true);
            this.permilagem.setText("");
            this.andar.setEnabled(true);
            this.andar.setText("");
            this.taxaUtilizacao.setEnabled(true);
            this.taxaUtilizacao.setText("");
            if (listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3) {
                this.quota.setEnabled(true);
            } else {
                this.quota.setEnabled(false);
            }
            this.quota.setText("");

            listagemCondominos.setEnabled(true);
            listagemCondominios.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelCondomino.revalidate();
            painelCondomino.repaint();

        } else if (nomebotao.equals("gravar")) {
            if (edicao == true) {
                editar();
            } else {
                gravar();
            }
        } else if (nomebotao.equals("editar")) {
            this.nome.setEnabled(true);
            this.morada.setEnabled(true);
            this.email.setEnabled(true);
            this.contactoTelefonico.setEnabled(true);
            this.nif.setEnabled(true);
            this.dataNascimento.setEnabled(true);
            this.profissao.setEnabled(true);
            this.designacao.setEnabled(true);
            this.permilagem.setEnabled(true);
            this.andar.setEnabled(true);
            this.taxaUtilizacao.setEnabled(true);
            if (listaCondominios.get(listagemCondominios.getSelectedIndex()).getModalidadeCalculoQuota() == 3) {
                this.quota.setEnabled(true);
            } else {
                this.quota.setEnabled(false);
            }

            listagemCondominos.setEnabled(true);
            listagemCondominios.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = true;

            painelCondomino.revalidate();
            painelCondomino.repaint();

        } else if (nomebotao.equals("apagar")) {
            apagar();
        }
    }
}
