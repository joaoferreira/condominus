package frontend;

import backend.*;
import static java.time.Instant.now;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 * A existência da classe PainelNotificacoes permite a criação do content panel
 * específico da janela principal para o menu Notificaçõesz
 *
 * @author JoaoFerreira
 */
public class PainelNotificacoes extends JPanel implements ActionListener {

    // Variáveis de Instância
    private JPanel painelNotificacoes;
    private JTextArea hoje, amanha, doisDias;
    private JScrollPane scrollHoje, scrollAmanha, scrollDoisDias;
    private ListaCondominios listaCondominios;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Notificações
     *
     * @return JPanel - Content panel do menu Condomínios
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelNotificacoes() throws IOException {

        carregar();

        painelNotificacoes = new JPanel(null);
        painelNotificacoes.setBackground(Color.white);

        JLabel titulo = new JLabel("Notificações");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(310, 20, 250, 50);
        painelNotificacoes.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelNotificacoes.add(separador);

        JLabel subtitulo1 = new JLabel("Notificações para hoje:");
        subtitulo1.setFont(new Font("Verdana", Font.BOLD, 16));
        subtitulo1.setBounds(100, 100, 250, 20);
        painelNotificacoes.add(subtitulo1);

        hoje = new JTextArea();
        hoje.setFont(new Font("Verdana", Font.PLAIN, 14));
        hoje.setEnabled(false);
        hoje.setLineWrap(true);
        hoje.setWrapStyleWord(true);

        scrollHoje = new JScrollPane(hoje);
        scrollHoje.setVerticalScrollBar(new JScrollBar());
        scrollHoje.setBackground(Color.RED);
        scrollHoje.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollHoje.setBounds(100, 135, 600, 65);

        painelNotificacoes.add(scrollHoje);

        JLabel subtitulo2 = new JLabel("Notificacoes para amanhã:");
        subtitulo2.setFont(new Font("Verdana", Font.BOLD, 14));
        subtitulo2.setBounds(100, 215, 250, 20);
        painelNotificacoes.add(subtitulo2);

        amanha = new JTextArea();
        amanha.setFont(new Font("Verdana", Font.PLAIN, 14));
        amanha.setEnabled(false);
        amanha.setLineWrap(true);
        amanha.setWrapStyleWord(true);

        scrollAmanha = new JScrollPane(amanha);
        scrollAmanha.setVerticalScrollBar(new JScrollBar());
        scrollAmanha.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollAmanha.setBackground(Color.YELLOW);
        scrollAmanha.setBounds(100, 250, 600, 65);

        painelNotificacoes.add(scrollAmanha);

        JLabel subtitulo3 = new JLabel("Notificações daqui a dois dias:");
        subtitulo3.setFont(new Font("Verdana", Font.BOLD, 14));
        subtitulo3.setBounds(100, 330, 250, 20);
        painelNotificacoes.add(subtitulo3);

        doisDias = new JTextArea();
        doisDias.setFont(new Font("Verdana", Font.PLAIN, 14));
        doisDias.setEnabled(false);
        doisDias.setLineWrap(true);
        doisDias.setWrapStyleWord(true);

        scrollDoisDias = new JScrollPane(doisDias);
        scrollDoisDias.setVerticalScrollBar(new JScrollBar());
        scrollDoisDias.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollDoisDias.setBackground(Color.GREEN);
        scrollDoisDias.setBounds(100, 365, 600, 65);

        painelNotificacoes.add(scrollDoisDias);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelNotificacoes.add(fundo);

        painelNotificacoes.revalidate();
        painelNotificacoes.repaint();
        verificarNotificacoes();
        return painelNotificacoes;
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelNotificacoes,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelNotificacoes,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que atribui aos respectivos campos para hoje, amanhã e para daqui
     * a dois dias as respectivas notificações existentes
     *
     */
    public void verificarNotificacoes() {
        Date hoje = new Date();
        Date amanha = Date.from(now().plus(1, ChronoUnit.DAYS));
        Date doisdias = Date.from(now().plus(2, ChronoUnit.DAYS));
        this.hoje.setText(listaCondominios.notificacoes(hoje));
        this.amanha.setText(listaCondominios.notificacoes(amanha));
        this.doisDias.setText(listaCondominios.notificacoes(doisdias));
    }

    /**
     * Override do método actionPerfomed
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
        // Override para compilação sem erros
    }

}
