package frontend;

import backend.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelCondominio permite a criação do content panel
 * específico da janela principal para o menu Condomínios
 *
 * @author JoaoFerreira
 */
public class PainelCondominio extends JPanel implements ActionListener {

    // Variáveis de Instância
    private JPanel painelCondominio;
    private JTextField nome, morada, codigoPostal1, codigoPostal2, localidade,
            estimativaDespesas, honorarios;
    private JComboBox modalidadeCalculoQuota;
    private JList listagem;
    private final DefaultListModel modeloListagem = new DefaultListModel();
    private JScrollPane scrollListagem;
    private JButton novo, gravar, editar, apagar, discriminar;
    private ListaCondominios listaCondominios;
    private Condominio novoCondominio, editarCondominio, apagarCondominio;
    private JDialog discriminarDespesas;
    private JPanel discriminarDespesasPanel;
    private JTextField valorDespesasAdministrativas, valorLimpezas, valorReparacoes, valorElevadores, valorOutras;
    private JButton ok;
    boolean edicao = false;
    boolean calculoManual = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Condomínios
     *
     * @return JPanel - Content panel do menu Condomínios
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelCondominio() throws IOException {

        carregar();
        carregarListagem();

        painelCondominio = new JPanel(null);
        painelCondominio.setBackground(Color.white);

        JLabel titulo = new JLabel("Condomínios");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(275, 20, 250, 50);
        painelCondominio.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelCondominio.add(separador);

        JLabel labelListagem = new JLabel("Lista Condomínios:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelCondominio.add(labelListagem);

        listagem = new JList(modeloListagem);
        listagem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagem.setLayoutOrientation(JList.VERTICAL);
        listagem.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagem.setFixedCellHeight(20);
        listagem.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagem.getSelectedIndex() != -1) {
                        carregarDadosCondominio();
                    }
                } else if (listagem.getSelectedIndex() != -1) {
                    carregarDadosCondominio();
                } else {
                    nome.setEnabled(false);
                    nome.setText("");
                    morada.setEnabled(false);
                    morada.setText("");
                    codigoPostal1.setEnabled(false);
                    codigoPostal1.setText("");
                    codigoPostal2.setEnabled(false);
                    codigoPostal2.setText("");
                    localidade.setEnabled(false);
                    localidade.setText("");
                    estimativaDespesas.setEnabled(false);
                    estimativaDespesas.setText("");
                    honorarios.setEnabled(false);
                    honorarios.setText("");
                    modalidadeCalculoQuota.setEnabled(false);
                    novo.setEnabled(true);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                    apagar.setEnabled(false);
                }
            }
        });

        scrollListagem = new JScrollPane(listagem);
        scrollListagem.setVerticalScrollBar(new JScrollBar());
        scrollListagem.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagem.setBounds(20, 150, 200, 300);

        painelCondominio.add(scrollListagem);

        JLabel labelNome = new JLabel("Nome do Condomínio");
        labelNome.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelNome.setBounds(240, 150, 155, 20);
        painelCondominio.add(labelNome);

        nome = new JTextField();
        nome.setFont(new Font("Verdana", Font.PLAIN, 14));
        nome.setBounds(410, 150, 350, 20);
        nome.setEnabled(false);
        painelCondominio.add(nome);

        JLabel labelMorada = new JLabel("Morada");
        labelMorada.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelMorada.setBounds(240, 185, 60, 20);
        painelCondominio.add(labelMorada);

        morada = new JTextField();
        morada.setFont(new Font("Verdana", Font.PLAIN, 14));
        morada.setBounds(315, 185, 445, 20);
        morada.setEnabled(false);
        painelCondominio.add(morada);

        JLabel labelCodigoPostal = new JLabel("Código Postal");
        labelCodigoPostal.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelCodigoPostal.setBounds(240, 220, 100, 20);
        painelCondominio.add(labelCodigoPostal);

        codigoPostal1 = new JTextField();
        codigoPostal1.setFont(new Font("Verdana", Font.PLAIN, 14));
        codigoPostal1.setBounds(355, 220, 60, 20);
        codigoPostal1.setEnabled(false);
        painelCondominio.add(codigoPostal1);

        codigoPostal2 = new JTextField();
        codigoPostal2.setFont(new Font("Verdana", Font.PLAIN, 14));
        codigoPostal2.setBounds(430, 220, 50, 20);
        codigoPostal2.setEnabled(false);
        painelCondominio.add(codigoPostal2);

        JLabel labelLocalidade = new JLabel("Localidade");
        labelLocalidade.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelLocalidade.setBounds(495, 220, 75, 20);
        painelCondominio.add(labelLocalidade);

        localidade = new JTextField();
        localidade.setFont(new Font("Verdana", Font.PLAIN, 14));
        localidade.setBounds(585, 220, 175, 20);
        localidade.setEnabled(false);
        painelCondominio.add(localidade);

        JLabel labelEstimativaDespesas = new JLabel("Estimativa Despesas");
        labelEstimativaDespesas.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelEstimativaDespesas.setBounds(240, 255, 150, 20);
        painelCondominio.add(labelEstimativaDespesas);

        estimativaDespesas = new JTextField();
        estimativaDespesas.setFont(new Font("Verdana", Font.PLAIN, 14));
        estimativaDespesas.setBounds(395, 255, 100, 20);
        estimativaDespesas.setEnabled(false);
        painelCondominio.add(estimativaDespesas);

        discriminar = new JButton("Inserir");
        discriminar.setName("discriminar");
        discriminar.setFont(new Font("Verdana", Font.ITALIC, 14));
        discriminar.setBounds(510, 255, 85, 20);
        discriminar.setEnabled(false);
        painelCondominio.add(discriminar);
        discriminar.addActionListener(this);

        desenharDiscriminarDespesasJDialog();

        JLabel labelHonorarios = new JLabel("Honorários");
        labelHonorarios.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelHonorarios.setBounds(240, 290, 80, 20);
        painelCondominio.add(labelHonorarios);

        honorarios = new JTextField();
        honorarios.setFont(new Font("Verdana", Font.PLAIN, 14));
        honorarios.setBounds(335, 290, 100, 20);
        honorarios.setEnabled(false);
        painelCondominio.add(honorarios);

        JLabel labelModalidadeCalculoQuota = new JLabel("Modalidade de Cálculo da Quota");
        labelModalidadeCalculoQuota.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelModalidadeCalculoQuota.setBounds(240, 325, 250, 20);
        painelCondominio.add(labelModalidadeCalculoQuota);

        String[] lista = {"Permilagem", "Equitativa", "Exacta", "Manual"};
        modalidadeCalculoQuota = new JComboBox(lista);
        modalidadeCalculoQuota.setFont(new Font("Verdana", Font.ITALIC, 14));
        modalidadeCalculoQuota.setBounds(240, 360, 230, 20);
        modalidadeCalculoQuota.setEditable(false);
        modalidadeCalculoQuota.setEnabled(false);
        painelCondominio.add(modalidadeCalculoQuota);

        novo = new JButton("Novo");
        novo.setName("novo");
        novo.setFont(new Font("Verdana", Font.ITALIC, 14));
        novo.setBounds(20, 500, 85, 20);
        painelCondominio.add(novo);
        novo.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(120, 500, 85, 20);
        editar.setEnabled(false);
        painelCondominio.add(editar);
        editar.addActionListener(this);

        apagar = new JButton("Apagar");
        apagar.setName("apagar");
        apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagar.setBounds(220, 500, 85, 20);
        apagar.setEnabled(false);
        painelCondominio.add(apagar);
        apagar.addActionListener(this);

        gravar = new JButton("Gravar");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelCondominio.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelCondominio.add(fundo);

        painelCondominio.revalidate();
        painelCondominio.repaint();
        return painelCondominio;
    }

    /**
     * Método utilizado para desenhar a JDialog específica para inserir as
     * várias despesas do condomínio
     *
     */
    public void desenharDiscriminarDespesasJDialog() {
        discriminarDespesas = new JDialog();
        discriminarDespesasPanel = new JPanel(null);
        discriminarDespesasPanel.setBackground(Color.white);
        discriminarDespesas.setContentPane(discriminarDespesasPanel);

        discriminarDespesas.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        discriminarDespesas.setPreferredSize(new Dimension(400, 300));

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        discriminarDespesas.setLocation(dim.width / 2 - 400 / 2, dim.height / 2 - 300 / 2);
        discriminarDespesas.setResizable(false);

        JLabel tituloJDialog = new JLabel("Inserir Despesas");
        tituloJDialog.setBounds(120, 20, 200, 40);
        tituloJDialog.setFont(new Font("Verdana", Font.BOLD, 18));
        discriminarDespesasPanel.add(tituloJDialog);

        JLabel despesasAdministrativas = new JLabel("Despesas Administrativas");
        despesasAdministrativas.setBounds(20, 70, 250, 20);
        despesasAdministrativas.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(despesasAdministrativas);

        valorDespesasAdministrativas = new JTextField();
        valorDespesasAdministrativas.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorDespesasAdministrativas.setBounds(280, 70, 70, 20);
        valorDespesasAdministrativas.setEnabled(true);
        discriminarDespesasPanel.add(valorDespesasAdministrativas);

        JLabel limpezas = new JLabel("Serviços Limpeza");
        limpezas.setBounds(20, 100, 250, 20);
        limpezas.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(limpezas);

        valorLimpezas = new JTextField();
        valorLimpezas.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorLimpezas.setBounds(280, 100, 70, 20);
        valorLimpezas.setEnabled(true);
        discriminarDespesasPanel.add(valorLimpezas);

        JLabel reparacoes = new JLabel("Pequenas Reparações");
        reparacoes.setBounds(20, 130, 250, 20);
        reparacoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(reparacoes);

        valorReparacoes = new JTextField();
        valorReparacoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorReparacoes.setBounds(280, 130, 70, 20);
        valorReparacoes.setEnabled(true);
        discriminarDespesasPanel.add(valorReparacoes);

        JLabel elevadores = new JLabel("Manutenção Elevadores");
        elevadores.setBounds(20, 160, 250, 20);
        elevadores.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(elevadores);

        valorElevadores = new JTextField();
        valorElevadores.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorElevadores.setBounds(280, 160, 70, 20);
        valorElevadores.setEnabled(true);
        discriminarDespesasPanel.add(valorElevadores);

        JLabel outras = new JLabel("Outras Despesas");
        outras.setBounds(20, 190, 250, 20);
        outras.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(outras);

        valorOutras = new JTextField();
        valorOutras.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorOutras.setBounds(280, 190, 70, 20);
        valorOutras.setEnabled(true);
        discriminarDespesasPanel.add(valorOutras);

        ok = new JButton("OK");
        ok.setName("ok");
        ok.setFont(new Font("Verdana", Font.ITALIC, 14));
        ok.setBounds(158, 220, 85, 20);
        ok.setEnabled(true);
        discriminarDespesasPanel.add(ok);
        ok.addActionListener(this);

        discriminarDespesasPanel.revalidate();
        discriminarDespesasPanel.repaint();

        discriminarDespesas.pack();
        discriminarDespesas.setVisible(false);
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelCondominio,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelCondominio,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(this, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);

            if (edicao == true) {
                JOptionPane.showMessageDialog(this, "O valor das quotas foram actualizados/recalculados com sucesso",
                        "Terminado", JOptionPane.INFORMATION_MESSAGE);
            }

            if (calculoManual == true) {
                JOptionPane.showMessageDialog(this, "Atenção: Modalidade de cálculo de quotas manual, estas deverão ser introduzidas no menu condómino",
                        "Aviso", JOptionPane.INFORMATION_MESSAGE);
                calculoManual = false;
            }
        } catch (IOException i) {
            JOptionPane.showMessageDialog(this,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condomínios para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaCondominios.size(); i++) {
            modeloListagem.addElement("*" + listaCondominios.get(i).getNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados do condomínio
     * selecionado
     */
    public void carregarDadosCondominio() {
        nome.setEnabled(false);
        nome.setText(listaCondominios.get(listagem.getSelectedIndex()).getNome());
        morada.setEnabled(false);
        morada.setText(listaCondominios.get(listagem.getSelectedIndex()).getMorada());
        codigoPostal1.setEnabled(false);
        codigoPostal1.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getCodigoPostal1());
        codigoPostal2.setEnabled(false);
        codigoPostal2.setText(listaCondominios.get(listagem.getSelectedIndex()).getCodigoPostal2());
        localidade.setEnabled(false);
        localidade.setText(listaCondominios.get(listagem.getSelectedIndex()).getLocalidade());
        estimativaDespesas.setEnabled(false);
        estimativaDespesas.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getEstimativaDespesas());
        honorarios.setEnabled(false);
        honorarios.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getHonorarios());
        modalidadeCalculoQuota.setEnabled(false);
        modalidadeCalculoQuota.setSelectedIndex(listaCondominios.get(listagem.getSelectedIndex()).getModalidadeCalculoQuota());

        gravar.setEnabled(false);
        novo.setEnabled(true);
        discriminar.setEnabled(false);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelCondominio.revalidate();
        painelCondominio.repaint();
    }

    /**
     * Método utilizado para gravar um novo condomínio
     */
    public void gravar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty()) || (this.morada.getText().isEmpty())
                || (this.codigoPostal1.getText().isEmpty())
                || (this.codigoPostal2.getText().isEmpty())
                || (this.localidade.getText().isEmpty())
                || (this.estimativaDespesas.getText().isEmpty())
                || (this.honorarios.getText().isEmpty())) {

            JOptionPane.showMessageDialog(this, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String nome = this.nome.getText();
            String morada = this.morada.getText();

            int codigoPostal1, codigoPostal2;
            Double honorarios;

            try {
                codigoPostal1 = Integer.parseInt(this.codigoPostal1.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.codigoPostal1.setText("");
                this.codigoPostal1.requestFocusInWindow();
                return;
            }

            try {
                codigoPostal2 = Integer.parseInt(this.codigoPostal2.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.codigoPostal2.setText("");
                this.codigoPostal2.requestFocusInWindow();
                return;
            }

            String localidade = this.localidade.getText();

            try {
                honorarios = Double.parseDouble(this.honorarios.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.honorarios.setText("");
                this.honorarios.requestFocusInWindow();
                return;
            }

            int modalidadeCalculoQuota = this.modalidadeCalculoQuota.getSelectedIndex();

            novoCondominio = new Condominio(nome, morada, codigoPostal1,
                    this.codigoPostal2.getText(), localidade, Double.parseDouble(this.estimativaDespesas.getText()),
                    Double.parseDouble(this.valorDespesasAdministrativas.getText()),
                    Double.parseDouble(this.valorLimpezas.getText()),
                    Double.parseDouble(this.valorReparacoes.getText()),
                    Double.parseDouble(this.valorElevadores.getText()),
                    Double.parseDouble(this.valorOutras.getText()),
                    honorarios, modalidadeCalculoQuota);

            int confirmacao = JOptionPane.showConfirmDialog(this,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                listaCondominios.registarCondominio(novoCondominio);
                if (novoCondominio.getModalidadeCalculoQuota() == 3) {
                    calculoManual = true;
                }

                guardar();

                this.nome.setEnabled(false);
                this.nome.setText("");
                this.morada.setEnabled(false);
                this.morada.setText("");
                this.codigoPostal1.setEnabled(false);
                this.codigoPostal1.setText("");
                this.codigoPostal2.setEnabled(false);
                this.codigoPostal2.setText("");
                this.localidade.setEnabled(false);
                this.localidade.setText("");
                this.estimativaDespesas.setEnabled(false);
                this.estimativaDespesas.setText("");
                this.honorarios.setEnabled(false);
                this.honorarios.setText("");
                this.modalidadeCalculoQuota.setEnabled(false);

                modeloListagem.addElement("*" + novoCondominio.getNome());
                listagem.clearSelection();

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                discriminar.setEnabled(false);
                novo.setEnabled(true);

                painelCondominio.revalidate();
                painelCondominio.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar um determinado condomínio
     */
    public void editar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty()) || (this.morada.getText().isEmpty())
                || (this.codigoPostal1.getText().isEmpty())
                || (this.codigoPostal2.getText().isEmpty())
                || (this.localidade.getText().isEmpty())
                || (this.estimativaDespesas.getText().isEmpty())
                || (this.honorarios.getText().isEmpty())) {

            JOptionPane.showMessageDialog(painelCondominio, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(this,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarCondominio = listaCondominios.get(listagem.getSelectedIndex());

                editarCondominio.setNome(this.nome.getText());
                editarCondominio.setMorada(this.morada.getText());

                int codigoPostal1, codigoPostal2;
                Double honorarios;

                try {
                    editarCondominio.setCodigoPostal1(Integer.parseInt(this.codigoPostal1.getText()));
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.codigoPostal1.setText("");
                    this.codigoPostal1.requestFocusInWindow();
                    return;
                }

                try {
                    codigoPostal2 = Integer.parseInt(this.codigoPostal2.getText());
                    editarCondominio.setCodigoPostal2(this.codigoPostal2.getText());
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.codigoPostal2.setText("");
                    this.codigoPostal2.requestFocusInWindow();
                    return;
                }

                editarCondominio.setLocalidade(this.localidade.getText());
                editarCondominio.setEstimativaDespesas(Double.parseDouble(this.estimativaDespesas.getText()));

                try {
                    editarCondominio.setHonorarios(Double.parseDouble(this.honorarios.getText()));
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.honorarios.setText("");
                    this.honorarios.requestFocusInWindow();
                    return;
                }

                editarCondominio.setModalidadeCalculoQuota(this.modalidadeCalculoQuota.getSelectedIndex());

                try {
                    editarCondominio.setValorDespesasAdministrativas(Double.parseDouble(this.valorDespesasAdministrativas.getText()));
                    editarCondominio.setValorLimpezas(Double.parseDouble(this.valorLimpezas.getText()));
                    editarCondominio.setValorReparacoes(Double.parseDouble(this.valorReparacoes.getText()));
                    editarCondominio.setValorElevadores(Double.parseDouble(this.valorElevadores.getText()));
                    editarCondominio.setValorOutras(Double.parseDouble(this.valorOutras.getText()));
                } catch (Exception z) {

                }

                if (editarCondominio.getModalidadeCalculoQuota() == 3) {
                    calculoManual = true;
                }

                guardar();

                this.nome.setEnabled(false);
                this.nome.setText("");
                this.morada.setEnabled(false);
                this.morada.setText("");
                this.codigoPostal1.setEnabled(false);
                this.codigoPostal1.setText("");
                this.codigoPostal2.setEnabled(false);
                this.codigoPostal2.setText("");
                this.localidade.setEnabled(false);
                this.localidade.setText("");
                this.estimativaDespesas.setEnabled(false);
                this.estimativaDespesas.setText("");
                this.honorarios.setEnabled(false);
                this.honorarios.setText("");
                this.modalidadeCalculoQuota.setEnabled(false);

                listagem.setEnabled(true);
                modeloListagem.setElementAt("*" + editarCondominio.getNome(), listagem.getSelectedIndex());
                listagem.clearSelection();

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                discriminar.setEnabled(false);
                novo.setEnabled(true);

                painelCondominio.revalidate();
                painelCondominio.repaint();
            }
        }
    }

    /**
     * Método utilizado para apagar um determinado condomínio
     */
    public void apagar() {
        int confirmacao = JOptionPane.showConfirmDialog(this,
                "Pretende apagar o condomínio?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarCondominio = listaCondominios.get(listagem.getSelectedIndex());
            listaCondominios.eliminarCondominio(apagarCondominio);

            guardar();

            JOptionPane.showMessageDialog(this,
                    "Condomínio eliminado com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.nome.setEnabled(false);
            this.nome.setText("");
            this.morada.setEnabled(false);
            this.morada.setText("");
            this.codigoPostal1.setEnabled(false);
            this.codigoPostal1.setText("");
            this.codigoPostal2.setEnabled(false);
            this.codigoPostal2.setText("");
            this.localidade.setEnabled(false);
            this.localidade.setText("");
            this.estimativaDespesas.setEnabled(false);
            this.estimativaDespesas.setText("");
            this.honorarios.setEnabled(false);
            this.honorarios.setText("");
            this.modalidadeCalculoQuota.setEnabled(false);

            modeloListagem.remove(listagem.getSelectedIndex());
            listagem.setEnabled(true);
            listagem.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            discriminar.setEnabled(false);
            novo.setEnabled(true);

            painelCondominio.revalidate();
            painelCondominio.repaint();
        }
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoclicado = (JButton) e.getSource();
        String nomeBotao = botaoclicado.getName();

        if (nomeBotao.equals("novo")) {
            nome.setEnabled(true);
            nome.setText("");
            morada.setEnabled(true);
            morada.setText("");
            codigoPostal1.setEnabled(true);
            codigoPostal1.setText("");
            codigoPostal2.setEnabled(true);
            codigoPostal2.setText("");
            localidade.setEnabled(true);
            localidade.setText("");
            estimativaDespesas.setEnabled(false);
            estimativaDespesas.setText("");
            honorarios.setEnabled(true);
            honorarios.setText("");
            modalidadeCalculoQuota.setEnabled(true);

            listagem.setEnabled(true);

            gravar.setEnabled(true);
            discriminar.setEnabled(true);
            novo.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelCondominio.revalidate();
            painelCondominio.repaint();

        } else if (nomeBotao.equals("gravar")) {
            if (edicao == true) {
                editar();
            } else {
                gravar();
            }
        } else if (nomeBotao.equals("editar")) {
            nome.setEnabled(true);
            morada.setEnabled(true);
            codigoPostal1.setEnabled(true);
            codigoPostal2.setEnabled(true);
            localidade.setEnabled(true);
            estimativaDespesas.setEnabled(false);
            honorarios.setEnabled(true);
            modalidadeCalculoQuota.setEnabled(true);

            listagem.setEnabled(false);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            discriminar.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = true;

            painelCondominio.revalidate();
            painelCondominio.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagar();
        } else if (nomeBotao.equals("discriminar")) {
            discriminarDespesas.setVisible(true);
            if ((listagem.getSelectedIndex() != -1) && (edicao == true)) {
                valorDespesasAdministrativas.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getValorDespesasAdministrativas());
                valorLimpezas.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getValorLimpezas());
                valorReparacoes.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getValorReparacoes());
                valorElevadores.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getValorElevadores());
                valorOutras.setText("" + listaCondominios.get(listagem.getSelectedIndex()).getValorOutras());
            } else {
                valorDespesasAdministrativas.setText("");
                valorLimpezas.setText("");
                valorReparacoes.setText("");
                valorElevadores.setText("");
                valorOutras.setText("");
            }
        } else if (nomeBotao.equals("ok")) {
            Double soma, valorDespesasAdministrativas, valorLimpezas, valorReparacoes, valorElevadores, valorOutras;

            try {
                valorDespesasAdministrativas = Double.parseDouble(this.valorDespesasAdministrativas.getText());
                valorLimpezas = Double.parseDouble(this.valorLimpezas.getText());
                valorReparacoes = Double.parseDouble(this.valorReparacoes.getText());
                valorElevadores = Double.parseDouble(this.valorElevadores.getText());
                valorOutras = Double.parseDouble(this.valorOutras.getText());
                soma = valorDespesasAdministrativas + valorLimpezas
                        + valorReparacoes + valorElevadores + valorOutras;
                estimativaDespesas.setText("" + soma);
                discriminarDespesas.dispose();
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                return;
            }

        }
    }
}
