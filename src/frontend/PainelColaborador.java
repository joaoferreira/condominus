package frontend;

import backend.*;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelColaborador permite a criação do content panel
 * específico da janela principal para o menu Colaboradores
 *
 * @author JoaoFerreira
 */
public class PainelColaborador extends JPanel implements ActionListener {

    // Variáveis de Instância
    private JPanel painelColaborador;
    private JTextField primeiroNome, ultimoNome, morada, contactoTelefonico,
            email, habilitacoes, funcoes;
    private JSpinner dataNascimento;
    private JList listagem;
    private JScrollPane scrollListagem;
    private final DefaultListModel modeloListagem = new DefaultListModel();
    private JButton novo, gravar, editar, apagar;
    private ListaColaboradores listaColaboradores;
    private Colaborador novoColaborador, editarColaborador, apagarColaborador;
    boolean edicao = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Colaboradores
     *
     * @return JPanel - Content panel do menu Colaboradores
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelColaborador() throws IOException {

        carregar();
        carregarListagem();

        painelColaborador = new JPanel(null);
        painelColaborador.setBackground(Color.white);

        JLabel titulo = new JLabel("Colaboradores");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(275, 20, 250, 50);
        painelColaborador.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelColaborador.add(separador);

        JLabel labelListagem = new JLabel("Lista Colaboradores:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelColaborador.add(labelListagem);

        listagem = new JList(modeloListagem);
        listagem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagem.setLayoutOrientation(JList.VERTICAL);
        listagem.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagem.setFixedCellHeight(20);
        listagem.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagem.getSelectedIndex() != -1) {
                        carregarDadosColaborador();
                    }
                } else if (listagem.getSelectedIndex() != -1) {
                    carregarDadosColaborador();
                } else {
                    primeiroNome.setEnabled(false);
                    primeiroNome.setText("");
                    ultimoNome.setEnabled(false);
                    ultimoNome.setText("");
                    morada.setEnabled(false);
                    morada.setText("");
                    dataNascimento.setEnabled(false);
                    email.setEnabled(false);
                    email.setText("");
                    contactoTelefonico.setEnabled(false);
                    contactoTelefonico.setText("");
                    habilitacoes.setEnabled(false);
                    habilitacoes.setText("");
                    funcoes.setEnabled(false);
                    funcoes.setText("");
                    novo.setEnabled(true);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                    apagar.setEnabled(false);
                }
            }
        });

        scrollListagem = new JScrollPane(listagem);
        scrollListagem.setVerticalScrollBar(new JScrollBar());
        scrollListagem.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagem.setBounds(20, 150, 150, 300);

        painelColaborador.add(scrollListagem);

        JLabel labelPrimeiroNome = new JLabel("Nome do Colaborador");
        labelPrimeiroNome.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelPrimeiroNome.setBounds(190, 150, 155, 20);
        painelColaborador.add(labelPrimeiroNome);

        primeiroNome = new JTextField();
        primeiroNome.setFont(new Font("Verdana", Font.PLAIN, 14));
        primeiroNome.setBounds(360, 150, 210, 20);
        primeiroNome.setEnabled(false);
        painelColaborador.add(primeiroNome);

        JLabel labelUltimoNome = new JLabel("Apelido do Colaborador");
        labelUltimoNome.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelUltimoNome.setBounds(190, 185, 170, 20);
        painelColaborador.add(labelUltimoNome);

        ultimoNome = new JTextField();
        ultimoNome.setFont(new Font("Verdana", Font.PLAIN, 14));
        ultimoNome.setBounds(375, 185, 200, 20);
        ultimoNome.setEnabled(false);
        painelColaborador.add(ultimoNome);

        JLabel labelMorada = new JLabel("Morada");
        labelMorada.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelMorada.setBounds(190, 220, 60, 20);
        painelColaborador.add(labelMorada);

        morada = new JTextField();
        morada.setFont(new Font("Verdana", Font.PLAIN, 14));
        morada.setBounds(275, 220, 510, 20);
        morada.setEnabled(false);
        painelColaborador.add(morada);

        JLabel labelDataNascimento = new JLabel("Data de Nascimento");
        labelDataNascimento.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDataNascimento.setBounds(190, 255, 150, 20);
        painelColaborador.add(labelDataNascimento);

        dataNascimento = new JSpinner(new SpinnerDateModel());
        dataNascimento.setEditor(new JSpinner.DateEditor(dataNascimento, "dd/MM/yyyy"));
        dataNascimento.setFont(new Font("Verdana", Font.PLAIN, 14));
        dataNascimento.setBounds(355, 255, 125, 20);
        dataNascimento.setEnabled(false);
        painelColaborador.add(dataNascimento);

        JLabel labelContactoTelefonico = new JLabel("Contacto Telefónico");
        labelContactoTelefonico.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelContactoTelefonico.setBounds(495, 255, 140, 20);
        painelColaborador.add(labelContactoTelefonico);

        contactoTelefonico = new JTextField();
        contactoTelefonico.setFont(new Font("Verdana", Font.PLAIN, 14));
        contactoTelefonico.setBounds(660, 255, 125, 20);
        contactoTelefonico.setEnabled(false);
        painelColaborador.add(contactoTelefonico);

        JLabel labelEmail = new JLabel("e-mail");
        labelEmail.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelEmail.setBounds(190, 290, 60, 20);
        painelColaborador.add(labelEmail);

        email = new JTextField();
        email.setFont(new Font("Verdana", Font.PLAIN, 14));
        email.setBounds(265, 290, 520, 20);
        email.setEnabled(false);
        painelColaborador.add(email);

        JLabel labelHabilitacoes = new JLabel("Habilitações Académicas");
        labelHabilitacoes.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelHabilitacoes.setBounds(190, 325, 175, 20);
        painelColaborador.add(labelHabilitacoes);

        habilitacoes = new JTextField();
        habilitacoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        habilitacoes.setBounds(380, 325, 405, 20);
        habilitacoes.setEnabled(false);
        painelColaborador.add(habilitacoes);

        JLabel labelFuncoes = new JLabel("Funções");
        labelFuncoes.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelFuncoes.setBounds(190, 360, 60, 20);
        painelColaborador.add(labelFuncoes);

        funcoes = new JTextField();
        funcoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        funcoes.setBounds(265, 360, 520, 20);
        funcoes.setEnabled(false);
        painelColaborador.add(funcoes);

        novo = new JButton("Novo");
        novo.setName("novo");
        novo.setFont(new Font("Verdana", Font.ITALIC, 14));
        novo.setBounds(20, 500, 85, 20);
        painelColaborador.add(novo);
        novo.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(120, 500, 85, 20);
        editar.setEnabled(false);
        painelColaborador.add(editar);
        editar.addActionListener(this);

        apagar = new JButton("Apagar");
        apagar.setName("apagar");
        apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagar.setBounds(220, 500, 85, 20);
        apagar.setEnabled(false);
        painelColaborador.add(apagar);
        apagar.addActionListener(this);

        gravar = new JButton("Gravar");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelColaborador.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelColaborador.add(fundo);

        painelColaborador.revalidate();
        painelColaborador.repaint();
        return painelColaborador;
    }

    /**
     * Método que carrega a lista de colaboradores de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaColaboradores infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_colaboradores.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaColaboradores) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaColaboradores = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelColaborador,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaColaboradores = new ListaColaboradores();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelColaborador,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaColaboradores = new ListaColaboradores();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de colaboradores num ficheiro graças ao
     * interface Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_colaboradores.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaColaboradores);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(this, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelColaborador,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome e o apelido dos colaboradores para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaColaboradores.size(); i++) {
            modeloListagem.addElement("*" + listaColaboradores.get(i).getPrimeiroNome() + " "
                    + listaColaboradores.get(i).getUltimoNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados do colaborador
     * selecionado
     */
    public void carregarDadosColaborador() {
        primeiroNome.setEnabled(false);
        primeiroNome.setText(listaColaboradores.get(listagem.getSelectedIndex()).getPrimeiroNome());
        ultimoNome.setEnabled(false);
        ultimoNome.setText(listaColaboradores.get(listagem.getSelectedIndex()).getUltimoNome());
        morada.setEnabled(false);
        morada.setText(listaColaboradores.get(listagem.getSelectedIndex()).getMorada());
        dataNascimento.setEnabled(false);
        dataNascimento.setValue(listaColaboradores.get(listagem.getSelectedIndex()).getDataNascimento());
        email.setEnabled(false);
        email.setText(listaColaboradores.get(listagem.getSelectedIndex()).getEmail());
        contactoTelefonico.setText(listaColaboradores.get(listagem.getSelectedIndex()).getContactoTelefonico());
        contactoTelefonico.setEnabled(false);
        habilitacoes.setText(listaColaboradores.get(listagem.getSelectedIndex()).getHabilitacoes());
        habilitacoes.setEnabled(false);
        funcoes.setText(listaColaboradores.get(listagem.getSelectedIndex()).getFuncao());
        funcoes.setEnabled(false);

        gravar.setEnabled(false);
        novo.setEnabled(true);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelColaborador.revalidate();
        painelColaborador.repaint();
    }

    /**
     * Método utilizado para gravar um novo colaborador
     */
    public void gravar() {
        // Verifica se todos os campos foram preenchidos
        if ((primeiroNome.getText().isEmpty())
                || (ultimoNome.getText().isEmpty())
                || (morada.getText().isEmpty())
                || (email.getText().isEmpty())
                || (contactoTelefonico.getText().isEmpty())
                || (habilitacoes.getText().isEmpty())
                || (funcoes.getText().isEmpty())) {

            JOptionPane.showMessageDialog(this, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String primeiroNome = this.primeiroNome.getText();
            String ultimoNome = this.ultimoNome.getText();
            String morada = this.morada.getText();
            String email = this.email.getText();
            String habilitacoes = this.habilitacoes.getText();
            String funcoes = this.funcoes.getText();

            int contactoTelefonico;

            try {
                contactoTelefonico = Integer.parseInt(this.contactoTelefonico.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.contactoTelefonico.setText("");
                this.contactoTelefonico.requestFocusInWindow();
                return;
            }

            novoColaborador = new Colaborador(primeiroNome,
                    ultimoNome, morada, (Date) this.dataNascimento.getValue(),
                    "" + contactoTelefonico, email, habilitacoes, funcoes);

            int confirmacao = JOptionPane.showConfirmDialog(this,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                listaColaboradores.registarColaborador(novoColaborador);

                guardar();

                this.primeiroNome.setEnabled(false);
                this.primeiroNome.setText("");
                this.ultimoNome.setEnabled(false);
                this.ultimoNome.setText("");
                this.morada.setEnabled(false);
                this.morada.setText("");
                this.dataNascimento.setEnabled(false);
                this.email.setEnabled(false);
                this.email.setText("");
                this.contactoTelefonico.setEnabled(false);
                this.contactoTelefonico.setText("");
                this.habilitacoes.setEnabled(false);
                this.habilitacoes.setText("");
                this.funcoes.setEnabled(false);
                this.funcoes.setText("");

                modeloListagem.addElement("*" + novoColaborador.getPrimeiroNome() + " "
                        + novoColaborador.getUltimoNome());

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelColaborador.revalidate();
                painelColaborador.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar um determinado colaborador
     */
    public void editar() {
        // Verifica se todos os campos foram preenchidos
        if ((primeiroNome.getText().isEmpty())
                || (ultimoNome.getText().isEmpty())
                || (morada.getText().isEmpty())
                || (email.getText().isEmpty())
                || (contactoTelefonico.getText().isEmpty())
                || (habilitacoes.getText().isEmpty())
                || (funcoes.getText().isEmpty())) {

            JOptionPane.showMessageDialog(this, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String primeiroNome = this.primeiroNome.getText();
            String ultimoNome = this.ultimoNome.getText();
            String morada = this.morada.getText();
            String email = this.email.getText();
            String habilitacoes = this.habilitacoes.getText();
            String funcoes = this.funcoes.getText();

            int contactoTelefonico;

            try {
                contactoTelefonico = Integer.parseInt(this.contactoTelefonico.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.contactoTelefonico.setText("");
                this.contactoTelefonico.requestFocusInWindow();
                return;
            }

            int confirmacao = JOptionPane.showConfirmDialog(this,
                    "Pretende guardar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarColaborador = listaColaboradores.get(listagem.getSelectedIndex());

                editarColaborador.setPrimeiroNome(primeiroNome);
                editarColaborador.setUltimoNome(ultimoNome);
                editarColaborador.setMorada(morada);
                editarColaborador.setDataNascimento((Date) this.dataNascimento.getValue());
                editarColaborador.setContactoTelefonico("" + contactoTelefonico);
                editarColaborador.setEmail(email);
                editarColaborador.setHabilitacoes(habilitacoes);
                editarColaborador.setFuncao(funcoes);

                guardar();

                this.primeiroNome.setEnabled(false);
                this.primeiroNome.setText("");
                this.ultimoNome.setEnabled(false);
                this.ultimoNome.setText("");
                this.morada.setEnabled(false);
                this.morada.setText("");
                this.dataNascimento.setEnabled(false);
                this.email.setEnabled(false);
                this.email.setText("");
                this.contactoTelefonico.setEnabled(false);
                this.contactoTelefonico.setText("");
                this.habilitacoes.setEnabled(false);
                this.habilitacoes.setText("");
                this.funcoes.setEnabled(false);
                this.funcoes.setText("");

                listagem.setEnabled(true);
                modeloListagem.setElementAt("*" + editarColaborador.getPrimeiroNome() + " "
                        + editarColaborador.getUltimoNome(), listagem.getSelectedIndex());

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelColaborador.revalidate();
                painelColaborador.repaint();
            }
        }
    }

    /**
     * Método utilizado para apagar um determinado colaborador
     */
    public void apagar() {
        int confirmacao = JOptionPane.showConfirmDialog(this,
                "Pretende apagar o colaborador?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarColaborador = listaColaboradores.get(listagem.getSelectedIndex());
            listaColaboradores.eliminarColaborador(apagarColaborador);

            guardar();

            JOptionPane.showMessageDialog(this,
                    "Colaborador eliminado com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.primeiroNome.setEnabled(false);
            this.primeiroNome.setText("");
            this.ultimoNome.setEnabled(false);
            this.ultimoNome.setText("");
            this.morada.setEnabled(false);
            this.morada.setText("");
            this.dataNascimento.setEnabled(false);
            this.email.setEnabled(false);
            this.email.setText("");
            this.contactoTelefonico.setEnabled(false);
            this.contactoTelefonico.setText("");
            this.habilitacoes.setEnabled(false);
            this.habilitacoes.setText("");
            this.funcoes.setEnabled(false);
            this.funcoes.setText("");
            this.listagem.setEnabled(true);

            modeloListagem.remove(listagem.getSelectedIndex());
            listagem.setEnabled(true);
            listagem.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelColaborador.revalidate();
            painelColaborador.repaint();
        }
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoClicado = (JButton) e.getSource();
        String nomeBotao = botaoClicado.getName();

        if (nomeBotao.equals("novo")) {
            primeiroNome.setEnabled(true);
            primeiroNome.setText("");
            ultimoNome.setEnabled(true);
            ultimoNome.setText("");
            morada.setEnabled(true);
            morada.setText("");
            dataNascimento.setEnabled(true);
            email.setEnabled(true);
            email.setText("");
            contactoTelefonico.setEnabled(true);
            contactoTelefonico.setText("");
            habilitacoes.setEnabled(true);
            habilitacoes.setText("");
            funcoes.setEnabled(true);
            funcoes.setText("");

            listagem.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelColaborador.revalidate();
            painelColaborador.repaint();

        } else if (nomeBotao.equals("gravar")) {
            if (edicao == true) {
                editar();
            } else {
                gravar();
            }
        } else if (nomeBotao.equals("editar")) {
            primeiroNome.setEnabled(true);
            ultimoNome.setEnabled(true);
            morada.setEnabled(true);
            dataNascimento.setEnabled(true);
            contactoTelefonico.setEnabled(true);
            email.setEnabled(true);
            habilitacoes.setEnabled(true);
            funcoes.setEnabled(true);

            listagem.setEnabled(false);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = true;

            painelColaborador.revalidate();
            painelColaborador.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagar();
        }
    }
}
