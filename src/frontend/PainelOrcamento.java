package frontend;

import backend.*;
import java.util.Date;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelOrcamento permite a criação do content panel
 * específico da janela principal para o menu Orçamentos
 *
 * @author JoaoFerreira
 */
public class PainelOrcamento extends JPanel implements ActionListener {

    // Variáveis de Instância (Passo 1)
    private JPanel painelOrcamento;
    private JTextField nome, morada, codigoPostal1, codigoPostal2, localidade,
            estimativaDespesas, honorarios;
    private JComboBox modalidadeCalculoQuota;
    private JList listagem;
    private JScrollPane scrollListagem;
    private final DefaultListModel modeloListagem = new DefaultListModel();
    private JButton novo, gravar, editar, apagar, discriminar;
    private ListaOrcamentos listaOrcamentos;
    private Orcamento novoOrcamento, editarOrcamento, apagarOrcamento;
    private JDialog discriminarDespesas;
    private JPanel discriminarDespesasPanel;
    private JTextField valorDespesasAdministrativas, valorLimpezas, valorReparacoes, valorElevadores, valorOutras;
    private JButton ok;
    boolean edicao = false;
    boolean calculoManual = false;

    // Variáveis de Instância (Passo 2)
    private JTextField designacao, permilagem, andar, taxaUtilizacao, quota;
    private JList listagemFracoes;
    private JScrollPane scrollListagemFracoes;
    private final DefaultListModel modeloListagemFracoes = new DefaultListModel();
    private Condomino novoCondomino, editarCondomino, apagarCondomino;
    private JButton novaFracao, gravarFracao, editarFracao, apagarFracao;
    private Proprietario novoProprietario;
    private Condominio condominioUsar;
    private Fracao novoFracao;
    boolean edicaoFracao = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Orçamentos (Passo 1)
     *
     * @return JPanel - Content panel do menu Orçamentos (Passo 1)
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelOrcamentoPasso1() throws IOException {

        carregar();
        carregarListagemPasso1();

        painelOrcamento = new JPanel(null);
        painelOrcamento.setBackground(Color.white);

        JLabel titulo = new JLabel("Orçamentos");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(275, 20, 250, 50);
        painelOrcamento.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelOrcamento.add(separador);

        JLabel labelListagem = new JLabel("Lista Orçamentos:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelOrcamento.add(labelListagem);

        JLabel labelPasso = new JLabel("PASSO 1 - INFO CONDOMÍNIO");
        labelPasso.setFont(new Font("Verdana", Font.BOLD, 14));
        labelPasso.setBounds(237, 115, 250, 20);
        painelOrcamento.add(labelPasso);

        listagem = new JList(modeloListagem);
        listagem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagem.setLayoutOrientation(JList.VERTICAL);
        listagem.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagem.setFixedCellHeight(20);
        listagem.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagem.getSelectedIndex() != -1) {
                        carregarDadosOrcamentoPasso1();
                    }
                } else if (listagem.getSelectedIndex() != -1) {
                    carregarDadosOrcamentoPasso1();
                } else {
                    nome.setEnabled(false);
                    nome.setText("");
                    morada.setEnabled(false);
                    morada.setText("");
                    codigoPostal1.setEnabled(false);
                    codigoPostal1.setText("");
                    codigoPostal2.setEnabled(false);
                    codigoPostal2.setText("");
                    localidade.setEnabled(false);
                    localidade.setText("");
                    estimativaDespesas.setEnabled(false);
                    estimativaDespesas.setText("");
                    honorarios.setEnabled(false);
                    honorarios.setText("");
                    modalidadeCalculoQuota.setEnabled(false);
                    novo.setEnabled(true);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                    apagar.setEnabled(false);
                }
            }
        });

        scrollListagem = new JScrollPane(listagem);
        scrollListagem.setVerticalScrollBar(new JScrollBar());
        scrollListagem.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagem.setBounds(20, 150, 200, 300);

        painelOrcamento.add(scrollListagem);

        JLabel labelNome = new JLabel("Nome do Condomínio");
        labelNome.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelNome.setBounds(240, 150, 155, 20);
        painelOrcamento.add(labelNome);

        nome = new JTextField();
        nome.setFont(new Font("Verdana", Font.PLAIN, 14));
        nome.setBounds(410, 150, 350, 20);
        nome.setEnabled(false);
        painelOrcamento.add(nome);

        JLabel labelMorada = new JLabel("Morada");
        labelMorada.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelMorada.setBounds(240, 185, 60, 20);
        painelOrcamento.add(labelMorada);

        morada = new JTextField();
        morada.setFont(new Font("Verdana", Font.PLAIN, 14));
        morada.setBounds(315, 185, 445, 20);
        morada.setEnabled(false);
        painelOrcamento.add(morada);

        JLabel labelCodigoPostal = new JLabel("Código Postal");
        labelCodigoPostal.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelCodigoPostal.setBounds(240, 220, 100, 20);
        painelOrcamento.add(labelCodigoPostal);

        codigoPostal1 = new JTextField();
        codigoPostal1.setFont(new Font("Verdana", Font.PLAIN, 14));
        codigoPostal1.setBounds(355, 220, 60, 20);
        codigoPostal1.setEnabled(false);
        painelOrcamento.add(codigoPostal1);

        codigoPostal2 = new JTextField();
        codigoPostal2.setFont(new Font("Verdana", Font.PLAIN, 14));
        codigoPostal2.setBounds(430, 220, 50, 20);
        codigoPostal2.setEnabled(false);
        painelOrcamento.add(codigoPostal2);

        JLabel labelLocalidade = new JLabel("Localidade");
        labelLocalidade.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelLocalidade.setBounds(495, 220, 75, 20);
        painelOrcamento.add(labelLocalidade);

        localidade = new JTextField();
        localidade.setFont(new Font("Verdana", Font.PLAIN, 14));
        localidade.setBounds(585, 220, 175, 20);
        localidade.setEnabled(false);
        painelOrcamento.add(localidade);

        JLabel labelEstimativaDespesas = new JLabel("Estimativa Despesas");
        labelEstimativaDespesas.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelEstimativaDespesas.setBounds(240, 255, 150, 20);
        painelOrcamento.add(labelEstimativaDespesas);

        estimativaDespesas = new JTextField();
        estimativaDespesas.setFont(new Font("Verdana", Font.PLAIN, 14));
        estimativaDespesas.setBounds(395, 255, 100, 20);
        estimativaDespesas.setEnabled(false);
        painelOrcamento.add(estimativaDespesas);

        discriminar = new JButton("Inserir");
        discriminar.setName("discriminar");
        discriminar.setFont(new Font("Verdana", Font.ITALIC, 14));
        discriminar.setBounds(510, 255, 85, 20);
        discriminar.setEnabled(false);
        painelOrcamento.add(discriminar);
        discriminar.addActionListener(this);

        desenharDiscriminarDespesasJDialog();

        JLabel labelHonorarios = new JLabel("Honorários");
        labelHonorarios.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelHonorarios.setBounds(240, 290, 80, 20);
        painelOrcamento.add(labelHonorarios);

        honorarios = new JTextField();
        honorarios.setFont(new Font("Verdana", Font.PLAIN, 14));
        honorarios.setBounds(335, 290, 100, 20);
        honorarios.setEnabled(false);
        painelOrcamento.add(honorarios);

        JLabel labelModalidadeCalculoQuota = new JLabel("Modalidade de Cálculo da Quota");
        labelModalidadeCalculoQuota.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelModalidadeCalculoQuota.setBounds(240, 325, 250, 20);
        painelOrcamento.add(labelModalidadeCalculoQuota);

        String[] lista = {"Permilagem", "Equitativa", "Exacta", "Manual"};
        modalidadeCalculoQuota = new JComboBox(lista);
        modalidadeCalculoQuota.setFont(new Font("Verdana", Font.ITALIC, 14));
        modalidadeCalculoQuota.setBounds(240, 360, 230, 20);
        modalidadeCalculoQuota.setEditable(false);
        modalidadeCalculoQuota.setEnabled(false);
        painelOrcamento.add(modalidadeCalculoQuota);

        novo = new JButton("Novo");
        novo.setName("novo");
        novo.setFont(new Font("Verdana", Font.ITALIC, 14));
        novo.setBounds(20, 500, 85, 20);
        painelOrcamento.add(novo);
        novo.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(120, 500, 85, 20);
        editar.setEnabled(false);
        painelOrcamento.add(editar);
        editar.addActionListener(this);

        apagar = new JButton("Apagar");
        apagar.setName("apagar");
        apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagar.setBounds(220, 500, 85, 20);
        apagar.setEnabled(false);
        painelOrcamento.add(apagar);
        apagar.addActionListener(this);

        gravar = new JButton("Seguinte");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelOrcamento.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelOrcamento.add(fundo);

        painelOrcamento.revalidate();
        painelOrcamento.repaint();
        return painelOrcamento;
    }

    /**
     * Método utilizado para desenhar a JDialog específica para inserir as
     * várias despesas do condomínio
     *
     */
    public void desenharDiscriminarDespesasJDialog() {
        discriminarDespesas = new JDialog();
        discriminarDespesasPanel = new JPanel(null);
        discriminarDespesasPanel.setBackground(Color.white);
        discriminarDespesas.setContentPane(discriminarDespesasPanel);

        discriminarDespesas.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        discriminarDespesas.setPreferredSize(new Dimension(400, 300));

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        discriminarDespesas.setLocation(dim.width / 2 - 400 / 2, dim.height / 2 - 300 / 2);
        discriminarDespesas.setResizable(false);

        JLabel tituloJDialog = new JLabel("Inserir Despesas");
        tituloJDialog.setBounds(120, 20, 200, 40);
        tituloJDialog.setFont(new Font("Verdana", Font.BOLD, 18));
        discriminarDespesasPanel.add(tituloJDialog);

        JLabel despesasAdministrativas = new JLabel("Despesas Administrativas");
        despesasAdministrativas.setBounds(20, 70, 250, 20);
        despesasAdministrativas.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(despesasAdministrativas);

        valorDespesasAdministrativas = new JTextField();
        valorDespesasAdministrativas.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorDespesasAdministrativas.setBounds(280, 70, 70, 20);
        valorDespesasAdministrativas.setEnabled(true);
        discriminarDespesasPanel.add(valorDespesasAdministrativas);

        JLabel limpezas = new JLabel("Serviços Limpeza");
        limpezas.setBounds(20, 100, 250, 20);
        limpezas.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(limpezas);

        valorLimpezas = new JTextField();
        valorLimpezas.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorLimpezas.setBounds(280, 100, 70, 20);
        valorLimpezas.setEnabled(true);
        discriminarDespesasPanel.add(valorLimpezas);

        JLabel reparacoes = new JLabel("Pequenas Reparações");
        reparacoes.setBounds(20, 130, 250, 20);
        reparacoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(reparacoes);

        valorReparacoes = new JTextField();
        valorReparacoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorReparacoes.setBounds(280, 130, 70, 20);
        valorReparacoes.setEnabled(true);
        discriminarDespesasPanel.add(valorReparacoes);

        JLabel elevadores = new JLabel("Manutenção Elevadores");
        elevadores.setBounds(20, 160, 250, 20);
        elevadores.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(elevadores);

        valorElevadores = new JTextField();
        valorElevadores.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorElevadores.setBounds(280, 160, 70, 20);
        valorElevadores.setEnabled(true);
        discriminarDespesasPanel.add(valorElevadores);

        JLabel outras = new JLabel("Outras Despesas");
        outras.setBounds(20, 190, 250, 20);
        outras.setFont(new Font("Verdana", Font.PLAIN, 14));
        discriminarDespesasPanel.add(outras);

        valorOutras = new JTextField();
        valorOutras.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorOutras.setBounds(280, 190, 70, 20);
        valorOutras.setEnabled(true);
        discriminarDespesasPanel.add(valorOutras);

        ok = new JButton("OK");
        ok.setName("ok");
        ok.setFont(new Font("Verdana", Font.ITALIC, 14));
        ok.setBounds(158, 220, 85, 20);
        ok.setEnabled(true);
        discriminarDespesasPanel.add(ok);
        ok.addActionListener(this);

        discriminarDespesasPanel.revalidate();
        discriminarDespesasPanel.repaint();

        discriminarDespesas.pack();
        discriminarDespesas.setVisible(false);
    }

    /**
     * Método que carrega a lista de orçamentos de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaOrcamentos infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_orcamentos.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaOrcamentos) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaOrcamentos = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelOrcamento,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaOrcamentos = new ListaOrcamentos();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelOrcamento,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaOrcamentos = new ListaOrcamentos();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de orçamentos num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_orcamentos.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaOrcamentos);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(this, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelOrcamento,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o número dos vários orçamentos guardados para uma
     * Jlist
     */
    public void carregarListagemPasso1() {
        for (int i = 0; i < listaOrcamentos.size(); i++) {
            if (listaOrcamentos.get(i) != null) {

                modeloListagem.addElement("*" + listaOrcamentos.get(i).getNumeroOrcamento());
            }
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados do orçamento
     * selecionado
     */
    public void carregarDadosOrcamentoPasso1() {
        nome.setEnabled(false);
        nome.setText(listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getNome());
        morada.setEnabled(false);
        morada.setText(listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getMorada());
        codigoPostal1.setEnabled(false);
        codigoPostal1.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getCodigoPostal1());
        codigoPostal2.setEnabled(false);
        codigoPostal2.setText(listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getCodigoPostal2());
        localidade.setEnabled(false);
        localidade.setText(listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getLocalidade());
        estimativaDespesas.setEnabled(false);
        estimativaDespesas.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getEstimativaDespesas());
        honorarios.setEnabled(false);
        honorarios.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getHonorarios());
        modalidadeCalculoQuota.setEnabled(false);
        modalidadeCalculoQuota.setSelectedIndex(listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getModalidadeCalculoQuota());

        gravar.setEnabled(false);
        novo.setEnabled(true);
        discriminar.setEnabled(false);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelOrcamento.revalidate();
        painelOrcamento.repaint();
    }

    /**
     * Método utilizado para gravar o primeiro passo da criação do orçamento
     */
    public void gravarPasso1() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty()) || (this.morada.getText().isEmpty())
                || (this.codigoPostal1.getText().isEmpty())
                || (this.codigoPostal2.getText().isEmpty())
                || (this.localidade.getText().isEmpty())
                || (this.estimativaDespesas.getText().isEmpty())
                || (this.honorarios.getText().isEmpty())) {

            JOptionPane.showMessageDialog(this, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String nome = this.nome.getText();
            String morada = this.morada.getText();

            int codigoPostal1, codigoPostal2;
            Double estimativaDespesas, honorarios;

            try {
                codigoPostal1 = Integer.parseInt(this.codigoPostal1.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.codigoPostal1.setText("");
                this.codigoPostal1.requestFocusInWindow();
                return;
            }

            try {
                codigoPostal2 = Integer.parseInt(this.codigoPostal2.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.codigoPostal2.setText("");
                this.codigoPostal2.requestFocusInWindow();
                return;
            }

            String localidade = this.localidade.getText();

            try {
                estimativaDespesas = Double.parseDouble(this.estimativaDespesas.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.estimativaDespesas.setText("");
                this.estimativaDespesas.requestFocusInWindow();
                return;
            }

            try {
                honorarios = Double.parseDouble(this.honorarios.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.honorarios.setText("");
                this.honorarios.requestFocusInWindow();
                return;
            }

            int modalidadeCalculoQuota = this.modalidadeCalculoQuota.getSelectedIndex();

            Condominio condominioOrcamentado = new Condominio(nome, morada, codigoPostal1,
                    this.codigoPostal2.getText(), localidade, Double.parseDouble(this.estimativaDespesas.getText()),
                    Double.parseDouble(this.valorDespesasAdministrativas.getText()),
                    Double.parseDouble(this.valorLimpezas.getText()),
                    Double.parseDouble(this.valorReparacoes.getText()),
                    Double.parseDouble(this.valorElevadores.getText()),
                    Double.parseDouble(this.valorOutras.getText()),
                    honorarios, modalidadeCalculoQuota);

            novoOrcamento = new Orcamento(condominioOrcamentado);

            int confirmacao = JOptionPane.showConfirmDialog(this,
                    "Pretende avançar?", "Avançar", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                painelOrcamento.removeAll();
                painelOrcamento.revalidate();
                painelOrcamento.repaint();
                desenharPainelOrcamentoPasso2(novoOrcamento.getCondominioOrcamentado());

            }
        }
    }

    /**
     * Método utilizado para editar o primeiro passo de um orçamento já criado
     */
    public void editarPasso1() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.nome.getText().isEmpty()) || (this.morada.getText().isEmpty())
                || (this.codigoPostal1.getText().isEmpty())
                || (this.codigoPostal2.getText().isEmpty())
                || (this.localidade.getText().isEmpty())
                || (this.estimativaDespesas.getText().isEmpty())
                || (this.honorarios.getText().isEmpty())) {

            JOptionPane.showMessageDialog(painelOrcamento, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(this,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarOrcamento = listaOrcamentos.get(listagem.getSelectedIndex());

                Condominio condominioOrcamentado = editarOrcamento.getCondominioOrcamentado();

                condominioOrcamentado.setNome(this.nome.getText());
                condominioOrcamentado.setMorada(this.morada.getText());

                int codigoPostal1, codigoPostal2;
                Double estimativaDespesas, honorarios;

                try {
                    condominioOrcamentado.setCodigoPostal1(Integer.parseInt(this.codigoPostal1.getText()));
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.codigoPostal1.setText("");
                    this.codigoPostal1.requestFocusInWindow();
                    return;
                }

                try {
                    codigoPostal2 = Integer.parseInt(this.codigoPostal2.getText());
                    condominioOrcamentado.setCodigoPostal2(this.codigoPostal2.getText());
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.codigoPostal2.setText("");
                    this.codigoPostal2.requestFocusInWindow();
                    return;
                }

                condominioOrcamentado.setLocalidade(this.localidade.getText());

                try {
                    condominioOrcamentado.setEstimativaDespesas(Double.parseDouble(this.estimativaDespesas.getText()));
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.estimativaDespesas.setText("");
                    this.estimativaDespesas.requestFocusInWindow();
                    return;
                }

                try {
                    condominioOrcamentado.setHonorarios(Double.parseDouble(this.honorarios.getText()));
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(this, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.honorarios.setText("");
                    this.honorarios.requestFocusInWindow();
                    return;
                }

                condominioOrcamentado.setModalidadeCalculoQuota(this.modalidadeCalculoQuota.getSelectedIndex());
                condominioOrcamentado.setModalidadeCalculoQuota(this.modalidadeCalculoQuota.getSelectedIndex());
                condominioOrcamentado.setValorDespesasAdministrativas(Double.parseDouble(this.valorDespesasAdministrativas.getText()));
                condominioOrcamentado.setValorLimpezas(Double.parseDouble(this.valorLimpezas.getText()));
                condominioOrcamentado.setValorReparacoes(Double.parseDouble(this.valorReparacoes.getText()));
                condominioOrcamentado.setValorElevadores(Double.parseDouble(this.valorElevadores.getText()));
                condominioOrcamentado.setValorOutras(Double.parseDouble(this.valorOutras.getText()));

                painelOrcamento.removeAll();
                painelOrcamento.revalidate();
                painelOrcamento.repaint();
                guardar();
                desenharPainelOrcamentoPasso2(editarOrcamento.getCondominioOrcamentado());
            }
        }
    }

    /**
     * Método utilizado para apagar um orçamento
     */
    public void apagarPasso1() {
        int confirmacao = JOptionPane.showConfirmDialog(this,
                "Pretende apagar o orçamento?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarOrcamento = listaOrcamentos.get(listagem.getSelectedIndex());
            listaOrcamentos.eliminarOrcamento(apagarOrcamento);

            guardar();

            JOptionPane.showMessageDialog(this,
                    "Orçamento eliminado com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.nome.setEnabled(false);
            this.nome.setText("");
            this.morada.setEnabled(false);
            this.morada.setText("");
            this.codigoPostal1.setEnabled(false);
            this.codigoPostal1.setText("");
            this.codigoPostal2.setEnabled(false);
            this.codigoPostal2.setText("");
            this.localidade.setEnabled(false);
            this.localidade.setText("");
            this.estimativaDespesas.setEnabled(false);
            this.estimativaDespesas.setText("");
            this.honorarios.setEnabled(false);
            this.honorarios.setText("");
            this.modalidadeCalculoQuota.setEnabled(false);

            modeloListagem.remove(listagem.getSelectedIndex());
            listagem.setEnabled(true);
            listagem.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            discriminar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelOrcamento.revalidate();
            painelOrcamento.repaint();
        }
    }

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Orçamentos (Passo 2)
     *
     * @param condominioUsar - Condominio orçamentado
     */
    public void desenharPainelOrcamentoPasso2(Condominio condominioUsar) {

        this.condominioUsar = condominioUsar;
        carregarListagemPasso2();

        JLabel titulo = new JLabel("Orçamentos");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(275, 20, 250, 50);
        painelOrcamento.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelOrcamento.add(separador);

        JLabel labelListagem = new JLabel("Lista Frações:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelOrcamento.add(labelListagem);

        JLabel labelPasso = new JLabel("PASSO 2 - INFO FRAÇÕES");
        labelPasso.setFont(new Font("Verdana", Font.BOLD, 14));
        labelPasso.setBounds(237, 115, 250, 20);
        painelOrcamento.add(labelPasso);

        listagemFracoes = new JList(modeloListagemFracoes);
        listagemFracoes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemFracoes.setLayoutOrientation(JList.VERTICAL);
        listagemFracoes.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemFracoes.setFixedCellHeight(20);
        listagemFracoes.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagemFracoes.getSelectedIndex() != -1) {
                        carregarDadosFracao();
                    }
                } else if (listagemFracoes.getSelectedIndex() != -1) {
                    carregarDadosFracao();
                } else {
                    designacao.setEnabled(false);
                    designacao.setText("");
                    permilagem.setEnabled(false);
                    permilagem.setText("");
                    andar.setEnabled(false);
                    andar.setText("");
                    taxaUtilizacao.setEnabled(false);
                    taxaUtilizacao.setText("");
                    quota.setEnabled(false);
                    quota.setText("");
                    novaFracao.setEnabled(true);
                    editarFracao.setEnabled(false);
                    gravarFracao.setEnabled(false);
                    apagarFracao.setEnabled(false);
                }
            }
        });

        scrollListagemFracoes = new JScrollPane(listagemFracoes);
        scrollListagemFracoes.setVerticalScrollBar(new JScrollBar());
        scrollListagemFracoes.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemFracoes.setBounds(20, 150, 200, 300);

        painelOrcamento.add(scrollListagemFracoes);

        JLabel labelDesignacao = new JLabel("Fração");
        labelDesignacao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDesignacao.setBounds(240, 150, 60, 20);
        painelOrcamento.add(labelDesignacao);

        designacao = new JTextField();
        designacao.setFont(new Font("Verdana", Font.PLAIN, 14));
        designacao.setBounds(315, 150, 155, 20);
        designacao.setEnabled(false);
        painelOrcamento.add(designacao);

        JLabel labelAndar = new JLabel("Andar");
        labelAndar.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelAndar.setBounds(240, 185, 50, 20);
        painelOrcamento.add(labelAndar);

        andar = new JTextField();
        andar.setFont(new Font("Verdana", Font.PLAIN, 14));
        andar.setBounds(305, 185, 50, 20);
        andar.setEnabled(false);
        painelOrcamento.add(andar);

        JLabel labelPermilagem = new JLabel("Permilagem");
        labelPermilagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelPermilagem.setBounds(370, 185, 85, 20);
        painelOrcamento.add(labelPermilagem);

        permilagem = new JTextField();
        permilagem.setFont(new Font("Verdana", Font.PLAIN, 14));
        permilagem.setBounds(470, 185, 65, 20);
        permilagem.setEnabled(false);
        painelOrcamento.add(permilagem);

        JLabel labelTaxaUtilizacao = new JLabel("Taxa de Utilização");
        labelTaxaUtilizacao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelTaxaUtilizacao.setBounds(240, 220, 140, 20);
        painelOrcamento.add(labelTaxaUtilizacao);

        taxaUtilizacao = new JTextField();
        taxaUtilizacao.setFont(new Font("Verdana", Font.PLAIN, 14));
        taxaUtilizacao.setBounds(395, 220, 65, 20);
        taxaUtilizacao.setEnabled(false);
        painelOrcamento.add(taxaUtilizacao);

        JLabel labelQuota = new JLabel("Quota");
        labelQuota.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelQuota.setBounds(475, 220, 50, 20);
        painelOrcamento.add(labelQuota);

        quota = new JTextField();
        quota.setFont(new Font("Verdana", Font.PLAIN, 14));
        quota.setBounds(540, 220, 65, 20);
        quota.setEnabled(false);
        painelOrcamento.add(quota);

        novaFracao = new JButton("Nova");
        novaFracao.setName("novaFracao");
        novaFracao.setFont(new Font("Verdana", Font.ITALIC, 14));
        novaFracao.setBounds(20, 515, 85, 20);
        painelOrcamento.add(novaFracao);
        novaFracao.addActionListener(this);

        editarFracao = new JButton("Editar");
        editarFracao.setName("editarFracao");
        editarFracao.setFont(new Font("Verdana", Font.ITALIC, 14));
        editarFracao.setBounds(120, 515, 85, 20);
        editarFracao.setEnabled(false);
        painelOrcamento.add(editarFracao);
        editarFracao.addActionListener(this);

        apagarFracao = new JButton("Apagar");
        apagarFracao.setName("apagarFracao");
        apagarFracao.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagarFracao.setBounds(220, 515, 85, 20);
        apagarFracao.setEnabled(false);
        painelOrcamento.add(apagarFracao);
        apagarFracao.addActionListener(this);

        gravarFracao = new JButton("Gravar");
        gravarFracao.setName("gravarFracao");
        gravarFracao.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravarFracao.setBounds(320, 515, 85, 20);
        gravarFracao.setEnabled(false);
        painelOrcamento.add(gravarFracao);
        gravarFracao.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelOrcamento.add(fundo);

        painelOrcamento.revalidate();
        painelOrcamento.repaint();
    }

    /**
     * Método que carrega a designação das fraçoes associado a um determinado
     * orçamento para uma Jlist
     */
    public void carregarListagemPasso2() {
        modeloListagemFracoes.clear();
        for (int i = 0; i < condominioUsar.getListaCondominos().size(); i++) {
            modeloListagemFracoes.addElement("*" + condominioUsar.getListaCondominos().get(i).getFracao().getDesignacao());
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados da fração
     * selecionada
     */
    public void carregarDadosFracao() {
        designacao.setEnabled(false);
        designacao.setText(condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex()).
                getFracao().getDesignacao());
        permilagem.setEnabled(false);
        permilagem.setText("" + condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex()).
                getFracao().getPermilagem());
        andar.setEnabled(false);
        andar.setText("" + condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex()).
                getFracao().getAndar());
        taxaUtilizacao.setEnabled(false);
        taxaUtilizacao.setText("" + condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex()).
                getFracao().getTaxaUtilizacao());
        quota.setEnabled(false);
        quota.setText("" + condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex()).
                getFracao().getQuota());

        gravarFracao.setEnabled(false);
        novaFracao.setEnabled(true);
        editarFracao.setEnabled(true);
        apagarFracao.setEnabled(true);

        painelOrcamento.revalidate();
        painelOrcamento.repaint();
    }

    /**
     * Método utilizado para gravar uma fração associada a um determinado
     * orçamento
     */
    public void gravarPasso2() {
        // Verifica se todos os campos foram preenchidos 
        if (((this.designacao.getText().isEmpty())
                || (this.andar.getText().isEmpty())
                || (this.permilagem.getText().isEmpty())
                || (this.taxaUtilizacao.getText().isEmpty())
                || (this.quota.getText().isEmpty()
                && condominioUsar.getModalidadeCalculoQuota() == 3))) {

            JOptionPane.showMessageDialog(painelOrcamento, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String nome = "indefinido";
            String morada = "indefinido";
            String email = "indefinido";
            String contactoTelefonico = "indefinido";
            String nif = "indefinido";
            String profissao = "indefinido";
            Date dataNascimento = null;

            novoProprietario = new Proprietario(nome, morada, email,
                    contactoTelefonico + "", nif + "", dataNascimento, profissao);

            String designacao = this.designacao.getText();

            int andar;
            Double permilagem, taxaUtilizacao;
            Double quota = 0.0;

            try {
                permilagem = Double.parseDouble(this.permilagem.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.permilagem.setText("");
                this.permilagem.requestFocusInWindow();
                return;
            }

            try {
                andar = Integer.parseInt(this.andar.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.andar.setText("");
                this.andar.requestFocusInWindow();
                return;
            }

            try {
                taxaUtilizacao = Double.parseDouble(this.taxaUtilizacao.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.taxaUtilizacao.setText("");
                this.taxaUtilizacao.requestFocusInWindow();
                return;
            }

            if (condominioUsar.getModalidadeCalculoQuota() == 3) {
                try {
                    quota = Double.parseDouble(this.quota.getText());
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.quota.setText("");
                    this.quota.requestFocusInWindow();
                    return;
                }
            }

            novoFracao = new Fracao(designacao, permilagem, andar, taxaUtilizacao);
            novoCondomino = new Condomino(novoProprietario, novoFracao);

            int confirmacao = JOptionPane.showConfirmDialog(painelOrcamento,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                condominioUsar.registarCondomino(novoCondomino);
                if (condominioUsar.getModalidadeCalculoQuota() == 3) {
                    novoCondomino.getFracao().setQuota(quota);
                }

                this.designacao.setEnabled(false);
                this.designacao.setText("");
                this.permilagem.setEnabled(false);
                this.permilagem.setText("");
                this.andar.setEnabled(false);
                this.andar.setText("");
                this.taxaUtilizacao.setEnabled(false);
                this.taxaUtilizacao.setText("");
                this.quota.setEnabled(false);
                this.quota.setText("");

                modeloListagemFracoes.addElement("*" + novoCondomino.getFracao().getDesignacao());

                gravarFracao.setEnabled(false);
                editarFracao.setEnabled(false);
                apagarFracao.setEnabled(false);
                novaFracao.setEnabled(true);

                if (edicao != true) {
                    listaOrcamentos.registarOrcamento(novoOrcamento);
                    guardar();
                } else {
                    guardar();
                }

                edicao = true;

                painelOrcamento.revalidate();
                painelOrcamento.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar uma fração associada a um determinado
     * orçamento
     */
    public void editarPasso2() {
        // Verifica se todos os campos foram preenchidos 
        if (((this.designacao.getText().isEmpty())
                || (this.andar.getText().isEmpty())
                || (this.permilagem.getText().isEmpty())
                || (this.taxaUtilizacao.getText().isEmpty())
                || (this.quota.getText().isEmpty()
                && condominioUsar.getModalidadeCalculoQuota() == 3))) {

            JOptionPane.showMessageDialog(painelOrcamento, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(painelOrcamento,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarCondomino = condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex());

                editarCondomino.getFracao().setDesignacao(this.designacao.getText());

                int andar;
                Double permilagem, taxaUtilizacao, quota;

                try {
                    permilagem = Double.parseDouble(this.permilagem.getText());
                    editarCondomino.getFracao().setPermilagem(permilagem);
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.permilagem.setText("");
                    this.permilagem.requestFocusInWindow();
                    return;
                }

                try {
                    andar = Integer.parseInt(this.andar.getText());
                    editarCondomino.getFracao().setAndar(andar);
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.andar.setText("");
                    this.andar.requestFocusInWindow();
                    return;
                }

                try {
                    taxaUtilizacao = Double.parseDouble(this.taxaUtilizacao.getText());
                    editarCondomino.getFracao().setTaxaUtilizacao(taxaUtilizacao);
                } catch (Exception z) {
                    JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                            "Erro", JOptionPane.ERROR_MESSAGE);
                    this.taxaUtilizacao.setText("");
                    this.taxaUtilizacao.requestFocusInWindow();
                    return;
                }

                if (condominioUsar.getModalidadeCalculoQuota() == 3) {
                    try {
                        quota = Double.parseDouble(this.quota.getText());
                        condominioUsar.getListaCondominos().
                                get(listagemFracoes.getSelectedIndex()).getFracao().setQuota(quota);
                    } catch (Exception z) {
                        JOptionPane.showMessageDialog(painelOrcamento, "Este campo apenas aceita números",
                                "Erro", JOptionPane.ERROR_MESSAGE);
                        this.quota.setText("");
                        this.quota.requestFocusInWindow();
                        return;
                    }
                }
            }

            if (condominioUsar.getModalidadeCalculoQuota() != 3) {
                condominioUsar.calculoValorFracao();
            }

            if (edicao != true) {
                listaOrcamentos.registarOrcamento(novoOrcamento);
                guardar();
            } else {
                guardar();
            }

            this.designacao.setEnabled(false);
            this.designacao.setText("");
            this.permilagem.setEnabled(false);
            this.permilagem.setText("");
            this.andar.setEnabled(false);
            this.andar.setText("");
            this.taxaUtilizacao.setEnabled(false);
            this.taxaUtilizacao.setText("");
            this.quota.setEnabled(false);
            this.quota.setText("");

            listagemFracoes.setEnabled(true);
            modeloListagemFracoes.setElementAt("*" + editarCondomino.getFracao().getDesignacao(), listagemFracoes.getSelectedIndex());

            gravarFracao.setEnabled(false);
            editarFracao.setEnabled(false);
            apagarFracao.setEnabled(false);
            novaFracao.setEnabled(true);

            painelOrcamento.revalidate();
            painelOrcamento.repaint();
        }
    }

    /**
     * Método utilizado para apagar uma fração associada a um determinado
     * orçamento
     */
    public void apagarPasso2() {
        int confirmacao = JOptionPane.showConfirmDialog(painelOrcamento,
                "Pretende apagar o condómino?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarCondomino = condominioUsar.getListaCondominos().get(listagemFracoes.getSelectedIndex());
            condominioUsar.eliminarCondomino(apagarCondomino);

            guardar();

            JOptionPane.showMessageDialog(painelOrcamento,
                    "Fração eliminada com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.designacao.setEnabled(false);
            this.designacao.setText("");
            this.permilagem.setEnabled(false);
            this.permilagem.setText("");
            this.andar.setEnabled(false);
            this.andar.setText("");
            this.taxaUtilizacao.setEnabled(false);
            this.taxaUtilizacao.setText("");
            this.quota.setEnabled(false);
            this.quota.setText("");

            modeloListagemFracoes.clear();
            listagemFracoes.setEnabled(false);
            listagemFracoes.clearSelection();

            gravarFracao.setEnabled(false);
            editarFracao.setEnabled(false);
            apagarFracao.setEnabled(false);
            novaFracao.setEnabled(true);

            painelOrcamento.revalidate();
            painelOrcamento.repaint();
        }

    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoclicado = (JButton) e.getSource();
        String nomeBotao = botaoclicado.getName();

        if (nomeBotao.equals("novo")) {
            nome.setEnabled(true);
            nome.setText("");
            morada.setEnabled(true);
            morada.setText("");
            codigoPostal1.setEnabled(true);
            codigoPostal1.setText("");
            codigoPostal2.setEnabled(true);
            codigoPostal2.setText("");
            localidade.setEnabled(true);
            localidade.setText("");
            estimativaDespesas.setEnabled(false);
            estimativaDespesas.setText("");
            honorarios.setEnabled(true);
            honorarios.setText("");
            modalidadeCalculoQuota.setEnabled(true);

            listagem.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(false);
            discriminar.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelOrcamento.revalidate();
            painelOrcamento.repaint();

        } else if (nomeBotao.equals("gravar")) {
            if (edicao == true) {
                editarPasso1();
            } else {
                gravarPasso1();
            }
        } else if (nomeBotao.equals("editar")) {
            nome.setEnabled(true);
            morada.setEnabled(true);
            codigoPostal1.setEnabled(true);
            codigoPostal2.setEnabled(true);
            localidade.setEnabled(true);
            estimativaDespesas.setEnabled(false);
            honorarios.setEnabled(true);
            modalidadeCalculoQuota.setEnabled(true);

            listagem.setEnabled(false);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            editar.setEnabled(false);
            discriminar.setEnabled(true);
            apagar.setEnabled(false);

            edicao = true;

            if ((listagem.getSelectedIndex() != -1) && (edicao == true)) {
                valorDespesasAdministrativas.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getValorDespesasAdministrativas());
                valorLimpezas.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getValorLimpezas());
                valorReparacoes.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getValorReparacoes());
                valorElevadores.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getValorElevadores());
                valorOutras.setText("" + listaOrcamentos.get(listagem.getSelectedIndex()).getCondominioOrcamentado().getValorOutras());
            } else {
                valorDespesasAdministrativas.setText("");
                valorLimpezas.setText("");
                valorReparacoes.setText("");
                valorElevadores.setText("");
                valorOutras.setText("");
            }

            painelOrcamento.revalidate();
            painelOrcamento.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagarPasso1();
        } else if (nomeBotao.equals("novaFracao")) {
            this.designacao.setEnabled(true);
            this.designacao.setText("");
            this.permilagem.setEnabled(true);
            this.permilagem.setText("");
            this.andar.setEnabled(true);
            this.andar.setText("");
            this.taxaUtilizacao.setEnabled(true);
            this.taxaUtilizacao.setText("");
            if (condominioUsar.getModalidadeCalculoQuota() == 3) {
                this.quota.setEnabled(true);
            } else {
                this.quota.setEnabled(false);
            }
            this.quota.setText("");

            listagemFracoes.setEnabled(true);

            gravarFracao.setEnabled(true);
            novaFracao.setEnabled(false);
            editarFracao.setEnabled(false);
            apagarFracao.setEnabled(false);

            edicaoFracao = false;

            painelOrcamento.revalidate();
            painelOrcamento.repaint();

        } else if (nomeBotao.equals("gravarFracao")) {
            if (edicaoFracao == true) {
                editarPasso2();
            } else {
                gravarPasso2();
            }
        } else if (nomeBotao.equals("editarFracao")) {
            this.designacao.setEnabled(true);
            this.permilagem.setEnabled(true);
            this.andar.setEnabled(true);
            this.taxaUtilizacao.setEnabled(true);
            if (condominioUsar.getModalidadeCalculoQuota() == 3) {
                this.quota.setEnabled(true);
            } else {
                this.quota.setEnabled(false);
            }

            listagemFracoes.setEnabled(true);

            gravarFracao.setEnabled(true);
            novaFracao.setEnabled(true);
            editarFracao.setEnabled(false);
            apagarFracao.setEnabled(false);

            edicaoFracao = true;

            painelOrcamento.revalidate();
            painelOrcamento.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagarPasso2();
        } else if (nomeBotao.equals("discriminar")) {
            discriminarDespesas.setVisible(true);
        } else if (nomeBotao.equals("ok")) {
            Double soma, valorDespesasAdministrativas, valorLimpezas, valorReparacoes, valorElevadores, valorOutras;

            try {
                valorDespesasAdministrativas = Double.parseDouble(this.valorDespesasAdministrativas.getText());
                valorLimpezas = Double.parseDouble(this.valorLimpezas.getText());
                valorReparacoes = Double.parseDouble(this.valorReparacoes.getText());
                valorElevadores = Double.parseDouble(this.valorElevadores.getText());
                valorOutras = Double.parseDouble(this.valorOutras.getText());
                soma = valorDespesasAdministrativas + valorLimpezas
                        + valorReparacoes + valorElevadores + valorOutras;
                estimativaDespesas.setText("" + soma);
                discriminarDespesas.dispose();
            } catch (Exception z) {
                JOptionPane.showMessageDialog(this, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
            }

        }
    }
}
