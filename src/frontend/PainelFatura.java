package frontend;

import backend.*;
import java.util.GregorianCalendar;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelFatura permite a criação do content panel
 * específico da janela principal para o menu Faturas
 *
 * @author JoaoFerreira
 */
public class PainelFatura implements ActionListener {

    // Variáveis de Instância
    private JPanel painelFatura;
    private JTextField limitePagamento, diasAtraso, taxaAgravamento,
            valorMensalidade, mensalidade;
    private JList listagemCondominios, listagemCondominos;
    private JScrollPane scrollListagemCondominios, scrollListagemCondominos;
    private final DefaultListModel modeloListagemCondominios = new DefaultListModel();
    private final DefaultListModel modeloListagemCondominos = new DefaultListModel();
    private JButton registar, emitir, editar, gravar, ok;
    private ListaCondominios listaCondominios;
    private Condominio editarCondominio;
    private Condomino editarCondomino;

    /**
     * Método utilizado para desenhar o content panel específico do menu Faturas
     *
     * @return JPanel - Content panel do menu Faturas
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelFatura() throws IOException {

        carregar();
        carregarListagem();

        painelFatura = new JPanel(null);
        painelFatura.setBackground(Color.white);

        JLabel titulo = new JLabel("Faturas");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(320, 20, 200, 50);
        painelFatura.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelFatura.add(separador);

        JLabel labelListagem = new JLabel("Lista Condomínios:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelFatura.add(labelListagem);

        JLabel labelListagemCondominos = new JLabel("Lista Condomínos:");
        labelListagemCondominos.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagemCondominos.setBounds(180, 115, 150, 20);
        painelFatura.add(labelListagemCondominos);

        listagemCondominios = new JList(modeloListagemCondominios);
        listagemCondominios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominios.setLayoutOrientation(JList.VERTICAL);
        listagemCondominios.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominios.setFixedCellHeight(20);
        listagemCondominios.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {

                } else if (listagemCondominios.getSelectedIndex() != -1) {
                    listagemCondominos.setEnabled(true);
                    carregarListagemCondominos();
                    registar.setEnabled(false);
                    limitePagamento.setEnabled(false);
                    diasAtraso.setEnabled(false);
                    taxaAgravamento.setEnabled(false);
                }
            }
        });

        scrollListagemCondominios = new JScrollPane(listagemCondominios);
        scrollListagemCondominios.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominios.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominios.setBounds(20, 150, 150, 300);

        painelFatura.add(scrollListagemCondominios);

        listagemCondominos = new JList(modeloListagemCondominos);
        listagemCondominos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominos.setLayoutOrientation(JList.VERTICAL);
        listagemCondominos.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominos.setFixedCellHeight(20);
        listagemCondominos.setEnabled(false);
        listagemCondominos.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagemCondominos.getSelectedIndex() != -1) {
                        carregarDadosFatura();
                    }
                } else if (listagemCondominos.getSelectedIndex() != -1) {
                    carregarDadosFatura();
                } else {
                    limitePagamento.setText("");
                    limitePagamento.setEnabled(false);
                    diasAtraso.setText("");
                    diasAtraso.setEnabled(false);
                    taxaAgravamento.setText("");
                    taxaAgravamento.setEnabled(false);
                    valorMensalidade.setText("");
                    valorMensalidade.setEnabled(false);
                    registar.setEnabled(false);
                    emitir.setEnabled(false);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                }
            }
        });

        scrollListagemCondominos = new JScrollPane(listagemCondominos);
        scrollListagemCondominos.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominos.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominos.setBounds(180, 150, 150, 300);

        painelFatura.add(scrollListagemCondominos);

        JLabel labelLimitePagamento = new JLabel("Dia Limite Pagamento");
        labelLimitePagamento.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelLimitePagamento.setBounds(345, 150, 200, 20);
        painelFatura.add(labelLimitePagamento);

        limitePagamento = new JTextField();
        limitePagamento.setFont(new Font("Verdana", Font.PLAIN, 14));
        limitePagamento.setBounds(520, 150, 50, 20);
        limitePagamento.setEnabled(false);
        painelFatura.add(limitePagamento);

        JLabel labelDiasAtraso = new JLabel("Dia de Atraso Aceites");
        labelDiasAtraso.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDiasAtraso.setBounds(345, 185, 200, 20);
        painelFatura.add(labelDiasAtraso);

        diasAtraso = new JTextField();
        diasAtraso.setFont(new Font("Verdana", Font.PLAIN, 14));
        diasAtraso.setBounds(520, 185, 50, 20);
        diasAtraso.setEnabled(false);
        painelFatura.add(diasAtraso);

        JLabel labelTaxaAgravamento = new JLabel("Taxa Agravamento");
        labelTaxaAgravamento.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelTaxaAgravamento.setBounds(345, 220, 200, 20);
        painelFatura.add(labelTaxaAgravamento);

        taxaAgravamento = new JTextField();
        taxaAgravamento.setFont(new Font("Verdana", Font.PLAIN, 14));
        taxaAgravamento.setBounds(490, 220, 80, 20);
        taxaAgravamento.setEnabled(false);
        painelFatura.add(taxaAgravamento);

        JLabel labelValorMensalidade = new JLabel("Valor Mensalidade");
        labelValorMensalidade.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelValorMensalidade.setBounds(345, 255, 200, 20);
        painelFatura.add(labelValorMensalidade);

        valorMensalidade = new JTextField();
        valorMensalidade.setFont(new Font("Verdana", Font.PLAIN, 14));
        valorMensalidade.setBounds(490, 255, 80, 20);
        valorMensalidade.setEnabled(false);
        painelFatura.add(valorMensalidade);

        registar = new JButton("Registar");
        registar.setName("registar");
        registar.setFont(new Font("Verdana", Font.ITALIC, 14));
        registar.setBounds(20, 500, 85, 20);
        registar.setEnabled(false);
        painelFatura.add(registar);
        registar.addActionListener(this);

        emitir = new JButton("Emitir");
        emitir.setName("emitir");
        emitir.setFont(new Font("Verdana", Font.ITALIC, 14));
        emitir.setBounds(120, 500, 85, 20);
        emitir.setEnabled(false);
        painelFatura.add(emitir);
        emitir.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(220, 500, 85, 20);
        editar.setEnabled(false);
        painelFatura.add(editar);
        editar.addActionListener(this);

        gravar = new JButton("Gravar");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelFatura.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelFatura.add(fundo);

        painelFatura.revalidate();
        painelFatura.repaint();
        return painelFatura;
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelFatura,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelFatura,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(painelFatura, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);

        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelFatura,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condomínios para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaCondominios.size(); i++) {
            modeloListagemCondominios.addElement("*" + listaCondominios.get(i).getNome());
        }
    }

    /**
     * Método que carrega o nome dos condóminos de um determinado condomínio
     * para uma Jlist
     */
    public void carregarListagemCondominos() {
        modeloListagemCondominos.clear();
        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().size(); i++) {
            modeloListagemCondominos.addElement("*" + listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaCondominos().get(i).getProprietario().getNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos a informação da faturação
     * do condómino selecionado
     */
    public void carregarDadosFatura() {

        editarCondominio = listaCondominios.get(listagemCondominios.getSelectedIndex());
        editarCondomino = editarCondominio.getListaCondominos().get(listagemCondominos.getSelectedIndex());

        if (editarCondomino.getDiasAtraso() == 0
                && editarCondomino.getLimitePagamento() == 0
                && editarCondomino.getTaxaAgravamento() == 0) {
            JOptionPane.showMessageDialog(painelFatura,
                    "As informações relativas à faturação deste condómino ainda "
                    + "não foram registadas\nPor favor preencha os dados "
                    + "desbloqueados e clique na opção \"Registar\"", "Erro", JOptionPane.ERROR_MESSAGE);
            registar.setEnabled(true);
            limitePagamento.setEnabled(true);
            limitePagamento.requestFocusInWindow();
            diasAtraso.setEnabled(true);
            taxaAgravamento.setEnabled(true);
        } else {
            limitePagamento.setText("" + editarCondomino.getLimitePagamento());
            diasAtraso.setText("" + editarCondomino.getDiasAtraso());
            taxaAgravamento.setText("" + editarCondomino.getTaxaAgravamento());
            valorMensalidade.setText("" + editarCondomino.getValorMensalidade());
            emitir.setEnabled(true);
            editar.setEnabled(true);
            gravar.setEnabled(false);
        }
    }

    /**
     * Método que regista a informação da faturação do condómino selecionado
     */
    public void registarInfoFaturas() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.limitePagamento.getText().isEmpty())
                || (this.diasAtraso.getText().isEmpty())
                || (this.taxaAgravamento.getText().isEmpty())) {

            JOptionPane.showMessageDialog(painelFatura, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int limite, dias;
            double agravamento;

            try {
                limite = Integer.parseInt(this.limitePagamento.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.limitePagamento.setText("");
                this.limitePagamento.requestFocusInWindow();
                return;
            }

            try {
                dias = Integer.parseInt(this.diasAtraso.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.diasAtraso.setText("");
                this.diasAtraso.requestFocusInWindow();
                return;
            }

            try {
                agravamento = Double.parseDouble(this.taxaAgravamento.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.taxaAgravamento.setText("");
                this.taxaAgravamento.requestFocusInWindow();
                return;
            }

            editarCondomino.infoFaturas(limite, dias, agravamento);

            JOptionPane.showMessageDialog(painelFatura,
                    "Informação registada com sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);

            guardar();

            limitePagamento.setText("");
            limitePagamento.setEnabled(false);
            diasAtraso.setText("");
            diasAtraso.setEnabled(false);
            taxaAgravamento.setText("");
            taxaAgravamento.setEnabled(false);
            valorMensalidade.setText("");
            valorMensalidade.setEnabled(false);
            registar.setEnabled(false);
            emitir.setEnabled(false);
            editar.setEnabled(false);
            gravar.setEnabled(false);
        }
    }

    /**
     * Método que edita a informação da faturação do condómino selecionado
     */
    public void editarInfoFaturas() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.limitePagamento.getText().isEmpty())
                || (this.diasAtraso.getText().isEmpty())
                || (this.taxaAgravamento.getText().isEmpty())) {

            JOptionPane.showMessageDialog(painelFatura, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int limite, dias;
            double agravamento;

            try {
                limite = Integer.parseInt(this.limitePagamento.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.limitePagamento.setText("");
                this.limitePagamento.requestFocusInWindow();
                return;
            }

            try {
                dias = Integer.parseInt(this.diasAtraso.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.diasAtraso.setText("");
                this.diasAtraso.requestFocusInWindow();
                return;
            }

            try {
                agravamento = Double.parseDouble(this.taxaAgravamento.getText());
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Estes campos apenas aceitam números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.taxaAgravamento.setText("");
                this.taxaAgravamento.requestFocusInWindow();
                return;
            }

            editarCondomino.infoFaturas(limite, dias, agravamento);

            JOptionPane.showMessageDialog(painelFatura,
                    "Informação editada com sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);

            guardar();

            limitePagamento.setText("");
            limitePagamento.setEnabled(false);
            diasAtraso.setText("");
            diasAtraso.setEnabled(false);
            taxaAgravamento.setText("");
            taxaAgravamento.setEnabled(false);
            valorMensalidade.setText("");
            valorMensalidade.setEnabled(false);
            registar.setEnabled(false);
            emitir.setEnabled(false);
            editar.setEnabled(false);
            gravar.setEnabled(false);
        }
    }

    /**
     * Método utilizado para desenhar a JDialog específica para inserir a
     * mensalidade que se deseja emitir para pagamento
     */
    public void desenharJDialogEmitir() {
        JDialog emitir = new JDialog();
        JPanel emitirPanel = new JPanel(null);
        emitirPanel.setBackground(Color.white);
        emitir.setContentPane(emitirPanel);

        emitir.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        emitir.setPreferredSize(new Dimension(400, 300));

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        emitir.setLocation(dim.width / 2 - 400 / 2, dim.height / 2 - 300 / 2);
        emitir.setResizable(false);

        JLabel tituloJDialog = new JLabel("Emitir Fatura");
        tituloJDialog.setBounds(120, 20, 200, 40);
        tituloJDialog.setFont(new Font("Verdana", Font.BOLD, 18));
        emitirPanel.add(tituloJDialog);

        JLabel labelMensalidade = new JLabel("Mensalidade:");
        labelMensalidade.setBounds(20, 120, 120, 20);
        labelMensalidade.setFont(new Font("Verdana", Font.PLAIN, 14));
        emitirPanel.add(labelMensalidade);

        mensalidade = new JTextField();
        GregorianCalendar agora = new GregorianCalendar();
        mensalidade.setFont(new Font("Verdana", Font.PLAIN, 14));
        mensalidade.setBounds(120, 120, 70, 20);
        mensalidade.setEnabled(true);
        mensalidade.setText("" + (1 + agora.get(agora.MONTH)));
        emitirPanel.add(mensalidade);

        ok = new JButton("OK");
        ok.setName("ok");
        ok.setFont(new Font("Verdana", Font.ITALIC, 14));
        ok.setBounds(158, 220, 85, 20);
        ok.setEnabled(true);
        emitirPanel.add(ok);
        ok.addActionListener(this);

        emitirPanel.revalidate();
        emitirPanel.repaint();

        emitir.pack();
        emitir.setVisible(true);
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoClicado = (JButton) e.getSource();
        String nomebotao = botaoClicado.getName();

        if (nomebotao.equals("registar")) {
            registarInfoFaturas();
        } else if (nomebotao.equals("emitir")) {
            desenharJDialogEmitir();
        } else if (nomebotao.equals("editar")) {
            limitePagamento.setEnabled(true);
            diasAtraso.setEnabled(true);
            taxaAgravamento.setEnabled(true);
            emitir.setEnabled(false);
            editar.setEnabled(false);
            gravar.setEnabled(true);
        } else if (nomebotao.equals("gravar")) {
            editarInfoFaturas();
        } else if (nomebotao.equals("ok")) {
            int mensalidade;
            try {
                mensalidade = Integer.parseInt(this.mensalidade.getText()) - 1;
            } catch (Exception z) {
                JOptionPane.showMessageDialog(painelFatura, "Este campo apenas aceita números",
                        "Erro", JOptionPane.ERROR_MESSAGE);
                this.taxaAgravamento.setText("");
                this.taxaAgravamento.requestFocusInWindow();
                return;
            }

            Fatura novaFatura = new Fatura(editarCondomino.getProprietario(), editarCondomino.getFracao(),
                    editarCondomino.getLimitePagamento(), editarCondomino.getDiasAtraso(),
                    mensalidade, editarCondomino.getTaxaAgravamento(), editarCondomino.getValorMensalidade());

            boolean repetido = false;
            GregorianCalendar agora = new GregorianCalendar();

            /* Verifica se já foi emitida para pagamento a mensalidade em questão 
            e caso tenha sido faz a reemissão da mesma. A reemissão só ocorre se a 
            fatura ainda não tiver sido paga
             */
            int i;
            for (i = 0; i < editarCondomino.getListaFaturas().size(); i++) {
                GregorianCalendar compararData = editarCondomino.getListaFaturas().get(i)
                        .getDataLimite();
                int compararAno = compararData.get(compararData.YEAR);
                if (editarCondomino.getListaFaturas().get(i).getMensalidade() == mensalidade
                        && compararData.get(compararData.YEAR) == compararAno) {
                    if (editarCondomino.getListaFaturas().get(i).isPaga() == true) {
                        JOptionPane.showMessageDialog(painelFatura,
                                "Esta fatura já foi paga.\n Impossivel reemitir",
                                "Erro", JOptionPane.OK_OPTION);
                        repetido = true;
                        break;
                    } else {
                        editarCondomino.eliminarFatura(editarCondomino.getListaFaturas().get(i));
                        editarCondomino.registarFatura(novaFatura);
                        JOptionPane.showMessageDialog(painelFatura,
                                "Esta fatura já tinha sido emitida.\n Foi reemitada com sucesso",
                                "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                        guardar();
                        repetido = true;
                        break;
                    }
                }
            }
            if (repetido == false) {
                editarCondomino.registarFatura(novaFatura);
                JOptionPane.showMessageDialog(painelFatura,
                        "Fatura emitida com sucesso", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                guardar();
            }
        }
    }
}
