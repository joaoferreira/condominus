package frontend;

import backend.*;
import com.itextpdf.text.DocumentException;
import java.util.Date;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.logging.*;

/**
 * A existência da classe JanelaPrincipal permite a criação do content panel
 * específico da janela principal do Programa
 *
 * @author JoaoFerreira
 */
public class JanelaPrincipal implements ActionListener {

    // Variáveis de instância 
    private final JFrame janelaPrincipal;
    private JPanel painelPrincipal;
    private JMenuBar barraMenu;
    private ListaCondominios listaCondominios;
    private JDialog dialogFaturacao;
    private JPanel dialogFaturacaoPanel;
    private JButton ok;
    private JSpinner dataInicio, dataFim;

    /**
     * Método construtor da classe
     */
    public JanelaPrincipal() {
        janelaPrincipal = new JFrame("Condominus - Sistema de Gestão de Condomínios v1.0");
    }

    /**
     * Método utilizado para criar e desenhar a janela principal do programa
     */
    public void desenharJanela() {
        painelPrincipal = new JPanel(new BorderLayout());
        painelPrincipal.setBackground(Color.white);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(800, 144,
                Image.SCALE_SMOOTH)));

        painelPrincipal.add(fundo, BorderLayout.CENTER);
        janelaPrincipal.setContentPane(painelPrincipal);
        janelaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janelaPrincipal.setPreferredSize(new Dimension(800, 600));

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        janelaPrincipal.setLocation(dim.width / 2 - 800 / 2, dim.height / 2 - 600 / 2);
        janelaPrincipal.setResizable(false);

        criarBarraMenu();
        janelaPrincipal.setJMenuBar(barraMenu);
        janelaPrincipal.pack();
        janelaPrincipal.setVisible(true);
    }

    /**
     * Método que carrega a lista de condomínios de um ficheiro previamente
     * gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(janelaPrincipal,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(janelaPrincipal,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método utilizado para criar a barra de menu da janela principal do
     * programa
     */
    public void criarBarraMenu() {
        barraMenu = new JMenuBar();
        barraMenu.setBackground(Color.white);
        criarMenuCondominio();
        criarMenuOrcamentos();
        criarMenuColaboradores();
        criarMenuFaturacao();
        criarMenuNotificacoes();
        criarMenuSobre();
    }

    /**
     * Método utilizado para criar o menu específico Gestão Condomínios
     */
    public void criarMenuCondominio() {

        JMenu gestaoCondominios = new JMenu("Gestão Condomínios");

        JMenuItem condominios = new JMenuItem("Condomínios");
        condominios.setName("condominios");
        condominios.addActionListener(this);

        JMenuItem condominos = new JMenuItem("Condóminos");
        condominos.setName("condominos");
        condominos.addActionListener(this);

        JMenuItem inquilinos = new JMenuItem("Inquilínos");
        inquilinos.setName("inquilinos");
        inquilinos.addActionListener(this);

        JMenuItem atividades = new JMenuItem("Atividades Manutenção");
        atividades.setName("atividades");
        atividades.addActionListener(this);

        JMenuItem reunioes = new JMenuItem("Reuniões");
        reunioes.setName("reunioes");
        reunioes.addActionListener(this);

        JMenuItem imprimirCondominio = new JMenuItem("Imprimir Relatório");
        imprimirCondominio.setName("imprimirCondominio");
        imprimirCondominio.addActionListener(this);

        gestaoCondominios.add(condominios);
        gestaoCondominios.add(condominos);
        gestaoCondominios.add(inquilinos);
        gestaoCondominios.add(atividades);
        gestaoCondominios.add(reunioes);
        gestaoCondominios.add(imprimirCondominio);

        barraMenu.add(gestaoCondominios);
    }

    /**
     * Método utilizado para criar o menu específico Orçamentos
     */
    public void criarMenuOrcamentos() {

        JMenu orcamento = new JMenu("Orçamentos");

        JMenuItem criarEditarOrcamento = new JMenuItem("Criar/Editar Orçamento");
        criarEditarOrcamento.setName("orcamento");
        criarEditarOrcamento.addActionListener(this);

        JMenuItem imprimirOrcamento = new JMenuItem("Imprimir Orçamento");
        imprimirOrcamento.setName("imprimirOrcamento");
        imprimirOrcamento.addActionListener(this);

        orcamento.add(criarEditarOrcamento);
        orcamento.add(imprimirOrcamento);

        barraMenu.add(orcamento);
    }

    /**
     * Método utilizado para criar o menu específico Colaboradores
     */
    public void criarMenuColaboradores() {

        JMenu colaborador = new JMenu("Colaboradores");

        JMenuItem colaboradores = new JMenuItem("Adicionar/Editar");
        colaboradores.setName("colaboradores");
        colaboradores.addActionListener(this);
        colaborador.add(colaboradores);

        barraMenu.add(colaborador);
    }

    /**
     * Método utilizado para criar o menu específico Faturação
     */
    public void criarMenuFaturacao() {

        JMenu faturacao = new JMenu("Faturação");

        JMenuItem faturas = new JMenuItem("Faturas");
        faturas.setName("faturas");
        faturas.addActionListener(this);

        JMenuItem pagamento = new JMenuItem("Registar Pagamento");
        pagamento.setName("pagamento");
        pagamento.addActionListener(this);

        JMenu estatisticas = new JMenu("Estatísticas");

        JMenuItem top5 = new JMenuItem("Top 5 Condomínios");
        top5.setName("top5");
        top5.addActionListener(this);

        JMenuItem condominiosCodigoPostal = new JMenuItem("Condomínios/Código Postal");
        condominiosCodigoPostal.setName("condominiosCodigoPostal");
        condominiosCodigoPostal.addActionListener(this);

        JMenuItem faturacaoPeriodo = new JMenuItem("Facturação/Período");
        faturacaoPeriodo.setName("faturacaoPeriodo");
        faturacaoPeriodo.addActionListener(this);

        estatisticas.add(condominiosCodigoPostal);
        estatisticas.add(faturacaoPeriodo);
        estatisticas.add(top5);

        faturacao.add(faturas);
        faturacao.add(pagamento);
        faturacao.add(estatisticas);

        barraMenu.add(faturacao);
    }

    /**
     * Método utilizado para criar o menu específico Notificações
     */
    public void criarMenuNotificacoes() {

        JMenu notificacoes = new JMenu("Notificações");

        JMenuItem consultarNotificacoes = new JMenuItem("Consultar");
        consultarNotificacoes.setName("notificacoes");
        consultarNotificacoes.addActionListener(this);
        notificacoes.add(consultarNotificacoes);

        barraMenu.add(notificacoes);
    }

    /**
     * Método utilizado para criar o menu específico Sobre
     */
    public void criarMenuSobre() {

        JMenu sobre = new JMenu("Sobre");

        JMenuItem info = new JMenuItem("Autor/Versão");
        info.setName("sobre");
        info.addActionListener(this);
        sobre.add(info);

        barraMenu.add(sobre);
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Condomínios
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelCondominio() throws IOException {
        PainelCondominio painelCondominio = new PainelCondominio();
        janelaPrincipal.setContentPane(painelCondominio.desenharPainelCondominio());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Condóminos
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelCondomino() throws IOException {
        PainelCondomino painelCondomino = new PainelCondomino();
        janelaPrincipal.setContentPane(painelCondomino.desenharPainelCondomino());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Colaboradores
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelColaboradores() throws IOException {
        PainelColaborador painelColaborador = new PainelColaborador();
        janelaPrincipal.setContentPane(painelColaborador.desenharPainelColaborador());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Inquilínos
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelInquilinos() throws IOException {
        PainelInquilino painelInquilino = new PainelInquilino();
        janelaPrincipal.setContentPane(painelInquilino.desenharPainelInquilino());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Atividades Manutenção
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelAtividadesManutencao() throws IOException {
        PainelAtividadeManutencao painelAtividadeManutencao = new PainelAtividadeManutencao();
        janelaPrincipal.setContentPane(painelAtividadeManutencao.desenharPainelAtividadeManutencao());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Reuniões
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelReuniao() throws IOException {
        PainelReuniao painelReuniao = new PainelReuniao();
        janelaPrincipal.setContentPane(painelReuniao.desenharPainelReuniao());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Orçamentos
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelOrcamento() throws IOException {
        PainelOrcamento painelOrcamento = new PainelOrcamento();
        janelaPrincipal.setContentPane(painelOrcamento.desenharPainelOrcamentoPasso1());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Faturas
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelFatura() throws IOException {
        PainelFatura painelFatura = new PainelFatura();
        janelaPrincipal.setContentPane(painelFatura.desenharPainelFatura());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Pagamentos
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelPagamento() throws IOException {
        PainelPagamento painelPagamento = new PainelPagamento();
        janelaPrincipal.setContentPane(painelPagamento.desenharPainelPagamento());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para lançar uma mensagem com os top 5 condomínios com
     * maior volume de faturação
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mensagemTop5() throws IOException {
        carregar();
        JOptionPane.showMessageDialog(janelaPrincipal, listaCondominios.top5Condominios(),
                "Informação", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Método utilizado para lançar uma mensagem com o código postal com mais
     * condomínios registados no programa
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mensagemTopCodigoPostal() throws IOException {
        carregar();
        JOptionPane.showMessageDialog(janelaPrincipal, listaCondominios.topCodigoPostal(),
                "Informação", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Método utilizado para desenhar a JDialog para inserir a data de início e
     * data de fim para calcular a faturação da empresa nesse período
     *
     * @throws java.io.IOException Exceção ocorrida
     */
    public void mensagemFaturacaoPeriodo() throws IOException {
        dialogFaturacao = new JDialog();
        dialogFaturacaoPanel = new JPanel(null);
        dialogFaturacaoPanel.setBackground(Color.white);
        dialogFaturacao.setContentPane(dialogFaturacaoPanel);

        dialogFaturacao.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialogFaturacao.setPreferredSize(new Dimension(400, 300));

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        dialogFaturacao.setLocation(dim.width / 2 - 400 / 2, dim.height / 2 - 300 / 2);
        dialogFaturacao.setResizable(false);

        JLabel tituloJDialog = new JLabel("Descriminar Período");
        tituloJDialog.setBounds(100, 20, 240, 40);
        tituloJDialog.setFont(new Font("Verdana", Font.BOLD, 18));
        dialogFaturacaoPanel.add(tituloJDialog);

        JLabel labelDataInicio = new JLabel("Data Início:");
        labelDataInicio.setBounds(20, 100, 120, 20);
        labelDataInicio.setFont(new Font("Verdana", Font.PLAIN, 14));
        dialogFaturacaoPanel.add(labelDataInicio);

        dataInicio = new JSpinner(new SpinnerDateModel());
        dataInicio.setEditor(new JSpinner.DateEditor(dataInicio, "dd/MM/yyyy"));
        dataInicio.setFont(new Font("Verdana", Font.PLAIN, 14));
        dataInicio.setBounds(110, 100, 165, 20);
        dataInicio.setEnabled(true);
        dialogFaturacaoPanel.add(dataInicio);

        JLabel labelDataFim = new JLabel("Data Fim:");
        labelDataFim.setBounds(20, 130, 250, 20);
        labelDataFim.setFont(new Font("Verdana", Font.PLAIN, 14));
        dialogFaturacaoPanel.add(labelDataFim);

        dataFim = new JSpinner(new SpinnerDateModel());
        dataFim.setEditor(new JSpinner.DateEditor(dataFim, "dd/MM/yyyy"));
        dataFim.setFont(new Font("Verdana", Font.PLAIN, 14));
        dataFim.setBounds(100, 130, 165, 20);
        dataFim.setEnabled(true);
        dialogFaturacaoPanel.add(dataFim);

        ok = new JButton("OK");
        ok.setName("ok");
        ok.setFont(new Font("Verdana", Font.ITALIC, 14));
        ok.setBounds(158, 220, 85, 20);
        ok.setEnabled(true);
        dialogFaturacaoPanel.add(ok);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    faturacaoPeriodo();
                } catch (IOException ex) {
                    Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        dialogFaturacaoPanel.revalidate();
        dialogFaturacaoPanel.repaint();

        dialogFaturacao.pack();
        dialogFaturacao.setVisible(true);
    }

    /**
     * Método utilizado para lançar o aviso com o valor faturado no período
     * escolhido
     *
     * @throws java.io.IOException Exceção ocorrida
     */
    public void faturacaoPeriodo() throws IOException {
        // Verifica se todos os campos foram corretamente preenchido
        Date dataInicio = (Date) this.dataInicio.getValue();
        Date dataFim = (Date) this.dataFim.getValue();
        if ((dataInicio.compareTo(dataFim)) > 0) {
            JOptionPane.showMessageDialog(dialogFaturacao, "A data de inicio não"
                    + " pode ser superior à data de fim",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else if (dataInicio.compareTo(dataFim) == 0) {
            JOptionPane.showMessageDialog(dialogFaturacao, "Selecione um intervalo superior"
                    + " a 24 horas",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {
            carregar();
            JOptionPane.showMessageDialog(janelaPrincipal, listaCondominios.topFaturacaoPeriodo(dataInicio, dataFim),
                    "Informação", JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * Método utilizado para mudar o content panel atual para o content panel
     * específico do menu Notificações
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void mudarPainelNotificacoes() throws IOException {
        PainelNotificacoes painelNotificacoes = new PainelNotificacoes();
        janelaPrincipal.setContentPane(painelNotificacoes.desenharPainelNotificacoes());
        janelaPrincipal.revalidate();
        janelaPrincipal.repaint();
    }

    /**
     * Método utilizado para criar a JOptionPane do tipo Information Message do
     * menu Sobre
     */
    public void sobre() {
        JOptionPane.showMessageDialog(painelPrincipal, "<html><center><b>Condominus, Sistema de Gestão de Condomínios <br>"
                + "Versão 1.0</b><br><i>Projecto da Disciplina de PP do 1º Ano de MiEGSI</i><br>"
                + "Desenvolvido por <b>João Ferreira (a47903)</html>",
                "Sobre", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Método utilizado para imprimir um documento que contem a informação de um
     * determinado condomínio
     *
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirCondominio() throws DocumentException, IOException {
        ImprimirDocumentos imprimirCondominio = new ImprimirDocumentos();
        imprimirCondominio.selecionarCondominio();
    }

    /**
     * Método utilizado para imprimir um documento que contem a informação de um
     * determinado orçamento
     *
     * @throws com.itextpdf.text.DocumentException Exceção ocorrida
     * @throws java.io.IOException Exceção ocorrida
     */
    public void imprimirOrcamento() throws DocumentException, IOException {
        ImprimirDocumentos imprimirOrcamento = new ImprimirDocumentos();
        imprimirOrcamento.selecionarOrcamento();
    }

    /**
     * Override do método actionPerformed
     *
     * @param e - Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        try {
            JMenuItem menuItem = (JMenuItem) e.getSource();
            String nomeItem = menuItem.getName();

            switch (nomeItem) {
                case "condominios":
                    mudarPainelCondominio();
                    break;
                case "condominos":
                    mudarPainelCondomino();
                    break;
                case "inquilinos":
                    mudarPainelInquilinos();
                    break;
                case "atividades":
                    mudarPainelAtividadesManutencao();
                    break;
                case "reunioes":
                    mudarPainelReuniao();
                    break;
                case "imprimirCondominio": {
                    try {
                        imprimirCondominio();
                    } catch (DocumentException ex) {
                        Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
                case "orcamento":
                    mudarPainelOrcamento();
                    break;
                case "imprimirOrcamento": {
                    try {
                        imprimirOrcamento();
                    } catch (DocumentException ex) {
                        Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
                case "faturas":
                    mudarPainelFatura();
                    break;
                case "pagamento":
                    mudarPainelPagamento();
                    break;
                case "top5":
                    mensagemTop5();
                    break;
                case "condominiosCodigoPostal":
                    mensagemTopCodigoPostal();
                    break;
                case "faturacaoPeriodo":
                    mensagemFaturacaoPeriodo();
                    break;
                case "sobre":
                    sobre();
                    break;
                case "colaboradores":
                    mudarPainelColaboradores();
                    break;
                case "notificacoes":
                    mudarPainelNotificacoes();
                    break;
            }
        } catch (IOException ex) {
            Logger.getLogger(JanelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método main do programa
     *
     * @param args - Argumentos do método main
     */
    public static void main(String[] args) {
        JanelaPrincipal janelaPrincipal = new JanelaPrincipal();
        janelaPrincipal.desenharJanela();
    }
}
