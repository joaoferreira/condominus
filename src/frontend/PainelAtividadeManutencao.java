package frontend;

import backend.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;

/**
 * A existência da classe PainelAtividadeManutencao permite a criação do content
 * panel específico da janela principal para o menu Atividades Manutenção
 *
 * @author JoaoFerreira
 */
public class PainelAtividadeManutencao implements ActionListener {

    // Variáveis de Instância
    private JPanel painelAtividadeManutencao;
    private JTextField atividadeManutencao;
    private JTextArea descricao;
    private JSpinner data;
    private JList listagemCondominios, listagemAtividadesManutencao, listagemColaboradores;
    private JScrollPane scrollListagemCondominios, scrollListagemAtividadesManutencao,
            scrollListagemColaboradores, scrollDescricao;
    private final DefaultListModel modeloListagemCondominios = new DefaultListModel();
    private final DefaultListModel modeloListagemAtividadesManutencao = new DefaultListModel();
    private final DefaultListModel modeloListagemColaboradores = new DefaultListModel();
    private JButton novo, gravar, editar, apagar;
    private ListaCondominios listaCondominios;
    private ListaColaboradores listaColaboradores;
    private Condominio editarCondominio;
    private AtividadeManutencao novaAtividadeManutencao, editarAtividadeManutencao, apagarAtividadeManutencao;
    boolean edicao = false;

    /**
     * Método utilizado para desenhar o content panel específico do menu
     * Atividades Manutenção
     *
     * @return JPanel - Content panel do menu Atividades Manutenção
     * @throws java.io.IOException Exceção lançada
     */
    public JPanel desenharPainelAtividadeManutencao() throws IOException {

        carregar();
        carregarListagem();

        painelAtividadeManutencao = new JPanel(null);
        painelAtividadeManutencao.setBackground(Color.white);

        JLabel titulo = new JLabel("Atividades Manutenção");
        titulo.setFont(new Font("Verdana", Font.BOLD, 25));
        titulo.setBounds(226, 20, 350, 50);
        painelAtividadeManutencao.add(titulo);

        JSeparator separador = new JSeparator();
        separador.setBounds(20, 85, 760, 10);
        painelAtividadeManutencao.add(separador);

        JLabel labelListagem = new JLabel("Lista Condomínios:");
        labelListagem.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagem.setBounds(20, 115, 150, 20);
        painelAtividadeManutencao.add(labelListagem);

        JLabel labelListagemAtividadesManutencao = new JLabel("Lista Atividades:");
        labelListagemAtividadesManutencao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagemAtividadesManutencao.setBounds(180, 115, 150, 20);
        painelAtividadeManutencao.add(labelListagemAtividadesManutencao);

        listagemCondominios = new JList(modeloListagemCondominios);
        listagemCondominios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemCondominios.setLayoutOrientation(JList.VERTICAL);
        listagemCondominios.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemCondominios.setFixedCellHeight(20);
        listagemCondominios.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {

                } else if (listagemCondominios.getSelectedIndex() != -1) {
                    listagemAtividadesManutencao.setEnabled(true);
                    novo.setEnabled(true);
                    carregarListagemAtividadesManutencao();
                }
            }
        });

        scrollListagemCondominios = new JScrollPane(listagemCondominios);
        scrollListagemCondominios.setVerticalScrollBar(new JScrollBar());
        scrollListagemCondominios.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemCondominios.setBounds(20, 150, 150, 300);

        painelAtividadeManutencao.add(scrollListagemCondominios);

        listagemAtividadesManutencao = new JList(modeloListagemAtividadesManutencao);
        listagemAtividadesManutencao.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listagemAtividadesManutencao.setLayoutOrientation(JList.VERTICAL);
        listagemAtividadesManutencao.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemAtividadesManutencao.setFixedCellHeight(20);
        listagemAtividadesManutencao.setEnabled(false);
        listagemAtividadesManutencao.addListSelectionListener(new ListSelectionListener() {
            // Override do método valueChanged
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting()) {
                    if (listagemAtividadesManutencao.getSelectedIndex() != -1) {
                        carregarDadosAtividadesManutencao();
                    }
                } else if (listagemAtividadesManutencao.getSelectedIndex() != -1) {
                    carregarDadosAtividadesManutencao();
                } else {
                    data.setEnabled(false);
                    atividadeManutencao.setEnabled(false);
                    atividadeManutencao.setText("");
                    descricao.setEnabled(false);
                    descricao.setText("");
                    listagemColaboradores.clearSelection();
                    novo.setEnabled(true);
                    editar.setEnabled(false);
                    gravar.setEnabled(false);
                    apagar.setEnabled(false);
                }
            }
        });

        scrollListagemAtividadesManutencao = new JScrollPane(listagemAtividadesManutencao);
        scrollListagemAtividadesManutencao.setVerticalScrollBar(new JScrollBar());
        scrollListagemAtividadesManutencao.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemAtividadesManutencao.setBounds(180, 150, 150, 300);

        painelAtividadeManutencao.add(scrollListagemAtividadesManutencao);

        JLabel labelAtividadeManutencao = new JLabel("Atividade Manutenção");
        labelAtividadeManutencao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelAtividadeManutencao.setBounds(345, 150, 155, 20);
        painelAtividadeManutencao.add(labelAtividadeManutencao);

        atividadeManutencao = new JTextField();
        atividadeManutencao.setFont(new Font("Verdana", Font.PLAIN, 14));
        atividadeManutencao.setBounds(515, 150, 265, 20);
        atividadeManutencao.setEnabled(false);
        painelAtividadeManutencao.add(atividadeManutencao);

        JLabel labelData = new JLabel("Data");
        labelData.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelData.setBounds(345, 185, 60, 20);
        painelAtividadeManutencao.add(labelData);

        data = new JSpinner(new SpinnerDateModel());
        data.setEditor(new JSpinner.DateEditor(data, "dd/MM/yyyy"));
        data.setFont(new Font("Verdana", Font.PLAIN, 14));
        data.setBounds(400, 185, 150, 20);
        data.setEnabled(false);
        painelAtividadeManutencao.add(data);

        JLabel labelDescricao = new JLabel("Descrição:");
        labelDescricao.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelDescricao.setBounds(345, 220, 80, 20);
        painelAtividadeManutencao.add(labelDescricao);

        descricao = new JTextArea();
        descricao.setFont(new Font("Verdana", Font.PLAIN, 14));
        descricao.setEnabled(false);
        descricao.setLineWrap(true);
        descricao.setWrapStyleWord(true);

        scrollDescricao = new JScrollPane(descricao);
        scrollDescricao.setVerticalScrollBar(new JScrollBar());
        scrollDescricao.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollDescricao.setBounds(345, 255, 250, 195);

        painelAtividadeManutencao.add(scrollDescricao);

        JLabel labelListagemColaboradores = new JLabel("Lista Colaboradores:");
        labelListagemColaboradores.setFont(new Font("Verdana", Font.ITALIC, 14));
        labelListagemColaboradores.setBounds(610, 220, 170, 20);
        painelAtividadeManutencao.add(labelListagemColaboradores);

        listagemColaboradores = new JList(modeloListagemColaboradores);
        listagemColaboradores.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listagemColaboradores.setLayoutOrientation(JList.VERTICAL);
        listagemColaboradores.setFont(new Font("Verdana", Font.PLAIN, 14));
        listagemColaboradores.setFixedCellHeight(20);
        listagemColaboradores.setEnabled(false);

        scrollListagemColaboradores = new JScrollPane(listagemColaboradores);
        scrollListagemColaboradores.setVerticalScrollBar(new JScrollBar());
        scrollListagemColaboradores.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollListagemColaboradores.setBounds(610, 255, 165, 195);

        painelAtividadeManutencao.add(scrollListagemColaboradores);

        carregarListagemColaboradores();

        novo = new JButton("Novo");
        novo.setName("novo");
        novo.setFont(new Font("Verdana", Font.ITALIC, 14));
        novo.setBounds(20, 500, 85, 20);
        novo.setEnabled(false);
        painelAtividadeManutencao.add(novo);
        novo.addActionListener(this);

        editar = new JButton("Editar");
        editar.setName("editar");
        editar.setFont(new Font("Verdana", Font.ITALIC, 14));
        editar.setBounds(120, 500, 85, 20);
        editar.setEnabled(false);
        painelAtividadeManutencao.add(editar);
        editar.addActionListener(this);

        apagar = new JButton("Apagar");
        apagar.setName("apagar");
        apagar.setFont(new Font("Verdana", Font.ITALIC, 14));
        apagar.setBounds(220, 500, 85, 20);
        apagar.setEnabled(false);
        painelAtividadeManutencao.add(apagar);
        apagar.addActionListener(this);

        gravar = new JButton("Gravar");
        gravar.setName("gravar");
        gravar.setFont(new Font("Verdana", Font.ITALIC, 14));
        gravar.setBounds(320, 500, 85, 20);
        gravar.setEnabled(false);
        painelAtividadeManutencao.add(gravar);
        gravar.addActionListener(this);

        JLabel fundo = new JLabel();
        fundo.setIcon(new ImageIcon(new ImageIcon("logotipo.png").getImage().getScaledInstance(320, 58,
                Image.SCALE_SMOOTH)));
        fundo.setBounds(450, 450, 320, 58);
        painelAtividadeManutencao.add(fundo);

        painelAtividadeManutencao.revalidate();
        painelAtividadeManutencao.repaint();
        return painelAtividadeManutencao;
    }

    /**
     * Método que carrega a lista de condomínios e a lista de colaboradores de
     * um ficheiro previamente gravado graças ao interface Serializable
     *
     * @throws java.io.IOException Exceção lançada
     */
    public void carregar() throws IOException {
        ListaCondominios infoCarregar = null;
        FileInputStream ficheiro = null;
        ObjectInputStream carregar = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_condominios.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregar = (ListaCondominios) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaCondominios = infoCarregar;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelAtividadeManutencao,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelAtividadeManutencao,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaCondominios = new ListaCondominios();
        }
        ListaColaboradores infoCarregarColaboradores = null;
        try {
            ficheiro = new FileInputStream("saved_data/lista_colaboradores.ser");
            carregar = new ObjectInputStream(ficheiro);
            infoCarregarColaboradores = (ListaColaboradores) carregar.readObject();
            carregar.close();
            ficheiro.close();
            listaColaboradores = infoCarregarColaboradores;
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelAtividadeManutencao,
                    "Ficheiro não encontrado/Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaColaboradores = new ListaColaboradores();
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(painelAtividadeManutencao,
                    "Dados corrompidos", "Erro", JOptionPane.ERROR_MESSAGE);
            listaColaboradores = new ListaColaboradores();
        } finally {
            if (ficheiro != null) {
                ficheiro.close();
            } else if (carregar != null) {
                carregar.close();
            }
        }
    }

    /**
     * Método que grava a lista de condomínios num ficheiro graças ao interface
     * Serializable permitindo a conservação dos dados
     */
    public void guardar() {
        try {
            FileOutputStream ficheiro = new FileOutputStream("saved_data/lista_condominios.ser");
            ObjectOutputStream guardar = new ObjectOutputStream(ficheiro);
            guardar.writeObject(listaCondominios);
            guardar.close();
            ficheiro.close();
            JOptionPane.showMessageDialog(painelAtividadeManutencao, "Dados gravados com sucesso",
                    "Terminado", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException i) {
            JOptionPane.showMessageDialog(painelAtividadeManutencao,
                    "Erro ao gravar", "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Método que carrega o nome dos condomínios para uma Jlist
     */
    public void carregarListagem() {
        for (int i = 0; i < listaCondominios.size(); i++) {
            modeloListagemCondominios.addElement("*" + listaCondominios.get(i).getNome());
        }
    }

    /**
     * Método que carrega a data das atividades de manutenção de um determinado
     * condomínio para uma Jlist
     */
    public void carregarListagemAtividadesManutencao() {
        modeloListagemAtividadesManutencao.clear();
        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().size(); i++) {
            String stringData = new SimpleDateFormat("yyyy-MM-dd").format(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().get(i).getData());
            modeloListagemAtividadesManutencao.addElement("*" + stringData);
        }
    }

    /**
     * Método que carrega os nomes dos colaboradores da empresa condominus para
     * uma Jlist
     */
    public void carregarListagemColaboradores() {
        modeloListagemColaboradores.clear();
        for (int i = 0; i < listaColaboradores.size(); i++) {
            modeloListagemColaboradores.addElement("*" + listaColaboradores.get(i).getPrimeiroNome() + " " + listaColaboradores.get(i).getUltimoNome());
        }
    }

    /**
     * Método que carrega para os respectivos campos os dados da atividade de
     * manutenção selecionada
     */
    public void carregarDadosAtividadesManutencao() {
        data.setEnabled(false);
        data.setValue(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                get(listagemAtividadesManutencao.getSelectedIndex()).getData());
        atividadeManutencao.setEnabled(false);
        atividadeManutencao.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                get(listagemAtividadesManutencao.getSelectedIndex()).getActividadeManutencao());
        descricao.setEnabled(false);
        descricao.setText(listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                get(listagemAtividadesManutencao.getSelectedIndex()).getDescricao());

        listagemColaboradores.setEnabled(false);

        /*
        Esta porção de código permite ir à array de colaboradores de uma determinada atividade de manutenção 
        buscar os seu índices para que possam ser carregados aquando da edição dessa mesma atividade de manutenção. 
        Assim os colobaradores aparecem já pré-selecionados durante a edição.
         */
        ArrayList<Integer> indices = new ArrayList<>();

        for (int i = 0; i < listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().get(listagemAtividadesManutencao.getSelectedIndex()).getColaboradores().length; i++) {
            if (listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().get(listagemAtividadesManutencao.getSelectedIndex()).getColaboradores()[i] != null) {
                indices.add(i);
            }
        }

        int[] indicesFinal = new int[indices.size()];
        for (int i = 0; i < indices.size(); i++) {
            indicesFinal[i] = indices.get(i);
        }
        listagemColaboradores.setSelectedIndices(indicesFinal);
        // Fim da porção de código previamente comentada

        gravar.setEnabled(false);
        novo.setEnabled(true);
        editar.setEnabled(true);
        apagar.setEnabled(true);

        painelAtividadeManutencao.revalidate();
        painelAtividadeManutencao.repaint();
    }

    /**
     * Método utilizado para gravar uma nova atividade de manutenção no
     * respectivo condomínio
     */
    public void gravar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.atividadeManutencao.getText().isEmpty())
                || (this.descricao.getText().isEmpty())
                || (listagemColaboradores.getSelectedIndices().length == 0)) {

            JOptionPane.showMessageDialog(painelAtividadeManutencao, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            String atividadeManutencao = this.atividadeManutencao.getText();
            String descricao = this.descricao.getText();
            Date data = (Date) this.data.getValue();

            /*
            Esta porção de código permite criar uma array de colaboradores que irá conter os 
            colaboradores selecionados na JList e manterá as suas posições relativas para 
            efeito de carregamento de dados futuro.
             */
            Colaborador[] colaboradores = new Colaborador[listaColaboradores.size()];

            int[] indices = listagemColaboradores.getSelectedIndices();

            for (int i = 0; i < indices.length; i++) {
                colaboradores[indices[i]] = listaColaboradores.get(i);
            }
            // Fim da porção de código previamente comentada

            novaAtividadeManutencao = new AtividadeManutencao(data, atividadeManutencao, descricao, colaboradores);

            int confirmacao = JOptionPane.showConfirmDialog(painelAtividadeManutencao,
                    "Pretende guardar os dados?", "Criação", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {
                editarCondominio = listaCondominios.get(listagemCondominios.getSelectedIndex());
                editarCondominio.agendarAtivManut(novaAtividadeManutencao);

                guardar();

                this.data.setEnabled(false);
                this.atividadeManutencao.setEnabled(false);
                this.atividadeManutencao.setText("");
                this.descricao.setEnabled(false);
                this.descricao.setText("");
                this.listagemColaboradores.clearSelection();
                this.listagemColaboradores.setEnabled(false);

                carregarListagemAtividadesManutencao();

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelAtividadeManutencao.revalidate();
                painelAtividadeManutencao.repaint();
            }
        }
    }

    /**
     * Método utilizado para editar uma determinada atividade de manutenção
     */
    public void editar() {
        // Verifica se todos os campos foram preenchidos 
        if ((this.atividadeManutencao.getText().isEmpty())
                || (this.descricao.getText().isEmpty())
                || (listagemColaboradores.getSelectedIndices().length == 0)) {

            JOptionPane.showMessageDialog(painelAtividadeManutencao, "Por favor preencha todos os campos",
                    "Erro", JOptionPane.ERROR_MESSAGE);
        } else {

            int confirmacao = JOptionPane.showConfirmDialog(painelAtividadeManutencao,
                    "Pretende actualizar os dados?", "Edição", JOptionPane.YES_NO_OPTION);

            if (confirmacao == JOptionPane.YES_OPTION) {

                editarAtividadeManutencao = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().get(listagemAtividadesManutencao.getSelectedIndex());

                listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                        get(listagemAtividadesManutencao.getSelectedIndex()).setData((Date) this.data.getValue());
                listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                        get(listagemAtividadesManutencao.getSelectedIndex()).setAtividadeManutencao(this.atividadeManutencao.getText());
                listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                        get(listagemAtividadesManutencao.getSelectedIndex()).setDescricao(this.descricao.getText());

                /*
                Esta porção de código permite criar uma array de colaboradores que irá conter os 
                colaboradores selecionados na JList e manterá as suas posições relativas para 
                efeito de carregamento de dados futuro.
                 */
                Colaborador[] colaboradores = new Colaborador[listaColaboradores.size()];

                int[] indices = listagemColaboradores.getSelectedIndices();

                for (int i = 0; i < indices.length; i++) {
                    colaboradores[indices[i]] = listaColaboradores.get(i);
                }
                // Fim da porção de código previamente comentada

                listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().
                        get(listagemAtividadesManutencao.getSelectedIndex()).setColaboradores(colaboradores);

                listaCondominios.get(listagemCondominios.getSelectedIndex()).ordenarListaAtivManut();

                guardar();

                this.data.setEnabled(false);
                this.atividadeManutencao.setEnabled(false);
                this.atividadeManutencao.setText("");
                this.descricao.setEnabled(false);
                this.descricao.setText("");
                this.listagemColaboradores.clearSelection();
                this.listagemColaboradores.setEnabled(false);

                listagemAtividadesManutencao.setEnabled(true);
                listagemCondominios.setEnabled(true);
                carregarListagemAtividadesManutencao();

                gravar.setEnabled(false);
                editar.setEnabled(false);
                apagar.setEnabled(false);
                novo.setEnabled(true);

                painelAtividadeManutencao.revalidate();
                painelAtividadeManutencao.repaint();
            }
        }
    }

    /**
     * Método utilizado para apagar uma determinada atividade de manutenção
     */
    public void apagar() {
        int confirmacao = JOptionPane.showConfirmDialog(painelAtividadeManutencao,
                "Pretende apagar a atividade de manutenção?", "Eliminação", JOptionPane.YES_NO_OPTION);

        if (confirmacao == JOptionPane.YES_OPTION) {
            apagarAtividadeManutencao = listaCondominios.get(listagemCondominios.getSelectedIndex()).getListaAtivManut().get(listagemAtividadesManutencao.getSelectedIndex());
            listaCondominios.get(listagemCondominios.getSelectedIndex()).desagendarAtivManut(apagarAtividadeManutencao);

            guardar();

            JOptionPane.showMessageDialog(painelAtividadeManutencao,
                    "Atividade de manutenção eliminada com sucesso", "Confirmação", JOptionPane.YES_NO_OPTION);

            this.data.setEnabled(false);
            this.atividadeManutencao.setEnabled(false);
            this.atividadeManutencao.setText("");
            this.descricao.setEnabled(false);
            this.descricao.setText("");
            this.listagemColaboradores.clearSelection();
            this.listagemColaboradores.setEnabled(false);

            modeloListagemAtividadesManutencao.clear();
            listagemAtividadesManutencao.setEnabled(false);
            listagemCondominios.setEnabled(true);
            listagemAtividadesManutencao.clearSelection();
            listagemCondominios.clearSelection();

            gravar.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);
            novo.setEnabled(true);

            painelAtividadeManutencao.revalidate();
            painelAtividadeManutencao.repaint();
        }
    }

    /**
     * Override do método actionPerformed utilizado para determinar que ação
     * será realizada a quando de um clique
     *
     * @param e Evento ocorrido
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton botaoClicado = (JButton) e.getSource();
        String nomeBotao = botaoClicado.getName();

        if (nomeBotao.equals("novo")) {
            this.data.setEnabled(true);
            this.atividadeManutencao.setEnabled(true);
            this.atividadeManutencao.setText("");
            this.descricao.setEnabled(true);
            this.descricao.setText("");
            this.listagemColaboradores.clearSelection();
            this.listagemColaboradores.setEnabled(true);

            listagemAtividadesManutencao.setEnabled(true);
            listagemCondominios.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(false);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = false;

            painelAtividadeManutencao.revalidate();
            painelAtividadeManutencao.repaint();

        } else if (nomeBotao.equals("gravar")) {
            if (edicao == true) {
                editar();
            } else {
                gravar();
            }
        } else if (nomeBotao.equals("editar")) {
            this.data.setEnabled(true);
            this.atividadeManutencao.setEnabled(true);
            this.descricao.setEnabled(true);

            listagemAtividadesManutencao.setEnabled(true);
            listagemCondominios.setEnabled(true);
            listagemColaboradores.setEnabled(true);

            gravar.setEnabled(true);
            novo.setEnabled(true);
            editar.setEnabled(false);
            apagar.setEnabled(false);

            edicao = true;

            painelAtividadeManutencao.revalidate();
            painelAtividadeManutencao.repaint();

        } else if (nomeBotao.equals("apagar")) {
            apagar();
        }
    }
}
